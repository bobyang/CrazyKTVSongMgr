﻿namespace CrazyKTV_SongMgr
{
    partial class MainForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.SongQuery_TabPage = new System.Windows.Forms.TabPage();
            this.SongQuery_OtherQuery_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongQuery_FavoriteQuery_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongQuery_FavoriteQuery_Label = new System.Windows.Forms.Label();
            this.SongQuery_ExceptionalQuery_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongQuery_ExceptionalQuery_Label = new System.Windows.Forms.Label();
            this.SongQuery_Query_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongQuery_SynonymousQuery_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongQuery_Clear_Button = new System.Windows.Forms.Button();
            this.SongQuery_Paste_Button = new System.Windows.Forms.Button();
            this.SongQuery_QueryFilter_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongQuery_QueryFilter_Label = new System.Windows.Forms.Label();
            this.SongQuery_FuzzyQuery_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongQuery_QueryValue_Label = new System.Windows.Forms.Label();
            this.SongQuery_Query_Button = new System.Windows.Forms.Button();
            this.SongQuery_QueryType_Label = new System.Windows.Forms.Label();
            this.SongQuery_QueryType_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongQuery_QueryValue_TextBox = new System.Windows.Forms.TextBox();
            this.SongQuery_QueryValue_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongQuery_Statistics_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongQuery_Statistics12Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics12_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics11Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics10Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics9Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics8Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics7Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics11_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics10_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics9_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics8_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics7_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics6Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics5Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics4Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics3Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics2Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics1Value_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics6_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics5_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics4_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics3_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics2_Label = new System.Windows.Forms.Label();
            this.SongQuery_Statistics1_Label = new System.Windows.Forms.Label();
            this.SongQuery_DataGridView = new System.Windows.Forms.DataGridView();
            this.SongQuery_QueryStatus_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongQuery_EditMode_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongQuery_QueryStatus_Label = new System.Windows.Forms.Label();
            this.SongAdd_TabPage = new System.Windows.Forms.TabPage();
            this.SongAdd_DragDrop_Label = new System.Windows.Forms.Label();
            this.SongAdd_Add_Button = new System.Windows.Forms.Button();
            this.SongAdd_SpecialStr_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongAdd_SpecialStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongAdd_SpecialStr_Button = new System.Windows.Forms.Button();
            this.SongAdd_SpecialStr_ListBox = new System.Windows.Forms.ListBox();
            this.SongAdd_DataGridView = new System.Windows.Forms.DataGridView();
            this.SongAdd_Tooltip_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongAdd_Tooltip_Label = new System.Windows.Forms.Label();
            this.SongAdd_Save_Button = new System.Windows.Forms.Button();
            this.SongAdd_DefaultSongInfo_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongAdd_DefaultSongVolume_TextBox = new System.Windows.Forms.TextBox();
            this.SongAdd_DefaultSongType_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongAdd_DefaultSongTrack_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongAdd_DefaultSingerType_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongAdd_DefaultSongLang_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongAdd_DefaultSongVolume_Label = new System.Windows.Forms.Label();
            this.SongAdd_DefaultSongType_Label = new System.Windows.Forms.Label();
            this.SongAdd_DefaultSongTrack_Label = new System.Windows.Forms.Label();
            this.SongAdd_DefaultSingerType_Label = new System.Windows.Forms.Label();
            this.SongAdd_DefaultSongLang_Label = new System.Windows.Forms.Label();
            this.SongAdd_SongAddCfg_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongAdd_UseCustomSongID_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongAdd_EngSongNameFormat_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongAdd_DupSongMode_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongAdd_DupSongMode_Label = new System.Windows.Forms.Label();
            this.SongAdd_SongIdentificationMode_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongAdd_SongIdentificationMode_Label = new System.Windows.Forms.Label();
            this.SingerMgr_TabPage = new System.Windows.Forms.TabPage();
            this.SingerMgr_OtherQuery_GroupBox = new System.Windows.Forms.GroupBox();
            this.SingerMgr_QueryType_Label = new System.Windows.Forms.Label();
            this.SingerMgr_QueryType_ComboBox = new System.Windows.Forms.ComboBox();
            this.SingerMgr_SingerAdd_GroupBox = new System.Windows.Forms.GroupBox();
            this.SingerMgr_SingerAddClear_Button = new System.Windows.Forms.Button();
            this.SingerMgr_SingerAddPaste_Button = new System.Windows.Forms.Button();
            this.SingerMgr_SingerAdd_Button = new System.Windows.Forms.Button();
            this.SingerMgr_SingerAddType_ComboBox = new System.Windows.Forms.ComboBox();
            this.SingerMgr_SingerAddType_Label = new System.Windows.Forms.Label();
            this.SingerMgr_SingerAddName_TextBox = new System.Windows.Forms.TextBox();
            this.SingerMgr_SingerAddName_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Manager_GroupBox = new System.Windows.Forms.GroupBox();
            this.SingerMgr_NonSingerDataLog_Button = new System.Windows.Forms.Button();
            this.SingerMgr_RebuildSingerData_Button = new System.Windows.Forms.Button();
            this.SingerMgr_SingerExport_Button = new System.Windows.Forms.Button();
            this.SingerMgr_SingerImport_Button = new System.Windows.Forms.Button();
            this.SingerMgr_Statistics_GroupBox = new System.Windows.Forms.GroupBox();
            this.SingerMgr_Statistics10Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics10_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics9Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics8Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics7Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics9_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics8_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics7_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics6Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics5Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics4Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics3Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics2Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics1Value_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics6_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics5_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics4_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics3_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics2_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Statistics1_Label = new System.Windows.Forms.Label();
            this.SingerMgr_Tooltip_GroupBox = new System.Windows.Forms.GroupBox();
            this.SingerMgr_Tooltip_Label = new System.Windows.Forms.Label();
            this.SingerMgr_DataGridView = new System.Windows.Forms.DataGridView();
            this.SingerMgr_Query_GroupBox = new System.Windows.Forms.GroupBox();
            this.SingerMgr_DefaultSingerDataTable_ComboBox = new System.Windows.Forms.ComboBox();
            this.SingerMgr_DefaultSingerDataTable_Label = new System.Windows.Forms.Label();
            this.SingerMgr_QueryClear_Button = new System.Windows.Forms.Button();
            this.SingerMgr_QueryPaste_Button = new System.Windows.Forms.Button();
            this.SingerMgr_Query_Button = new System.Windows.Forms.Button();
            this.SingerMgr_QueryValue_Label = new System.Windows.Forms.Label();
            this.SingerMgr_QueryValue_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_TabPage = new System.Windows.Forms.TabPage();
            this.SongMgrCfg_SongType_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMgrCfg_SongType_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_SongType_Button = new System.Windows.Forms.Button();
            this.SongMgrCfg_SongType_ListBox = new System.Windows.Forms.ListBox();
            this.SongMgrCfg_SongStructure_TabControl = new System.Windows.Forms.TabControl();
            this.SongMgrCfg_SongStructure_TabPage = new System.Windows.Forms.TabPage();
            this.SongMgrCfg_CrtchorusMerge_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongMgrCfg_SongInfoSeparate_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_FileStructure_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_SongInfoSeparate_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_FileStructure_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CrtchorusSeparate_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_FolderStructure_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_CrtchorusSeparate_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_FolderStructure_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomStructure_TabPage = new System.Windows.Forms.TabPage();
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMgrCfg_CustomSingerTypeStructure1_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_CustomSingerTypeStructure2_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_CustomSingerTypeStructure3_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_CustomSingerTypeStructure4_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_CustomSingerTypeStructure5_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_CustomSingerTypeStructure6_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_CustomSingerTypeStructure7_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_CustomSingerTypeStructure8_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_Tooltip_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMgrCfg_Tooltip_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_SongID_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMgrCfg_Lang10Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang10Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang9Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang9Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang8Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang8Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang7Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang7Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang6Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang6Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang5Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang5Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang4Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang4Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang3Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang3Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang2Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang2Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Lang1Code_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_Lang1Code_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_MaxDigitCode_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_MaxDigitCode_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_General_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMgrCfg_BackupRemoveSong_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongMgrCfg_SongTrackMode_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongMgrCfg_SongAddMode_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongMgrCfg_SongAddMode_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_DestFolder_Button = new System.Windows.Forms.Button();
            this.SongMgrCfg_DestFolder_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_DestFolder_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_SupportFormat_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_SupportFormat_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_DBFile_Button = new System.Windows.Forms.Button();
            this.SongMgrCfg_DBFile_TextBox = new System.Windows.Forms.TextBox();
            this.SongMgrCfg_DBFile_Label = new System.Windows.Forms.Label();
            this.SongMgrCfg_Save_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_TabPage = new System.Windows.Forms.TabPage();
            this.SongMaintenance_Save_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_SongPathChange_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_DestSongPath_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_DestSongPath_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_SrcSongPath_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_SrcSongPath_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_SongPathChange_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_PlayCount_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_PlayCountReset_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_TabControl = new System.Windows.Forms.TabControl();
            this.SongMaintenance_Misc_TabPage = new System.Windows.Forms.TabPage();
            this.SongMaintenance_RebuildSongStructure_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_RebuildSongStructure_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_RebuildSongStructure_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_RebuildSongStructure_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_Phonetics_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_NonPhoneticsWordLog_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_PhoneticsExport_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_PhoneticsImport_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_RemoteCfg_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_RemoteCfgExport_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_RemoteCfgImport_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_Misc_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_RemoveEmptyDirs_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_CompactAccessDB_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_SongWordCountCorrect_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_Favorite_TabPage = new System.Windows.Forms.TabPage();
            this.SongMaintenance_FavoriteExport_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_FavoriteImport_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_Favorite_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Favorite_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_Favorite_ListBox = new System.Windows.Forms.ListBox();
            this.SongMaintenance_CustomLang_TabPage = new System.Windows.Forms.TabPage();
            this.SongMaintenance_Lang10IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang10IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang10_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang10_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang9IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang9IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang9_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang9_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang8IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang8IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang8_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang8_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang7IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang7IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang7_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang7_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang6IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang6IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang6_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang6_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang5IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang5IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang5_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang5_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang4IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang4IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang4_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang4_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang3IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang3IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang3_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang3_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang2IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang2IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang2_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang2_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang1IDStr_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang1IDStr_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_Lang1_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_Lang1_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_MultiSongPath_TabPage = new System.Windows.Forms.TabPage();
            this.SongMaintenance_MultiSongPath_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_MultiSongPath_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_MultiSongPath_ListBox = new System.Windows.Forms.ListBox();
            this.SongMaintenance_EnableMultiSongPath_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongMaintenance_DBVer_TabPage = new System.Windows.Forms.TabPage();
            this.SongMaintenance_DBVerUpdate_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_EnableRebuildSingerData_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongMaintenance_EnableDBVerUpdate_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongMaintenance_DBVerTooltip_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_DBVerTooltip_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_DBVer3Value_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_DBVer2Value_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_DBVer1Value_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_DBVer3_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_DBVer2_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_DBVer1_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_VolumeChange_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_VolumeChange_TextBox = new System.Windows.Forms.TextBox();
            this.SongMaintenance_VolumeChange_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_VolumeChange_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_TrackExchange_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_LRTrackExchange_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_CodeConv_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_CodeCorrect_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_CodeConvTo6_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_CodeConvTo5_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_Tooltip_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_Tooltip_Label = new System.Windows.Forms.Label();
            this.SongMaintenance_SpellCorrect_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongMaintenance_SongSpellCorrect_Button = new System.Windows.Forms.Button();
            this.SongMaintenance_SingerSpellCorrect_Button = new System.Windows.Forms.Button();
            this.MainCfg_TabPage = new System.Windows.Forms.TabPage();
            this.MainCfg_Save_Button = new System.Windows.Forms.Button();
            this.MainCfg_Tooltip_GroupBox = new System.Windows.Forms.GroupBox();
            this.MainCfg_Tooltip_Label = new System.Windows.Forms.Label();
            this.MainCfg_General_ＧroupBox = new System.Windows.Forms.GroupBox();
            this.MainCfg_EnableAutoUpdate_CheckBox = new System.Windows.Forms.CheckBox();
            this.MainCfg_BackupRemoveSongDays_ComboBox = new System.Windows.Forms.ComboBox();
            this.MainCfg_BackupRemoveSongDays_Label = new System.Windows.Forms.Label();
            this.MainCfg_HideSongLogTab_CheckBox = new System.Windows.Forms.CheckBox();
            this.MainCfg_HideSongAddResultTab_CheckBox = new System.Windows.Forms.CheckBox();
            this.MainCfg_HideSongDBConvTab_CheckBox = new System.Windows.Forms.CheckBox();
            this.MainCfg_HideTab_Label = new System.Windows.Forms.Label();
            this.MainCfg_AlwaysOnTop_CheckBox = new System.Windows.Forms.CheckBox();
            this.SongDBConverter_TabPage = new System.Windows.Forms.TabPage();
            this.SongDBConverter_ConvHelp_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongDBConverter_ConvHelp_RichTextBox = new System.Windows.Forms.RichTextBox();
            this.SongDBConverter_StartConv_Button = new System.Windows.Forms.Button();
            this.SongDBConverter_Tooltip_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongDBConverter_Tooltip_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_Converter_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongDBConverter_SrcDBType_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_SrcDBType_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_DestDBFile_Button = new System.Windows.Forms.Button();
            this.SongDBConverter_DestDBFile_TextBox = new System.Windows.Forms.TextBox();
            this.SongDBConverter_DestDBFile_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_SrcDBFile_Button = new System.Windows.Forms.Button();
            this.SongDBConverter_SrcDBFile_TextBox = new System.Windows.Forms.TextBox();
            this.SongDBConverter_SrcDBFile_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLangCfg_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongDBConverter_JetktvLang9_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang9_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLang8_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang8_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLang7_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang7_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLang6_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang6_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLang5_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang5_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLang4_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang4_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLang3_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang3_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLang2_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang2_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvLang1_ComboBox = new System.Windows.Forms.ComboBox();
            this.SongDBConverter_JetktvLang1_Label = new System.Windows.Forms.Label();
            this.SongDBConverter_JetktvPathCfg_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongDBConverter_JetktvPathCfg_Button = new System.Windows.Forms.Button();
            this.SongDBConverter_JetktvPathCfg_TextBox = new System.Windows.Forms.TextBox();
            this.SongDBConverter_JetktvPathCfg_ListBox = new System.Windows.Forms.ListBox();
            this.SongAddResult_TabPage = new System.Windows.Forms.TabPage();
            this.SongAddResult_SplitContainer = new System.Windows.Forms.SplitContainer();
            this.SongAddResult_DuplicateSong_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongAddResult_DuplicateSong_ListBox = new System.Windows.Forms.ListBox();
            this.SongAddResult_FailureSong_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongAddResult_FailureSong_ListBox = new System.Windows.Forms.ListBox();
            this.SongLog_TabPage = new System.Windows.Forms.TabPage();
            this.SongLog_GroupBox = new System.Windows.Forms.GroupBox();
            this.SongLog_ListBox = new System.Windows.Forms.ListBox();
            this.MainTabControl.SuspendLayout();
            this.SongQuery_TabPage.SuspendLayout();
            this.SongQuery_OtherQuery_GroupBox.SuspendLayout();
            this.SongQuery_Query_GroupBox.SuspendLayout();
            this.SongQuery_Statistics_GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SongQuery_DataGridView)).BeginInit();
            this.SongQuery_QueryStatus_GroupBox.SuspendLayout();
            this.SongAdd_TabPage.SuspendLayout();
            this.SongAdd_SpecialStr_GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SongAdd_DataGridView)).BeginInit();
            this.SongAdd_Tooltip_GroupBox.SuspendLayout();
            this.SongAdd_DefaultSongInfo_GroupBox.SuspendLayout();
            this.SongAdd_SongAddCfg_GroupBox.SuspendLayout();
            this.SingerMgr_TabPage.SuspendLayout();
            this.SingerMgr_OtherQuery_GroupBox.SuspendLayout();
            this.SingerMgr_SingerAdd_GroupBox.SuspendLayout();
            this.SingerMgr_Manager_GroupBox.SuspendLayout();
            this.SingerMgr_Statistics_GroupBox.SuspendLayout();
            this.SingerMgr_Tooltip_GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SingerMgr_DataGridView)).BeginInit();
            this.SingerMgr_Query_GroupBox.SuspendLayout();
            this.SongMgrCfg_TabPage.SuspendLayout();
            this.SongMgrCfg_SongType_GroupBox.SuspendLayout();
            this.SongMgrCfg_SongStructure_TabControl.SuspendLayout();
            this.SongMgrCfg_SongStructure_TabPage.SuspendLayout();
            this.SongMgrCfg_CustomStructure_TabPage.SuspendLayout();
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.SuspendLayout();
            this.SongMgrCfg_Tooltip_GroupBox.SuspendLayout();
            this.SongMgrCfg_SongID_GroupBox.SuspendLayout();
            this.SongMgrCfg_General_GroupBox.SuspendLayout();
            this.SongMaintenance_TabPage.SuspendLayout();
            this.SongMaintenance_SongPathChange_GroupBox.SuspendLayout();
            this.SongMaintenance_PlayCount_GroupBox.SuspendLayout();
            this.SongMaintenance_TabControl.SuspendLayout();
            this.SongMaintenance_Misc_TabPage.SuspendLayout();
            this.SongMaintenance_RebuildSongStructure_GroupBox.SuspendLayout();
            this.SongMaintenance_Phonetics_GroupBox.SuspendLayout();
            this.SongMaintenance_RemoteCfg_GroupBox.SuspendLayout();
            this.SongMaintenance_Misc_GroupBox.SuspendLayout();
            this.SongMaintenance_Favorite_TabPage.SuspendLayout();
            this.SongMaintenance_CustomLang_TabPage.SuspendLayout();
            this.SongMaintenance_MultiSongPath_TabPage.SuspendLayout();
            this.SongMaintenance_DBVer_TabPage.SuspendLayout();
            this.SongMaintenance_DBVerUpdate_GroupBox.SuspendLayout();
            this.SongMaintenance_DBVerTooltip_GroupBox.SuspendLayout();
            this.SongMaintenance_VolumeChange_GroupBox.SuspendLayout();
            this.SongMaintenance_TrackExchange_GroupBox.SuspendLayout();
            this.SongMaintenance_CodeConv_GroupBox.SuspendLayout();
            this.SongMaintenance_Tooltip_GroupBox.SuspendLayout();
            this.SongMaintenance_SpellCorrect_GroupBox.SuspendLayout();
            this.MainCfg_TabPage.SuspendLayout();
            this.MainCfg_Tooltip_GroupBox.SuspendLayout();
            this.MainCfg_General_ＧroupBox.SuspendLayout();
            this.SongDBConverter_TabPage.SuspendLayout();
            this.SongDBConverter_ConvHelp_GroupBox.SuspendLayout();
            this.SongDBConverter_Tooltip_GroupBox.SuspendLayout();
            this.SongDBConverter_Converter_GroupBox.SuspendLayout();
            this.SongDBConverter_JetktvLangCfg_GroupBox.SuspendLayout();
            this.SongDBConverter_JetktvPathCfg_GroupBox.SuspendLayout();
            this.SongAddResult_TabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SongAddResult_SplitContainer)).BeginInit();
            this.SongAddResult_SplitContainer.Panel1.SuspendLayout();
            this.SongAddResult_SplitContainer.Panel2.SuspendLayout();
            this.SongAddResult_SplitContainer.SuspendLayout();
            this.SongAddResult_DuplicateSong_GroupBox.SuspendLayout();
            this.SongAddResult_FailureSong_GroupBox.SuspendLayout();
            this.SongLog_TabPage.SuspendLayout();
            this.SongLog_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.SongQuery_TabPage);
            this.MainTabControl.Controls.Add(this.SongAdd_TabPage);
            this.MainTabControl.Controls.Add(this.SingerMgr_TabPage);
            this.MainTabControl.Controls.Add(this.SongMgrCfg_TabPage);
            this.MainTabControl.Controls.Add(this.SongMaintenance_TabPage);
            this.MainTabControl.Controls.Add(this.MainCfg_TabPage);
            this.MainTabControl.Controls.Add(this.SongDBConverter_TabPage);
            this.MainTabControl.Controls.Add(this.SongAddResult_TabPage);
            this.MainTabControl.Controls.Add(this.SongLog_TabPage);
            this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabControl.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainTabControl.Location = new System.Drawing.Point(0, 0);
            this.MainTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(1006, 722);
            this.MainTabControl.TabIndex = 0;
            this.MainTabControl.SelectedIndexChanged += new System.EventHandler(this.MainTabControl_SelectedIndexChanged);
            // 
            // SongQuery_TabPage
            // 
            this.SongQuery_TabPage.Controls.Add(this.SongQuery_OtherQuery_GroupBox);
            this.SongQuery_TabPage.Controls.Add(this.SongQuery_Query_GroupBox);
            this.SongQuery_TabPage.Controls.Add(this.SongQuery_Statistics_GroupBox);
            this.SongQuery_TabPage.Controls.Add(this.SongQuery_DataGridView);
            this.SongQuery_TabPage.Controls.Add(this.SongQuery_QueryStatus_GroupBox);
            this.SongQuery_TabPage.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongQuery_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongQuery_TabPage.Name = "SongQuery_TabPage";
            this.SongQuery_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.SongQuery_TabPage.Size = new System.Drawing.Size(998, 684);
            this.SongQuery_TabPage.TabIndex = 1;
            this.SongQuery_TabPage.Text = "歌庫查詢";
            this.SongQuery_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongQuery_OtherQuery_GroupBox
            // 
            this.SongQuery_OtherQuery_GroupBox.Controls.Add(this.SongQuery_FavoriteQuery_ComboBox);
            this.SongQuery_OtherQuery_GroupBox.Controls.Add(this.SongQuery_FavoriteQuery_Label);
            this.SongQuery_OtherQuery_GroupBox.Controls.Add(this.SongQuery_ExceptionalQuery_ComboBox);
            this.SongQuery_OtherQuery_GroupBox.Controls.Add(this.SongQuery_ExceptionalQuery_Label);
            this.SongQuery_OtherQuery_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_OtherQuery_GroupBox.Location = new System.Drawing.Point(23, 209);
            this.SongQuery_OtherQuery_GroupBox.Name = "SongQuery_OtherQuery_GroupBox";
            this.SongQuery_OtherQuery_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongQuery_OtherQuery_GroupBox.Size = new System.Drawing.Size(468, 82);
            this.SongQuery_OtherQuery_GroupBox.TabIndex = 1;
            this.SongQuery_OtherQuery_GroupBox.TabStop = false;
            this.SongQuery_OtherQuery_GroupBox.Text = "其它查詢";
            // 
            // SongQuery_FavoriteQuery_ComboBox
            // 
            this.SongQuery_FavoriteQuery_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongQuery_FavoriteQuery_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_FavoriteQuery_ComboBox.FormattingEnabled = true;
            this.SongQuery_FavoriteQuery_ComboBox.Location = new System.Drawing.Point(332, 35);
            this.SongQuery_FavoriteQuery_ComboBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 14);
            this.SongQuery_FavoriteQuery_ComboBox.Name = "SongQuery_FavoriteQuery_ComboBox";
            this.SongQuery_FavoriteQuery_ComboBox.Size = new System.Drawing.Size(120, 30);
            this.SongQuery_FavoriteQuery_ComboBox.TabIndex = 3;
            this.SongQuery_FavoriteQuery_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongQuery_FavoriteQuery_ComboBox_SelectedIndexChanged);
            // 
            // SongQuery_FavoriteQuery_Label
            // 
            this.SongQuery_FavoriteQuery_Label.AutoSize = true;
            this.SongQuery_FavoriteQuery_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_FavoriteQuery_Label.Location = new System.Drawing.Point(272, 39);
            this.SongQuery_FavoriteQuery_Label.Margin = new System.Windows.Forms.Padding(10, 9, 6, 18);
            this.SongQuery_FavoriteQuery_Label.Name = "SongQuery_FavoriteQuery_Label";
            this.SongQuery_FavoriteQuery_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_FavoriteQuery_Label.TabIndex = 2;
            this.SongQuery_FavoriteQuery_Label.Text = "最愛:";
            // 
            // SongQuery_ExceptionalQuery_ComboBox
            // 
            this.SongQuery_ExceptionalQuery_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongQuery_ExceptionalQuery_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_ExceptionalQuery_ComboBox.FormattingEnabled = true;
            this.SongQuery_ExceptionalQuery_ComboBox.Location = new System.Drawing.Point(76, 35);
            this.SongQuery_ExceptionalQuery_ComboBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 14);
            this.SongQuery_ExceptionalQuery_ComboBox.Name = "SongQuery_ExceptionalQuery_ComboBox";
            this.SongQuery_ExceptionalQuery_ComboBox.Size = new System.Drawing.Size(180, 30);
            this.SongQuery_ExceptionalQuery_ComboBox.TabIndex = 1;
            this.SongQuery_ExceptionalQuery_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongQuery_ExceptionalQuery_ComboBox_SelectedIndexChanged);
            // 
            // SongQuery_ExceptionalQuery_Label
            // 
            this.SongQuery_ExceptionalQuery_Label.AutoSize = true;
            this.SongQuery_ExceptionalQuery_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_ExceptionalQuery_Label.Location = new System.Drawing.Point(16, 39);
            this.SongQuery_ExceptionalQuery_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 18);
            this.SongQuery_ExceptionalQuery_Label.Name = "SongQuery_ExceptionalQuery_Label";
            this.SongQuery_ExceptionalQuery_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_ExceptionalQuery_Label.TabIndex = 0;
            this.SongQuery_ExceptionalQuery_Label.Text = "異常:";
            // 
            // SongQuery_Query_GroupBox
            // 
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_SynonymousQuery_CheckBox);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_Clear_Button);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_Paste_Button);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_QueryFilter_ComboBox);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_QueryFilter_Label);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_FuzzyQuery_CheckBox);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_QueryValue_Label);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_Query_Button);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_QueryType_Label);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_QueryType_ComboBox);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_QueryValue_TextBox);
            this.SongQuery_Query_GroupBox.Controls.Add(this.SongQuery_QueryValue_ComboBox);
            this.SongQuery_Query_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Query_GroupBox.Location = new System.Drawing.Point(23, 23);
            this.SongQuery_Query_GroupBox.Name = "SongQuery_Query_GroupBox";
            this.SongQuery_Query_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongQuery_Query_GroupBox.Size = new System.Drawing.Size(468, 180);
            this.SongQuery_Query_GroupBox.TabIndex = 0;
            this.SongQuery_Query_GroupBox.TabStop = false;
            this.SongQuery_Query_GroupBox.Text = "歌曲查詢";
            // 
            // SongQuery_SynonymousQuery_CheckBox
            // 
            this.SongQuery_SynonymousQuery_CheckBox.AutoSize = true;
            this.SongQuery_SynonymousQuery_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_SynonymousQuery_CheckBox.Location = new System.Drawing.Point(175, 135);
            this.SongQuery_SynonymousQuery_CheckBox.Margin = new System.Windows.Forms.Padding(6, 9, 6, 16);
            this.SongQuery_SynonymousQuery_CheckBox.Name = "SongQuery_SynonymousQuery_CheckBox";
            this.SongQuery_SynonymousQuery_CheckBox.Size = new System.Drawing.Size(83, 26);
            this.SongQuery_SynonymousQuery_CheckBox.TabIndex = 8;
            this.SongQuery_SynonymousQuery_CheckBox.Text = "同義字";
            this.SongQuery_SynonymousQuery_CheckBox.UseVisualStyleBackColor = true;
            this.SongQuery_SynonymousQuery_CheckBox.CheckedChanged += new System.EventHandler(this.SongQuery_SynonymousQuery_CheckBox_CheckedChanged);
            // 
            // SongQuery_Clear_Button
            // 
            this.SongQuery_Clear_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Clear_Button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SongQuery_Clear_Button.Location = new System.Drawing.Point(392, 86);
            this.SongQuery_Clear_Button.Margin = new System.Windows.Forms.Padding(6, 6, 6, 10);
            this.SongQuery_Clear_Button.Name = "SongQuery_Clear_Button";
            this.SongQuery_Clear_Button.Size = new System.Drawing.Size(60, 30);
            this.SongQuery_Clear_Button.TabIndex = 7;
            this.SongQuery_Clear_Button.Text = "清空";
            this.SongQuery_Clear_Button.UseVisualStyleBackColor = true;
            this.SongQuery_Clear_Button.Click += new System.EventHandler(this.SongQuery_Clear_Button_Click);
            // 
            // SongQuery_Paste_Button
            // 
            this.SongQuery_Paste_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Paste_Button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SongQuery_Paste_Button.Location = new System.Drawing.Point(320, 86);
            this.SongQuery_Paste_Button.Margin = new System.Windows.Forms.Padding(6, 6, 6, 10);
            this.SongQuery_Paste_Button.Name = "SongQuery_Paste_Button";
            this.SongQuery_Paste_Button.Size = new System.Drawing.Size(60, 30);
            this.SongQuery_Paste_Button.TabIndex = 6;
            this.SongQuery_Paste_Button.Text = "貼上";
            this.SongQuery_Paste_Button.UseVisualStyleBackColor = true;
            this.SongQuery_Paste_Button.Click += new System.EventHandler(this.SongQuery_Paste_Button_Click);
            // 
            // SongQuery_QueryFilter_ComboBox
            // 
            this.SongQuery_QueryFilter_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongQuery_QueryFilter_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryFilter_ComboBox.FormattingEnabled = true;
            this.SongQuery_QueryFilter_ComboBox.Location = new System.Drawing.Point(307, 40);
            this.SongQuery_QueryFilter_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongQuery_QueryFilter_ComboBox.Name = "SongQuery_QueryFilter_ComboBox";
            this.SongQuery_QueryFilter_ComboBox.Size = new System.Drawing.Size(145, 30);
            this.SongQuery_QueryFilter_ComboBox.TabIndex = 3;
            this.SongQuery_QueryFilter_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongQuery_QueryFilter_ComboBox_SelectedIndexChanged);
            // 
            // SongQuery_QueryFilter_Label
            // 
            this.SongQuery_QueryFilter_Label.AutoSize = true;
            this.SongQuery_QueryFilter_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryFilter_Label.Location = new System.Drawing.Point(247, 44);
            this.SongQuery_QueryFilter_Label.Margin = new System.Windows.Forms.Padding(10, 14, 6, 14);
            this.SongQuery_QueryFilter_Label.Name = "SongQuery_QueryFilter_Label";
            this.SongQuery_QueryFilter_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_QueryFilter_Label.TabIndex = 2;
            this.SongQuery_QueryFilter_Label.Text = "篩選:";
            // 
            // SongQuery_FuzzyQuery_CheckBox
            // 
            this.SongQuery_FuzzyQuery_CheckBox.AutoSize = true;
            this.SongQuery_FuzzyQuery_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_FuzzyQuery_CheckBox.Location = new System.Drawing.Point(270, 135);
            this.SongQuery_FuzzyQuery_CheckBox.Margin = new System.Windows.Forms.Padding(6, 9, 6, 16);
            this.SongQuery_FuzzyQuery_CheckBox.Name = "SongQuery_FuzzyQuery_CheckBox";
            this.SongQuery_FuzzyQuery_CheckBox.Size = new System.Drawing.Size(100, 26);
            this.SongQuery_FuzzyQuery_CheckBox.TabIndex = 9;
            this.SongQuery_FuzzyQuery_CheckBox.Text = "模糊查詢";
            this.SongQuery_FuzzyQuery_CheckBox.UseVisualStyleBackColor = true;
            this.SongQuery_FuzzyQuery_CheckBox.CheckedChanged += new System.EventHandler(this.SongQuery_FuzzyQuery_CheckBox_CheckedChanged);
            // 
            // SongQuery_QueryValue_Label
            // 
            this.SongQuery_QueryValue_Label.AutoSize = true;
            this.SongQuery_QueryValue_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryValue_Label.Location = new System.Drawing.Point(16, 90);
            this.SongQuery_QueryValue_Label.Margin = new System.Windows.Forms.Padding(6, 10, 6, 14);
            this.SongQuery_QueryValue_Label.Name = "SongQuery_QueryValue_Label";
            this.SongQuery_QueryValue_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_QueryValue_Label.TabIndex = 4;
            this.SongQuery_QueryValue_Label.Text = "條件:";
            // 
            // SongQuery_Query_Button
            // 
            this.SongQuery_Query_Button.AutoSize = true;
            this.SongQuery_Query_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Query_Button.Location = new System.Drawing.Point(382, 131);
            this.SongQuery_Query_Button.Margin = new System.Windows.Forms.Padding(6, 5, 6, 14);
            this.SongQuery_Query_Button.Name = "SongQuery_Query_Button";
            this.SongQuery_Query_Button.Size = new System.Drawing.Size(70, 32);
            this.SongQuery_Query_Button.TabIndex = 10;
            this.SongQuery_Query_Button.Text = "查詢";
            this.SongQuery_Query_Button.UseVisualStyleBackColor = true;
            this.SongQuery_Query_Button.Click += new System.EventHandler(this.SongQuery_Query_Button_Click);
            // 
            // SongQuery_QueryType_Label
            // 
            this.SongQuery_QueryType_Label.AutoSize = true;
            this.SongQuery_QueryType_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryType_Label.Location = new System.Drawing.Point(16, 44);
            this.SongQuery_QueryType_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongQuery_QueryType_Label.Name = "SongQuery_QueryType_Label";
            this.SongQuery_QueryType_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_QueryType_Label.TabIndex = 0;
            this.SongQuery_QueryType_Label.Text = "類型:";
            // 
            // SongQuery_QueryType_ComboBox
            // 
            this.SongQuery_QueryType_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongQuery_QueryType_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryType_ComboBox.FormattingEnabled = true;
            this.SongQuery_QueryType_ComboBox.Location = new System.Drawing.Point(76, 40);
            this.SongQuery_QueryType_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongQuery_QueryType_ComboBox.Name = "SongQuery_QueryType_ComboBox";
            this.SongQuery_QueryType_ComboBox.Size = new System.Drawing.Size(155, 30);
            this.SongQuery_QueryType_ComboBox.TabIndex = 1;
            this.SongQuery_QueryType_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongQuery_QueryType_ComboBox_SelectedIndexChanged);
            // 
            // SongQuery_QueryValue_TextBox
            // 
            this.SongQuery_QueryValue_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryValue_TextBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SongQuery_QueryValue_TextBox.Location = new System.Drawing.Point(76, 86);
            this.SongQuery_QueryValue_TextBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 10);
            this.SongQuery_QueryValue_TextBox.Name = "SongQuery_QueryValue_TextBox";
            this.SongQuery_QueryValue_TextBox.Size = new System.Drawing.Size(232, 30);
            this.SongQuery_QueryValue_TextBox.TabIndex = 5;
            this.SongQuery_QueryValue_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SongQuery_QueryValue_TextBox_KeyPress);
            // 
            // SongQuery_QueryValue_ComboBox
            // 
            this.SongQuery_QueryValue_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongQuery_QueryValue_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryValue_ComboBox.FormattingEnabled = true;
            this.SongQuery_QueryValue_ComboBox.Location = new System.Drawing.Point(76, 86);
            this.SongQuery_QueryValue_ComboBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 10);
            this.SongQuery_QueryValue_ComboBox.Name = "SongQuery_QueryValue_ComboBox";
            this.SongQuery_QueryValue_ComboBox.Size = new System.Drawing.Size(232, 30);
            this.SongQuery_QueryValue_ComboBox.TabIndex = 6;
            this.SongQuery_QueryValue_ComboBox.Visible = false;
            // 
            // SongQuery_Statistics_GroupBox
            // 
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics12Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics12_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics11Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics10Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics9Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics8Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics7Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics11_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics10_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics9_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics8_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics7_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics6Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics5Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics4Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics3Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics2Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics1Value_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics6_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics5_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics4_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics3_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics2_Label);
            this.SongQuery_Statistics_GroupBox.Controls.Add(this.SongQuery_Statistics1_Label);
            this.SongQuery_Statistics_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics_GroupBox.Location = new System.Drawing.Point(507, 23);
            this.SongQuery_Statistics_GroupBox.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.SongQuery_Statistics_GroupBox.Name = "SongQuery_Statistics_GroupBox";
            this.SongQuery_Statistics_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongQuery_Statistics_GroupBox.Size = new System.Drawing.Size(468, 270);
            this.SongQuery_Statistics_GroupBox.TabIndex = 2;
            this.SongQuery_Statistics_GroupBox.TabStop = false;
            this.SongQuery_Statistics_GroupBox.Text = "歌庫統計";
            // 
            // SongQuery_Statistics12Value_Label
            // 
            this.SongQuery_Statistics12Value_Label.AutoSize = true;
            this.SongQuery_Statistics12Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics12Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics12Value_Label.Location = new System.Drawing.Point(344, 44);
            this.SongQuery_Statistics12Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics12Value_Label.Name = "SongQuery_Statistics12Value_Label";
            this.SongQuery_Statistics12Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics12Value_Label.TabIndex = 13;
            this.SongQuery_Statistics12Value_Label.Text = "0 個";
            // 
            // SongQuery_Statistics12_Label
            // 
            this.SongQuery_Statistics12_Label.AutoSize = true;
            this.SongQuery_Statistics12_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics12_Label.ForeColor = System.Drawing.Color.SaddleBrown;
            this.SongQuery_Statistics12_Label.Location = new System.Drawing.Point(250, 44);
            this.SongQuery_Statistics12_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics12_Label.Name = "SongQuery_Statistics12_Label";
            this.SongQuery_Statistics12_Label.Size = new System.Drawing.Size(82, 22);
            this.SongQuery_Statistics12_Label.TabIndex = 12;
            this.SongQuery_Statistics12_Label.Text = "歌庫檔案:";
            // 
            // SongQuery_Statistics11Value_Label
            // 
            this.SongQuery_Statistics11Value_Label.AutoSize = true;
            this.SongQuery_Statistics11Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics11Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics11Value_Label.Location = new System.Drawing.Point(344, 224);
            this.SongQuery_Statistics11Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics11Value_Label.Name = "SongQuery_Statistics11Value_Label";
            this.SongQuery_Statistics11Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics11Value_Label.TabIndex = 23;
            this.SongQuery_Statistics11Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics10Value_Label
            // 
            this.SongQuery_Statistics10Value_Label.AutoSize = true;
            this.SongQuery_Statistics10Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics10Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics10Value_Label.Location = new System.Drawing.Point(344, 188);
            this.SongQuery_Statistics10Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics10Value_Label.Name = "SongQuery_Statistics10Value_Label";
            this.SongQuery_Statistics10Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics10Value_Label.TabIndex = 21;
            this.SongQuery_Statistics10Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics9Value_Label
            // 
            this.SongQuery_Statistics9Value_Label.AutoSize = true;
            this.SongQuery_Statistics9Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics9Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics9Value_Label.Location = new System.Drawing.Point(344, 152);
            this.SongQuery_Statistics9Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics9Value_Label.Name = "SongQuery_Statistics9Value_Label";
            this.SongQuery_Statistics9Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics9Value_Label.TabIndex = 19;
            this.SongQuery_Statistics9Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics8Value_Label
            // 
            this.SongQuery_Statistics8Value_Label.AutoSize = true;
            this.SongQuery_Statistics8Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics8Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics8Value_Label.Location = new System.Drawing.Point(344, 116);
            this.SongQuery_Statistics8Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics8Value_Label.Name = "SongQuery_Statistics8Value_Label";
            this.SongQuery_Statistics8Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics8Value_Label.TabIndex = 17;
            this.SongQuery_Statistics8Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics7Value_Label
            // 
            this.SongQuery_Statistics7Value_Label.AutoSize = true;
            this.SongQuery_Statistics7Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics7Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics7Value_Label.Location = new System.Drawing.Point(344, 80);
            this.SongQuery_Statistics7Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics7Value_Label.Name = "SongQuery_Statistics7Value_Label";
            this.SongQuery_Statistics7Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics7Value_Label.TabIndex = 15;
            this.SongQuery_Statistics7Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics11_Label
            // 
            this.SongQuery_Statistics11_Label.AutoSize = true;
            this.SongQuery_Statistics11_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics11_Label.Location = new System.Drawing.Point(250, 224);
            this.SongQuery_Statistics11_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics11_Label.Name = "SongQuery_Statistics11_Label";
            this.SongQuery_Statistics11_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics11_Label.TabIndex = 22;
            this.SongQuery_Statistics11_Label.Text = "其它:";
            // 
            // SongQuery_Statistics10_Label
            // 
            this.SongQuery_Statistics10_Label.AutoSize = true;
            this.SongQuery_Statistics10_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics10_Label.Location = new System.Drawing.Point(250, 188);
            this.SongQuery_Statistics10_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics10_Label.Name = "SongQuery_Statistics10_Label";
            this.SongQuery_Statistics10_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics10_Label.TabIndex = 20;
            this.SongQuery_Statistics10_Label.Text = "兒歌:";
            // 
            // SongQuery_Statistics9_Label
            // 
            this.SongQuery_Statistics9_Label.AutoSize = true;
            this.SongQuery_Statistics9_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics9_Label.Location = new System.Drawing.Point(250, 152);
            this.SongQuery_Statistics9_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics9_Label.Name = "SongQuery_Statistics9_Label";
            this.SongQuery_Statistics9_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics9_Label.TabIndex = 18;
            this.SongQuery_Statistics9_Label.Text = "韓語:";
            // 
            // SongQuery_Statistics8_Label
            // 
            this.SongQuery_Statistics8_Label.AutoSize = true;
            this.SongQuery_Statistics8_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics8_Label.Location = new System.Drawing.Point(250, 116);
            this.SongQuery_Statistics8_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics8_Label.Name = "SongQuery_Statistics8_Label";
            this.SongQuery_Statistics8_Label.Size = new System.Drawing.Size(82, 22);
            this.SongQuery_Statistics8_Label.TabIndex = 16;
            this.SongQuery_Statistics8_Label.Text = "原住民語:";
            // 
            // SongQuery_Statistics7_Label
            // 
            this.SongQuery_Statistics7_Label.AutoSize = true;
            this.SongQuery_Statistics7_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics7_Label.Location = new System.Drawing.Point(250, 80);
            this.SongQuery_Statistics7_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics7_Label.Name = "SongQuery_Statistics7_Label";
            this.SongQuery_Statistics7_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics7_Label.TabIndex = 14;
            this.SongQuery_Statistics7_Label.Text = "客語:";
            // 
            // SongQuery_Statistics6Value_Label
            // 
            this.SongQuery_Statistics6Value_Label.AutoSize = true;
            this.SongQuery_Statistics6Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics6Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics6Value_Label.Location = new System.Drawing.Point(110, 224);
            this.SongQuery_Statistics6Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics6Value_Label.Name = "SongQuery_Statistics6Value_Label";
            this.SongQuery_Statistics6Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics6Value_Label.TabIndex = 11;
            this.SongQuery_Statistics6Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics5Value_Label
            // 
            this.SongQuery_Statistics5Value_Label.AutoSize = true;
            this.SongQuery_Statistics5Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics5Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics5Value_Label.Location = new System.Drawing.Point(110, 188);
            this.SongQuery_Statistics5Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics5Value_Label.Name = "SongQuery_Statistics5Value_Label";
            this.SongQuery_Statistics5Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics5Value_Label.TabIndex = 9;
            this.SongQuery_Statistics5Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics4Value_Label
            // 
            this.SongQuery_Statistics4Value_Label.AutoSize = true;
            this.SongQuery_Statistics4Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics4Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics4Value_Label.Location = new System.Drawing.Point(110, 152);
            this.SongQuery_Statistics4Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics4Value_Label.Name = "SongQuery_Statistics4Value_Label";
            this.SongQuery_Statistics4Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics4Value_Label.TabIndex = 7;
            this.SongQuery_Statistics4Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics3Value_Label
            // 
            this.SongQuery_Statistics3Value_Label.AutoSize = true;
            this.SongQuery_Statistics3Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics3Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics3Value_Label.Location = new System.Drawing.Point(110, 116);
            this.SongQuery_Statistics3Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics3Value_Label.Name = "SongQuery_Statistics3Value_Label";
            this.SongQuery_Statistics3Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics3Value_Label.TabIndex = 5;
            this.SongQuery_Statistics3Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics2Value_Label
            // 
            this.SongQuery_Statistics2Value_Label.AutoSize = true;
            this.SongQuery_Statistics2Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics2Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics2Value_Label.Location = new System.Drawing.Point(110, 80);
            this.SongQuery_Statistics2Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics2Value_Label.Name = "SongQuery_Statistics2Value_Label";
            this.SongQuery_Statistics2Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics2Value_Label.TabIndex = 3;
            this.SongQuery_Statistics2Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics1Value_Label
            // 
            this.SongQuery_Statistics1Value_Label.AutoSize = true;
            this.SongQuery_Statistics1Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics1Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongQuery_Statistics1Value_Label.Location = new System.Drawing.Point(110, 44);
            this.SongQuery_Statistics1Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics1Value_Label.Name = "SongQuery_Statistics1Value_Label";
            this.SongQuery_Statistics1Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongQuery_Statistics1Value_Label.TabIndex = 1;
            this.SongQuery_Statistics1Value_Label.Text = "0 首";
            // 
            // SongQuery_Statistics6_Label
            // 
            this.SongQuery_Statistics6_Label.AutoSize = true;
            this.SongQuery_Statistics6_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics6_Label.Location = new System.Drawing.Point(16, 224);
            this.SongQuery_Statistics6_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics6_Label.Name = "SongQuery_Statistics6_Label";
            this.SongQuery_Statistics6_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics6_Label.TabIndex = 10;
            this.SongQuery_Statistics6_Label.Text = "英語:";
            // 
            // SongQuery_Statistics5_Label
            // 
            this.SongQuery_Statistics5_Label.AutoSize = true;
            this.SongQuery_Statistics5_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics5_Label.Location = new System.Drawing.Point(16, 188);
            this.SongQuery_Statistics5_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics5_Label.Name = "SongQuery_Statistics5_Label";
            this.SongQuery_Statistics5_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics5_Label.TabIndex = 8;
            this.SongQuery_Statistics5_Label.Text = "日語:";
            // 
            // SongQuery_Statistics4_Label
            // 
            this.SongQuery_Statistics4_Label.AutoSize = true;
            this.SongQuery_Statistics4_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics4_Label.Location = new System.Drawing.Point(16, 152);
            this.SongQuery_Statistics4_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics4_Label.Name = "SongQuery_Statistics4_Label";
            this.SongQuery_Statistics4_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics4_Label.TabIndex = 6;
            this.SongQuery_Statistics4_Label.Text = "粵語:";
            // 
            // SongQuery_Statistics3_Label
            // 
            this.SongQuery_Statistics3_Label.AutoSize = true;
            this.SongQuery_Statistics3_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics3_Label.Location = new System.Drawing.Point(16, 116);
            this.SongQuery_Statistics3_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics3_Label.Name = "SongQuery_Statistics3_Label";
            this.SongQuery_Statistics3_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics3_Label.TabIndex = 4;
            this.SongQuery_Statistics3_Label.Text = "台語:";
            // 
            // SongQuery_Statistics2_Label
            // 
            this.SongQuery_Statistics2_Label.AutoSize = true;
            this.SongQuery_Statistics2_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics2_Label.Location = new System.Drawing.Point(16, 80);
            this.SongQuery_Statistics2_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics2_Label.Name = "SongQuery_Statistics2_Label";
            this.SongQuery_Statistics2_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics2_Label.TabIndex = 2;
            this.SongQuery_Statistics2_Label.Text = "國語:";
            // 
            // SongQuery_Statistics1_Label
            // 
            this.SongQuery_Statistics1_Label.AutoSize = true;
            this.SongQuery_Statistics1_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_Statistics1_Label.ForeColor = System.Drawing.Color.SaddleBrown;
            this.SongQuery_Statistics1_Label.Location = new System.Drawing.Point(16, 44);
            this.SongQuery_Statistics1_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SongQuery_Statistics1_Label.Name = "SongQuery_Statistics1_Label";
            this.SongQuery_Statistics1_Label.Size = new System.Drawing.Size(48, 22);
            this.SongQuery_Statistics1_Label.TabIndex = 0;
            this.SongQuery_Statistics1_Label.Text = "總計:";
            // 
            // SongQuery_DataGridView
            // 
            this.SongQuery_DataGridView.AllowUserToAddRows = false;
            this.SongQuery_DataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.SongQuery_DataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.SongQuery_DataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SongQuery_DataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SongQuery_DataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SongQuery_DataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.SongQuery_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SongQuery_DataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.SongQuery_DataGridView.EnableHeadersVisualStyles = false;
            this.SongQuery_DataGridView.Location = new System.Drawing.Point(23, 365);
            this.SongQuery_DataGridView.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.SongQuery_DataGridView.Name = "SongQuery_DataGridView";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_DataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.SongQuery_DataGridView.RowTemplate.Height = 27;
            this.SongQuery_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SongQuery_DataGridView.Size = new System.Drawing.Size(952, 296);
            this.SongQuery_DataGridView.TabIndex = 4;
            this.SongQuery_DataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.SongQuery_DataGridView_CellBeginEdit);
            this.SongQuery_DataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.SongQuery_DataGridView_CellEndEdit);
            this.SongQuery_DataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.SongQuery_DataGridView_CellFormatting);
            this.SongQuery_DataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SongQuery_DataGridView_CellMouseClick);
            this.SongQuery_DataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.SongQuery_DataGridView_CellValidating);
            this.SongQuery_DataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.SongQuery_DataGridView_EditingControlShowing);
            this.SongQuery_DataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SongQuery_DataGridView_KeyDown);
            // 
            // SongQuery_QueryStatus_GroupBox
            // 
            this.SongQuery_QueryStatus_GroupBox.Controls.Add(this.SongQuery_EditMode_CheckBox);
            this.SongQuery_QueryStatus_GroupBox.Controls.Add(this.SongQuery_QueryStatus_Label);
            this.SongQuery_QueryStatus_GroupBox.Font = new System.Drawing.Font("新細明體", 1.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryStatus_GroupBox.Location = new System.Drawing.Point(23, 310);
            this.SongQuery_QueryStatus_GroupBox.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.SongQuery_QueryStatus_GroupBox.Name = "SongQuery_QueryStatus_GroupBox";
            this.SongQuery_QueryStatus_GroupBox.Padding = new System.Windows.Forms.Padding(0, 0, 6, 0);
            this.SongQuery_QueryStatus_GroupBox.Size = new System.Drawing.Size(952, 35);
            this.SongQuery_QueryStatus_GroupBox.TabIndex = 3;
            this.SongQuery_QueryStatus_GroupBox.TabStop = false;
            // 
            // SongQuery_EditMode_CheckBox
            // 
            this.SongQuery_EditMode_CheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SongQuery_EditMode_CheckBox.AutoSize = true;
            this.SongQuery_EditMode_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_EditMode_CheckBox.Location = new System.Drawing.Point(840, 6);
            this.SongQuery_EditMode_CheckBox.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.SongQuery_EditMode_CheckBox.Name = "SongQuery_EditMode_CheckBox";
            this.SongQuery_EditMode_CheckBox.Size = new System.Drawing.Size(100, 26);
            this.SongQuery_EditMode_CheckBox.TabIndex = 1;
            this.SongQuery_EditMode_CheckBox.Text = "編輯模式";
            this.SongQuery_EditMode_CheckBox.UseVisualStyleBackColor = true;
            this.SongQuery_EditMode_CheckBox.CheckedChanged += new System.EventHandler(this.SongQuery_EditMode_CheckBox_CheckedChanged);
            // 
            // SongQuery_QueryStatus_Label
            // 
            this.SongQuery_QueryStatus_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongQuery_QueryStatus_Label.ForeColor = System.Drawing.Color.Red;
            this.SongQuery_QueryStatus_Label.Location = new System.Drawing.Point(0, 0);
            this.SongQuery_QueryStatus_Label.Margin = new System.Windows.Forms.Padding(6);
            this.SongQuery_QueryStatus_Label.Name = "SongQuery_QueryStatus_Label";
            this.SongQuery_QueryStatus_Label.Size = new System.Drawing.Size(828, 35);
            this.SongQuery_QueryStatus_Label.TabIndex = 0;
            this.SongQuery_QueryStatus_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SongQuery_QueryStatus_Label.UseMnemonic = false;
            // 
            // SongAdd_TabPage
            // 
            this.SongAdd_TabPage.Controls.Add(this.SongAdd_DragDrop_Label);
            this.SongAdd_TabPage.Controls.Add(this.SongAdd_Add_Button);
            this.SongAdd_TabPage.Controls.Add(this.SongAdd_SpecialStr_GroupBox);
            this.SongAdd_TabPage.Controls.Add(this.SongAdd_DataGridView);
            this.SongAdd_TabPage.Controls.Add(this.SongAdd_Tooltip_GroupBox);
            this.SongAdd_TabPage.Controls.Add(this.SongAdd_Save_Button);
            this.SongAdd_TabPage.Controls.Add(this.SongAdd_DefaultSongInfo_GroupBox);
            this.SongAdd_TabPage.Controls.Add(this.SongAdd_SongAddCfg_GroupBox);
            this.SongAdd_TabPage.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongAdd_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongAdd_TabPage.Name = "SongAdd_TabPage";
            this.SongAdd_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.SongAdd_TabPage.Size = new System.Drawing.Size(998, 684);
            this.SongAdd_TabPage.TabIndex = 3;
            this.SongAdd_TabPage.Text = "加歌頁面";
            this.SongAdd_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongAdd_DragDrop_Label
            // 
            this.SongAdd_DragDrop_Label.AllowDrop = true;
            this.SongAdd_DragDrop_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SongAdd_DragDrop_Label.BackColor = System.Drawing.SystemColors.Control;
            this.SongAdd_DragDrop_Label.Font = new System.Drawing.Font("微軟正黑體", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DragDrop_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongAdd_DragDrop_Label.Location = new System.Drawing.Point(108, 485);
            this.SongAdd_DragDrop_Label.Margin = new System.Windows.Forms.Padding(88, 0, 88, 0);
            this.SongAdd_DragDrop_Label.Name = "SongAdd_DragDrop_Label";
            this.SongAdd_DragDrop_Label.Size = new System.Drawing.Size(782, 50);
            this.SongAdd_DragDrop_Label.TabIndex = 7;
            this.SongAdd_DragDrop_Label.Text = "請將要加入的歌曲檔案或資料夾拖曳至此處";
            this.SongAdd_DragDrop_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SongAdd_DragDrop_Label.DragDrop += new System.Windows.Forms.DragEventHandler(this.SongAdd_DragDrop);
            this.SongAdd_DragDrop_Label.DragEnter += new System.Windows.Forms.DragEventHandler(this.SongAdd_DragEnter);
            // 
            // SongAdd_Add_Button
            // 
            this.SongAdd_Add_Button.AutoSize = true;
            this.SongAdd_Add_Button.Enabled = false;
            this.SongAdd_Add_Button.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_Add_Button.ForeColor = System.Drawing.Color.DarkGreen;
            this.SongAdd_Add_Button.Location = new System.Drawing.Point(743, 310);
            this.SongAdd_Add_Button.Margin = new System.Windows.Forms.Padding(6, 14, 6, 6);
            this.SongAdd_Add_Button.Name = "SongAdd_Add_Button";
            this.SongAdd_Add_Button.Size = new System.Drawing.Size(110, 35);
            this.SongAdd_Add_Button.TabIndex = 4;
            this.SongAdd_Add_Button.Text = "加入歌庫";
            this.SongAdd_Add_Button.UseVisualStyleBackColor = true;
            this.SongAdd_Add_Button.Click += new System.EventHandler(this.SongAdd_Add_Button_Click);
            // 
            // SongAdd_SpecialStr_GroupBox
            // 
            this.SongAdd_SpecialStr_GroupBox.Controls.Add(this.SongAdd_SpecialStr_TextBox);
            this.SongAdd_SpecialStr_GroupBox.Controls.Add(this.SongAdd_SpecialStr_Button);
            this.SongAdd_SpecialStr_GroupBox.Controls.Add(this.SongAdd_SpecialStr_ListBox);
            this.SongAdd_SpecialStr_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_SpecialStr_GroupBox.Location = new System.Drawing.Point(433, 23);
            this.SongAdd_SpecialStr_GroupBox.Margin = new System.Windows.Forms.Padding(14, 20, 3, 3);
            this.SongAdd_SpecialStr_GroupBox.Name = "SongAdd_SpecialStr_GroupBox";
            this.SongAdd_SpecialStr_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongAdd_SpecialStr_GroupBox.Size = new System.Drawing.Size(245, 270);
            this.SongAdd_SpecialStr_GroupBox.TabIndex = 1;
            this.SongAdd_SpecialStr_GroupBox.TabStop = false;
            this.SongAdd_SpecialStr_GroupBox.Text = "特殊歌手及歌曲名稱";
            // 
            // SongAdd_SpecialStr_TextBox
            // 
            this.SongAdd_SpecialStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_SpecialStr_TextBox.Location = new System.Drawing.Point(16, 229);
            this.SongAdd_SpecialStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongAdd_SpecialStr_TextBox.Name = "SongAdd_SpecialStr_TextBox";
            this.SongAdd_SpecialStr_TextBox.Size = new System.Drawing.Size(131, 30);
            this.SongAdd_SpecialStr_TextBox.TabIndex = 1;
            this.SongAdd_SpecialStr_TextBox.Enter += new System.EventHandler(this.SongAdd_SpecialStr_TextBox_Enter);
            this.SongAdd_SpecialStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongAdd_SpecialStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_HasInvalidChar_TextBox_Validating);
            // 
            // SongAdd_SpecialStr_Button
            // 
            this.SongAdd_SpecialStr_Button.AutoSize = true;
            this.SongAdd_SpecialStr_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_SpecialStr_Button.Location = new System.Drawing.Point(159, 229);
            this.SongAdd_SpecialStr_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongAdd_SpecialStr_Button.Name = "SongAdd_SpecialStr_Button";
            this.SongAdd_SpecialStr_Button.Size = new System.Drawing.Size(70, 32);
            this.SongAdd_SpecialStr_Button.TabIndex = 2;
            this.SongAdd_SpecialStr_Button.Text = "加入";
            this.SongAdd_SpecialStr_Button.UseVisualStyleBackColor = true;
            this.SongAdd_SpecialStr_Button.Click += new System.EventHandler(this.SongAdd_SpecialStr_Button_Click);
            // 
            // SongAdd_SpecialStr_ListBox
            // 
            this.SongAdd_SpecialStr_ListBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_SpecialStr_ListBox.FormattingEnabled = true;
            this.SongAdd_SpecialStr_ListBox.ItemHeight = 22;
            this.SongAdd_SpecialStr_ListBox.Location = new System.Drawing.Point(16, 40);
            this.SongAdd_SpecialStr_ListBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongAdd_SpecialStr_ListBox.Name = "SongAdd_SpecialStr_ListBox";
            this.SongAdd_SpecialStr_ListBox.Size = new System.Drawing.Size(213, 180);
            this.SongAdd_SpecialStr_ListBox.TabIndex = 0;
            this.SongAdd_SpecialStr_ListBox.Enter += new System.EventHandler(this.SongAdd_SpecialStr_ListBox_Enter);
            // 
            // SongAdd_DataGridView
            // 
            this.SongAdd_DataGridView.AllowDrop = true;
            this.SongAdd_DataGridView.AllowUserToAddRows = false;
            this.SongAdd_DataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            this.SongAdd_DataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.SongAdd_DataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SongAdd_DataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SongAdd_DataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SongAdd_DataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.SongAdd_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SongAdd_DataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.SongAdd_DataGridView.EnableHeadersVisualStyles = false;
            this.SongAdd_DataGridView.Location = new System.Drawing.Point(23, 365);
            this.SongAdd_DataGridView.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.SongAdd_DataGridView.Name = "SongAdd_DataGridView";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DataGridView.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.SongAdd_DataGridView.RowTemplate.Height = 27;
            this.SongAdd_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SongAdd_DataGridView.Size = new System.Drawing.Size(952, 291);
            this.SongAdd_DataGridView.TabIndex = 6;
            this.SongAdd_DataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.SongAdd_DataGridView_CellEndEdit);
            this.SongAdd_DataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.SongAdd_DataGridView_CellFormatting);
            this.SongAdd_DataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SongAdd_DataGridView_CellMouseClick);
            this.SongAdd_DataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.SongAdd_DataGridView_CellValidating);
            this.SongAdd_DataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.SongAdd_DataGridView_EditingControlShowing);
            this.SongAdd_DataGridView.DragDrop += new System.Windows.Forms.DragEventHandler(this.SongAdd_DragDrop);
            this.SongAdd_DataGridView.DragEnter += new System.Windows.Forms.DragEventHandler(this.SongAdd_DragEnter);
            this.SongAdd_DataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SongAdd_DataGridView_KeyDown);
            // 
            // SongAdd_Tooltip_GroupBox
            // 
            this.SongAdd_Tooltip_GroupBox.Controls.Add(this.SongAdd_Tooltip_Label);
            this.SongAdd_Tooltip_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 1.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_Tooltip_GroupBox.Location = new System.Drawing.Point(23, 310);
            this.SongAdd_Tooltip_GroupBox.Margin = new System.Windows.Forms.Padding(3, 14, 6, 3);
            this.SongAdd_Tooltip_GroupBox.Name = "SongAdd_Tooltip_GroupBox";
            this.SongAdd_Tooltip_GroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.SongAdd_Tooltip_GroupBox.Size = new System.Drawing.Size(708, 35);
            this.SongAdd_Tooltip_GroupBox.TabIndex = 3;
            this.SongAdd_Tooltip_GroupBox.TabStop = false;
            // 
            // SongAdd_Tooltip_Label
            // 
            this.SongAdd_Tooltip_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_Tooltip_Label.ForeColor = System.Drawing.Color.Red;
            this.SongAdd_Tooltip_Label.Location = new System.Drawing.Point(0, 0);
            this.SongAdd_Tooltip_Label.Margin = new System.Windows.Forms.Padding(0);
            this.SongAdd_Tooltip_Label.Name = "SongAdd_Tooltip_Label";
            this.SongAdd_Tooltip_Label.Size = new System.Drawing.Size(708, 35);
            this.SongAdd_Tooltip_Label.TabIndex = 0;
            this.SongAdd_Tooltip_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SongAdd_Tooltip_Label.UseMnemonic = false;
            // 
            // SongAdd_Save_Button
            // 
            this.SongAdd_Save_Button.AutoSize = true;
            this.SongAdd_Save_Button.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_Save_Button.Location = new System.Drawing.Point(865, 310);
            this.SongAdd_Save_Button.Margin = new System.Windows.Forms.Padding(6, 14, 6, 6);
            this.SongAdd_Save_Button.Name = "SongAdd_Save_Button";
            this.SongAdd_Save_Button.Size = new System.Drawing.Size(110, 35);
            this.SongAdd_Save_Button.TabIndex = 5;
            this.SongAdd_Save_Button.Text = "儲存設定";
            this.SongAdd_Save_Button.UseVisualStyleBackColor = true;
            this.SongAdd_Save_Button.Click += new System.EventHandler(this.SongAdd_Save_Button_Click);
            // 
            // SongAdd_DefaultSongInfo_GroupBox
            // 
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSongVolume_TextBox);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSongType_ComboBox);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSongTrack_ComboBox);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSingerType_ComboBox);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSongLang_ComboBox);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSongVolume_Label);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSongType_Label);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSongTrack_Label);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSingerType_Label);
            this.SongAdd_DefaultSongInfo_GroupBox.Controls.Add(this.SongAdd_DefaultSongLang_Label);
            this.SongAdd_DefaultSongInfo_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongInfo_GroupBox.Location = new System.Drawing.Point(695, 23);
            this.SongAdd_DefaultSongInfo_GroupBox.Margin = new System.Windows.Forms.Padding(14, 3, 3, 3);
            this.SongAdd_DefaultSongInfo_GroupBox.Name = "SongAdd_DefaultSongInfo_GroupBox";
            this.SongAdd_DefaultSongInfo_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongAdd_DefaultSongInfo_GroupBox.Size = new System.Drawing.Size(280, 270);
            this.SongAdd_DefaultSongInfo_GroupBox.TabIndex = 2;
            this.SongAdd_DefaultSongInfo_GroupBox.TabStop = false;
            this.SongAdd_DefaultSongInfo_GroupBox.Text = "預設歌曲資訊";
            // 
            // SongAdd_DefaultSongVolume_TextBox
            // 
            this.SongAdd_DefaultSongVolume_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongVolume_TextBox.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.SongAdd_DefaultSongVolume_TextBox.Location = new System.Drawing.Point(110, 228);
            this.SongAdd_DefaultSongVolume_TextBox.Margin = new System.Windows.Forms.Padding(6, 7, 6, 9);
            this.SongAdd_DefaultSongVolume_TextBox.MaxLength = 3;
            this.SongAdd_DefaultSongVolume_TextBox.Name = "SongAdd_DefaultSongVolume_TextBox";
            this.SongAdd_DefaultSongVolume_TextBox.Size = new System.Drawing.Size(154, 30);
            this.SongAdd_DefaultSongVolume_TextBox.TabIndex = 9;
            this.SongAdd_DefaultSongVolume_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongAdd_DefaultSongVolume_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongAdd_DefaultSongVolume_TextBox_Validating);
            // 
            // SongAdd_DefaultSongType_ComboBox
            // 
            this.SongAdd_DefaultSongType_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongAdd_DefaultSongType_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongType_ComboBox.FormattingEnabled = true;
            this.SongAdd_DefaultSongType_ComboBox.Location = new System.Drawing.Point(110, 181);
            this.SongAdd_DefaultSongType_ComboBox.Margin = new System.Windows.Forms.Padding(6, 7, 6, 10);
            this.SongAdd_DefaultSongType_ComboBox.Name = "SongAdd_DefaultSongType_ComboBox";
            this.SongAdd_DefaultSongType_ComboBox.Size = new System.Drawing.Size(154, 30);
            this.SongAdd_DefaultSongType_ComboBox.TabIndex = 7;
            this.SongAdd_DefaultSongType_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongAdd_DefaultSongInfo_ComboBox_SelectedIndexChanged);
            // 
            // SongAdd_DefaultSongTrack_ComboBox
            // 
            this.SongAdd_DefaultSongTrack_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongAdd_DefaultSongTrack_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongTrack_ComboBox.FormattingEnabled = true;
            this.SongAdd_DefaultSongTrack_ComboBox.Location = new System.Drawing.Point(110, 134);
            this.SongAdd_DefaultSongTrack_ComboBox.Margin = new System.Windows.Forms.Padding(6, 7, 6, 10);
            this.SongAdd_DefaultSongTrack_ComboBox.Name = "SongAdd_DefaultSongTrack_ComboBox";
            this.SongAdd_DefaultSongTrack_ComboBox.Size = new System.Drawing.Size(154, 30);
            this.SongAdd_DefaultSongTrack_ComboBox.TabIndex = 5;
            this.SongAdd_DefaultSongTrack_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongAdd_DefaultSongInfo_ComboBox_SelectedIndexChanged);
            // 
            // SongAdd_DefaultSingerType_ComboBox
            // 
            this.SongAdd_DefaultSingerType_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongAdd_DefaultSingerType_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSingerType_ComboBox.FormattingEnabled = true;
            this.SongAdd_DefaultSingerType_ComboBox.Location = new System.Drawing.Point(110, 87);
            this.SongAdd_DefaultSingerType_ComboBox.Margin = new System.Windows.Forms.Padding(6, 7, 6, 10);
            this.SongAdd_DefaultSingerType_ComboBox.Name = "SongAdd_DefaultSingerType_ComboBox";
            this.SongAdd_DefaultSingerType_ComboBox.Size = new System.Drawing.Size(154, 30);
            this.SongAdd_DefaultSingerType_ComboBox.TabIndex = 3;
            this.SongAdd_DefaultSingerType_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongAdd_DefaultSongInfo_ComboBox_SelectedIndexChanged);
            // 
            // SongAdd_DefaultSongLang_ComboBox
            // 
            this.SongAdd_DefaultSongLang_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongAdd_DefaultSongLang_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongLang_ComboBox.FormattingEnabled = true;
            this.SongAdd_DefaultSongLang_ComboBox.Location = new System.Drawing.Point(110, 40);
            this.SongAdd_DefaultSongLang_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongAdd_DefaultSongLang_ComboBox.Name = "SongAdd_DefaultSongLang_ComboBox";
            this.SongAdd_DefaultSongLang_ComboBox.Size = new System.Drawing.Size(154, 30);
            this.SongAdd_DefaultSongLang_ComboBox.TabIndex = 1;
            this.SongAdd_DefaultSongLang_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongAdd_DefaultSongInfo_ComboBox_SelectedIndexChanged);
            // 
            // SongAdd_DefaultSongVolume_Label
            // 
            this.SongAdd_DefaultSongVolume_Label.AutoSize = true;
            this.SongAdd_DefaultSongVolume_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongVolume_Label.Location = new System.Drawing.Point(16, 232);
            this.SongAdd_DefaultSongVolume_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 13);
            this.SongAdd_DefaultSongVolume_Label.Name = "SongAdd_DefaultSongVolume_Label";
            this.SongAdd_DefaultSongVolume_Label.Size = new System.Drawing.Size(82, 22);
            this.SongAdd_DefaultSongVolume_Label.TabIndex = 8;
            this.SongAdd_DefaultSongVolume_Label.Text = "歌曲音量:";
            // 
            // SongAdd_DefaultSongType_Label
            // 
            this.SongAdd_DefaultSongType_Label.AutoSize = true;
            this.SongAdd_DefaultSongType_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongType_Label.Location = new System.Drawing.Point(16, 185);
            this.SongAdd_DefaultSongType_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 11);
            this.SongAdd_DefaultSongType_Label.Name = "SongAdd_DefaultSongType_Label";
            this.SongAdd_DefaultSongType_Label.Size = new System.Drawing.Size(82, 22);
            this.SongAdd_DefaultSongType_Label.TabIndex = 6;
            this.SongAdd_DefaultSongType_Label.Text = "歌曲類別:";
            // 
            // SongAdd_DefaultSongTrack_Label
            // 
            this.SongAdd_DefaultSongTrack_Label.AutoSize = true;
            this.SongAdd_DefaultSongTrack_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongTrack_Label.Location = new System.Drawing.Point(16, 138);
            this.SongAdd_DefaultSongTrack_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 11);
            this.SongAdd_DefaultSongTrack_Label.Name = "SongAdd_DefaultSongTrack_Label";
            this.SongAdd_DefaultSongTrack_Label.Size = new System.Drawing.Size(82, 22);
            this.SongAdd_DefaultSongTrack_Label.TabIndex = 4;
            this.SongAdd_DefaultSongTrack_Label.Text = "歌曲聲道:";
            // 
            // SongAdd_DefaultSingerType_Label
            // 
            this.SongAdd_DefaultSingerType_Label.AutoSize = true;
            this.SongAdd_DefaultSingerType_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSingerType_Label.Location = new System.Drawing.Point(16, 91);
            this.SongAdd_DefaultSingerType_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 11);
            this.SongAdd_DefaultSingerType_Label.Name = "SongAdd_DefaultSingerType_Label";
            this.SongAdd_DefaultSingerType_Label.Size = new System.Drawing.Size(82, 22);
            this.SongAdd_DefaultSingerType_Label.TabIndex = 2;
            this.SongAdd_DefaultSingerType_Label.Text = "歌手類別:";
            // 
            // SongAdd_DefaultSongLang_Label
            // 
            this.SongAdd_DefaultSongLang_Label.AutoSize = true;
            this.SongAdd_DefaultSongLang_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DefaultSongLang_Label.Location = new System.Drawing.Point(16, 44);
            this.SongAdd_DefaultSongLang_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 11);
            this.SongAdd_DefaultSongLang_Label.Name = "SongAdd_DefaultSongLang_Label";
            this.SongAdd_DefaultSongLang_Label.Size = new System.Drawing.Size(82, 22);
            this.SongAdd_DefaultSongLang_Label.TabIndex = 0;
            this.SongAdd_DefaultSongLang_Label.Text = "語系類別:";
            // 
            // SongAdd_SongAddCfg_GroupBox
            // 
            this.SongAdd_SongAddCfg_GroupBox.Controls.Add(this.SongAdd_UseCustomSongID_CheckBox);
            this.SongAdd_SongAddCfg_GroupBox.Controls.Add(this.SongAdd_EngSongNameFormat_CheckBox);
            this.SongAdd_SongAddCfg_GroupBox.Controls.Add(this.SongAdd_DupSongMode_ComboBox);
            this.SongAdd_SongAddCfg_GroupBox.Controls.Add(this.SongAdd_DupSongMode_Label);
            this.SongAdd_SongAddCfg_GroupBox.Controls.Add(this.SongAdd_SongIdentificationMode_ComboBox);
            this.SongAdd_SongAddCfg_GroupBox.Controls.Add(this.SongAdd_SongIdentificationMode_Label);
            this.SongAdd_SongAddCfg_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_SongAddCfg_GroupBox.Location = new System.Drawing.Point(23, 23);
            this.SongAdd_SongAddCfg_GroupBox.Name = "SongAdd_SongAddCfg_GroupBox";
            this.SongAdd_SongAddCfg_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongAdd_SongAddCfg_GroupBox.Size = new System.Drawing.Size(393, 270);
            this.SongAdd_SongAddCfg_GroupBox.TabIndex = 0;
            this.SongAdd_SongAddCfg_GroupBox.TabStop = false;
            this.SongAdd_SongAddCfg_GroupBox.Text = "加歌設定";
            // 
            // SongAdd_UseCustomSongID_CheckBox
            // 
            this.SongAdd_UseCustomSongID_CheckBox.AutoSize = true;
            this.SongAdd_UseCustomSongID_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_UseCustomSongID_CheckBox.Location = new System.Drawing.Point(20, 177);
            this.SongAdd_UseCustomSongID_CheckBox.Margin = new System.Windows.Forms.Padding(10, 6, 6, 6);
            this.SongAdd_UseCustomSongID_CheckBox.Name = "SongAdd_UseCustomSongID_CheckBox";
            this.SongAdd_UseCustomSongID_CheckBox.Size = new System.Drawing.Size(253, 26);
            this.SongAdd_UseCustomSongID_CheckBox.TabIndex = 5;
            this.SongAdd_UseCustomSongID_CheckBox.Text = "優先使用檔名自訂的歌曲編號";
            this.SongAdd_UseCustomSongID_CheckBox.UseVisualStyleBackColor = true;
            this.SongAdd_UseCustomSongID_CheckBox.CheckedChanged += new System.EventHandler(this.SongAdd_UseCustomSongID_CheckBox_CheckedChanged);
            // 
            // SongAdd_EngSongNameFormat_CheckBox
            // 
            this.SongAdd_EngSongNameFormat_CheckBox.AutoSize = true;
            this.SongAdd_EngSongNameFormat_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_EngSongNameFormat_CheckBox.Location = new System.Drawing.Point(20, 139);
            this.SongAdd_EngSongNameFormat_CheckBox.Margin = new System.Windows.Forms.Padding(10, 12, 6, 6);
            this.SongAdd_EngSongNameFormat_CheckBox.Name = "SongAdd_EngSongNameFormat_CheckBox";
            this.SongAdd_EngSongNameFormat_CheckBox.Size = new System.Drawing.Size(253, 26);
            this.SongAdd_EngSongNameFormat_CheckBox.TabIndex = 4;
            this.SongAdd_EngSongNameFormat_CheckBox.Text = "純英文歌名自動轉成首字大寫";
            this.SongAdd_EngSongNameFormat_CheckBox.UseVisualStyleBackColor = true;
            this.SongAdd_EngSongNameFormat_CheckBox.CheckedChanged += new System.EventHandler(this.SongAdd_EngSongNameFormat_CheckBox_CheckedChanged);
            // 
            // SongAdd_DupSongMode_ComboBox
            // 
            this.SongAdd_DupSongMode_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongAdd_DupSongMode_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DupSongMode_ComboBox.FormattingEnabled = true;
            this.SongAdd_DupSongMode_ComboBox.Location = new System.Drawing.Point(144, 87);
            this.SongAdd_DupSongMode_ComboBox.Margin = new System.Windows.Forms.Padding(6, 7, 6, 10);
            this.SongAdd_DupSongMode_ComboBox.Name = "SongAdd_DupSongMode_ComboBox";
            this.SongAdd_DupSongMode_ComboBox.Size = new System.Drawing.Size(233, 30);
            this.SongAdd_DupSongMode_ComboBox.TabIndex = 3;
            this.SongAdd_DupSongMode_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongAdd_DupSongMode_ComboBox_SelectedIndexChanged);
            // 
            // SongAdd_DupSongMode_Label
            // 
            this.SongAdd_DupSongMode_Label.AutoSize = true;
            this.SongAdd_DupSongMode_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_DupSongMode_Label.Location = new System.Drawing.Point(16, 91);
            this.SongAdd_DupSongMode_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 11);
            this.SongAdd_DupSongMode_Label.Name = "SongAdd_DupSongMode_Label";
            this.SongAdd_DupSongMode_Label.Size = new System.Drawing.Size(116, 22);
            this.SongAdd_DupSongMode_Label.TabIndex = 2;
            this.SongAdd_DupSongMode_Label.Text = "重複歌曲處理:";
            // 
            // SongAdd_SongIdentificationMode_ComboBox
            // 
            this.SongAdd_SongIdentificationMode_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongAdd_SongIdentificationMode_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_SongIdentificationMode_ComboBox.FormattingEnabled = true;
            this.SongAdd_SongIdentificationMode_ComboBox.Location = new System.Drawing.Point(144, 40);
            this.SongAdd_SongIdentificationMode_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongAdd_SongIdentificationMode_ComboBox.Name = "SongAdd_SongIdentificationMode_ComboBox";
            this.SongAdd_SongIdentificationMode_ComboBox.Size = new System.Drawing.Size(233, 30);
            this.SongAdd_SongIdentificationMode_ComboBox.TabIndex = 1;
            this.SongAdd_SongIdentificationMode_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongAdd_SongIdentificationMode_ComboBox_SelectedIndexChanged);
            // 
            // SongAdd_SongIdentificationMode_Label
            // 
            this.SongAdd_SongIdentificationMode_Label.AutoSize = true;
            this.SongAdd_SongIdentificationMode_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAdd_SongIdentificationMode_Label.Location = new System.Drawing.Point(16, 44);
            this.SongAdd_SongIdentificationMode_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 11);
            this.SongAdd_SongIdentificationMode_Label.Name = "SongAdd_SongIdentificationMode_Label";
            this.SongAdd_SongIdentificationMode_Label.Size = new System.Drawing.Size(116, 22);
            this.SongAdd_SongIdentificationMode_Label.TabIndex = 0;
            this.SongAdd_SongIdentificationMode_Label.Text = "歌曲辨識方式:";
            // 
            // SingerMgr_TabPage
            // 
            this.SingerMgr_TabPage.Controls.Add(this.SingerMgr_OtherQuery_GroupBox);
            this.SingerMgr_TabPage.Controls.Add(this.SingerMgr_SingerAdd_GroupBox);
            this.SingerMgr_TabPage.Controls.Add(this.SingerMgr_Manager_GroupBox);
            this.SingerMgr_TabPage.Controls.Add(this.SingerMgr_Statistics_GroupBox);
            this.SingerMgr_TabPage.Controls.Add(this.SingerMgr_Tooltip_GroupBox);
            this.SingerMgr_TabPage.Controls.Add(this.SingerMgr_DataGridView);
            this.SingerMgr_TabPage.Controls.Add(this.SingerMgr_Query_GroupBox);
            this.SingerMgr_TabPage.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SingerMgr_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SingerMgr_TabPage.Name = "SingerMgr_TabPage";
            this.SingerMgr_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.SingerMgr_TabPage.Size = new System.Drawing.Size(998, 684);
            this.SingerMgr_TabPage.TabIndex = 8;
            this.SingerMgr_TabPage.Text = "歌手管理";
            this.SingerMgr_TabPage.UseVisualStyleBackColor = true;
            // 
            // SingerMgr_OtherQuery_GroupBox
            // 
            this.SingerMgr_OtherQuery_GroupBox.Controls.Add(this.SingerMgr_QueryType_Label);
            this.SingerMgr_OtherQuery_GroupBox.Controls.Add(this.SingerMgr_QueryType_ComboBox);
            this.SingerMgr_OtherQuery_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_OtherQuery_GroupBox.Location = new System.Drawing.Point(23, 174);
            this.SingerMgr_OtherQuery_GroupBox.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.SingerMgr_OtherQuery_GroupBox.Name = "SingerMgr_OtherQuery_GroupBox";
            this.SingerMgr_OtherQuery_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SingerMgr_OtherQuery_GroupBox.Size = new System.Drawing.Size(468, 88);
            this.SingerMgr_OtherQuery_GroupBox.TabIndex = 1;
            this.SingerMgr_OtherQuery_GroupBox.TabStop = false;
            this.SingerMgr_OtherQuery_GroupBox.Text = "其它查詢";
            // 
            // SingerMgr_QueryType_Label
            // 
            this.SingerMgr_QueryType_Label.AutoSize = true;
            this.SingerMgr_QueryType_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_QueryType_Label.Location = new System.Drawing.Point(16, 40);
            this.SingerMgr_QueryType_Label.Margin = new System.Windows.Forms.Padding(6, 10, 6, 23);
            this.SingerMgr_QueryType_Label.Name = "SingerMgr_QueryType_Label";
            this.SingerMgr_QueryType_Label.Size = new System.Drawing.Size(82, 22);
            this.SingerMgr_QueryType_Label.TabIndex = 0;
            this.SingerMgr_QueryType_Label.Text = "類別查詢:";
            // 
            // SingerMgr_QueryType_ComboBox
            // 
            this.SingerMgr_QueryType_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SingerMgr_QueryType_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_QueryType_ComboBox.FormattingEnabled = true;
            this.SingerMgr_QueryType_ComboBox.Location = new System.Drawing.Point(110, 36);
            this.SingerMgr_QueryType_ComboBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 19);
            this.SingerMgr_QueryType_ComboBox.Name = "SingerMgr_QueryType_ComboBox";
            this.SingerMgr_QueryType_ComboBox.Size = new System.Drawing.Size(198, 30);
            this.SingerMgr_QueryType_ComboBox.TabIndex = 1;
            this.SingerMgr_QueryType_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SingerMgr_QueryType_ComboBox_SelectedIndexChanged);
            // 
            // SingerMgr_SingerAdd_GroupBox
            // 
            this.SingerMgr_SingerAdd_GroupBox.Controls.Add(this.SingerMgr_SingerAddClear_Button);
            this.SingerMgr_SingerAdd_GroupBox.Controls.Add(this.SingerMgr_SingerAddPaste_Button);
            this.SingerMgr_SingerAdd_GroupBox.Controls.Add(this.SingerMgr_SingerAdd_Button);
            this.SingerMgr_SingerAdd_GroupBox.Controls.Add(this.SingerMgr_SingerAddType_ComboBox);
            this.SingerMgr_SingerAdd_GroupBox.Controls.Add(this.SingerMgr_SingerAddType_Label);
            this.SingerMgr_SingerAdd_GroupBox.Controls.Add(this.SingerMgr_SingerAddName_TextBox);
            this.SingerMgr_SingerAdd_GroupBox.Controls.Add(this.SingerMgr_SingerAddName_Label);
            this.SingerMgr_SingerAdd_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerAdd_GroupBox.Location = new System.Drawing.Point(507, 306);
            this.SingerMgr_SingerAdd_GroupBox.Margin = new System.Windows.Forms.Padding(13, 10, 3, 3);
            this.SingerMgr_SingerAdd_GroupBox.Name = "SingerMgr_SingerAdd_GroupBox";
            this.SingerMgr_SingerAdd_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SingerMgr_SingerAdd_GroupBox.Size = new System.Drawing.Size(468, 144);
            this.SingerMgr_SingerAdd_GroupBox.TabIndex = 4;
            this.SingerMgr_SingerAdd_GroupBox.TabStop = false;
            this.SingerMgr_SingerAdd_GroupBox.Text = "歌手新增";
            // 
            // SingerMgr_SingerAddClear_Button
            // 
            this.SingerMgr_SingerAddClear_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerAddClear_Button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SingerMgr_SingerAddClear_Button.Location = new System.Drawing.Point(392, 40);
            this.SingerMgr_SingerAddClear_Button.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SingerMgr_SingerAddClear_Button.Name = "SingerMgr_SingerAddClear_Button";
            this.SingerMgr_SingerAddClear_Button.Size = new System.Drawing.Size(60, 30);
            this.SingerMgr_SingerAddClear_Button.TabIndex = 3;
            this.SingerMgr_SingerAddClear_Button.Text = "清空";
            this.SingerMgr_SingerAddClear_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_SingerAddClear_Button.Click += new System.EventHandler(this.SingerMgr_SingerAddClear_Button_Click);
            // 
            // SingerMgr_SingerAddPaste_Button
            // 
            this.SingerMgr_SingerAddPaste_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerAddPaste_Button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SingerMgr_SingerAddPaste_Button.Location = new System.Drawing.Point(320, 40);
            this.SingerMgr_SingerAddPaste_Button.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SingerMgr_SingerAddPaste_Button.Name = "SingerMgr_SingerAddPaste_Button";
            this.SingerMgr_SingerAddPaste_Button.Size = new System.Drawing.Size(60, 30);
            this.SingerMgr_SingerAddPaste_Button.TabIndex = 2;
            this.SingerMgr_SingerAddPaste_Button.Text = "貼上";
            this.SingerMgr_SingerAddPaste_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_SingerAddPaste_Button.Click += new System.EventHandler(this.SingerMgr_SingerAddPaste_Button_Click);
            // 
            // SingerMgr_SingerAdd_Button
            // 
            this.SingerMgr_SingerAdd_Button.AutoSize = true;
            this.SingerMgr_SingerAdd_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerAdd_Button.Location = new System.Drawing.Point(382, 89);
            this.SingerMgr_SingerAdd_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SingerMgr_SingerAdd_Button.Name = "SingerMgr_SingerAdd_Button";
            this.SingerMgr_SingerAdd_Button.Size = new System.Drawing.Size(70, 32);
            this.SingerMgr_SingerAdd_Button.TabIndex = 6;
            this.SingerMgr_SingerAdd_Button.Text = "新增";
            this.SingerMgr_SingerAdd_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_SingerAdd_Button.Click += new System.EventHandler(this.SingerMgr_SingerAdd_Button_Click);
            // 
            // SingerMgr_SingerAddType_ComboBox
            // 
            this.SingerMgr_SingerAddType_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SingerMgr_SingerAddType_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerAddType_ComboBox.FormattingEnabled = true;
            this.SingerMgr_SingerAddType_ComboBox.Location = new System.Drawing.Point(110, 90);
            this.SingerMgr_SingerAddType_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SingerMgr_SingerAddType_ComboBox.Name = "SingerMgr_SingerAddType_ComboBox";
            this.SingerMgr_SingerAddType_ComboBox.Size = new System.Drawing.Size(198, 30);
            this.SingerMgr_SingerAddType_ComboBox.TabIndex = 5;
            // 
            // SingerMgr_SingerAddType_Label
            // 
            this.SingerMgr_SingerAddType_Label.AutoSize = true;
            this.SingerMgr_SingerAddType_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerAddType_Label.Location = new System.Drawing.Point(16, 94);
            this.SingerMgr_SingerAddType_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SingerMgr_SingerAddType_Label.Name = "SingerMgr_SingerAddType_Label";
            this.SingerMgr_SingerAddType_Label.Size = new System.Drawing.Size(82, 22);
            this.SingerMgr_SingerAddType_Label.TabIndex = 4;
            this.SingerMgr_SingerAddType_Label.Text = "歌手類別:";
            // 
            // SingerMgr_SingerAddName_TextBox
            // 
            this.SingerMgr_SingerAddName_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerAddName_TextBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SingerMgr_SingerAddName_TextBox.Location = new System.Drawing.Point(110, 40);
            this.SingerMgr_SingerAddName_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SingerMgr_SingerAddName_TextBox.Name = "SingerMgr_SingerAddName_TextBox";
            this.SingerMgr_SingerAddName_TextBox.Size = new System.Drawing.Size(198, 30);
            this.SingerMgr_SingerAddName_TextBox.TabIndex = 1;
            this.SingerMgr_SingerAddName_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SingerMgr_SingerAddName_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_HasInvalidChar_TextBox_Validating);
            // 
            // SingerMgr_SingerAddName_Label
            // 
            this.SingerMgr_SingerAddName_Label.AutoSize = true;
            this.SingerMgr_SingerAddName_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerAddName_Label.Location = new System.Drawing.Point(16, 44);
            this.SingerMgr_SingerAddName_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SingerMgr_SingerAddName_Label.Name = "SingerMgr_SingerAddName_Label";
            this.SingerMgr_SingerAddName_Label.Size = new System.Drawing.Size(82, 22);
            this.SingerMgr_SingerAddName_Label.TabIndex = 0;
            this.SingerMgr_SingerAddName_Label.Text = "歌手名稱:";
            // 
            // SingerMgr_Manager_GroupBox
            // 
            this.SingerMgr_Manager_GroupBox.Controls.Add(this.SingerMgr_NonSingerDataLog_Button);
            this.SingerMgr_Manager_GroupBox.Controls.Add(this.SingerMgr_RebuildSingerData_Button);
            this.SingerMgr_Manager_GroupBox.Controls.Add(this.SingerMgr_SingerExport_Button);
            this.SingerMgr_Manager_GroupBox.Controls.Add(this.SingerMgr_SingerImport_Button);
            this.SingerMgr_Manager_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Manager_GroupBox.Location = new System.Drawing.Point(507, 462);
            this.SingerMgr_Manager_GroupBox.Margin = new System.Windows.Forms.Padding(13, 9, 3, 3);
            this.SingerMgr_Manager_GroupBox.Name = "SingerMgr_Manager_GroupBox";
            this.SingerMgr_Manager_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SingerMgr_Manager_GroupBox.Size = new System.Drawing.Size(468, 144);
            this.SingerMgr_Manager_GroupBox.TabIndex = 5;
            this.SingerMgr_Manager_GroupBox.TabStop = false;
            this.SingerMgr_Manager_GroupBox.Text = "歌手管理";
            // 
            // SingerMgr_NonSingerDataLog_Button
            // 
            this.SingerMgr_NonSingerDataLog_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_NonSingerDataLog_Button.Location = new System.Drawing.Point(175, 89);
            this.SingerMgr_NonSingerDataLog_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SingerMgr_NonSingerDataLog_Button.Name = "SingerMgr_NonSingerDataLog_Button";
            this.SingerMgr_NonSingerDataLog_Button.Size = new System.Drawing.Size(147, 32);
            this.SingerMgr_NonSingerDataLog_Button.TabIndex = 3;
            this.SingerMgr_NonSingerDataLog_Button.Text = "記錄無資料歌手";
            this.SingerMgr_NonSingerDataLog_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_NonSingerDataLog_Button.Click += new System.EventHandler(this.SingerMgr_NonSingerDataLog_Button_Click);
            // 
            // SingerMgr_RebuildSingerData_Button
            // 
            this.SingerMgr_RebuildSingerData_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_RebuildSingerData_Button.Location = new System.Drawing.Point(16, 89);
            this.SingerMgr_RebuildSingerData_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SingerMgr_RebuildSingerData_Button.Name = "SingerMgr_RebuildSingerData_Button";
            this.SingerMgr_RebuildSingerData_Button.Size = new System.Drawing.Size(147, 32);
            this.SingerMgr_RebuildSingerData_Button.TabIndex = 2;
            this.SingerMgr_RebuildSingerData_Button.Text = "重建歌庫歌手";
            this.SingerMgr_RebuildSingerData_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_RebuildSingerData_Button.Click += new System.EventHandler(this.SingerMgr_RebuildSingerData_Button_Click);
            // 
            // SingerMgr_SingerExport_Button
            // 
            this.SingerMgr_SingerExport_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerExport_Button.Location = new System.Drawing.Point(175, 39);
            this.SingerMgr_SingerExport_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SingerMgr_SingerExport_Button.Name = "SingerMgr_SingerExport_Button";
            this.SingerMgr_SingerExport_Button.Size = new System.Drawing.Size(147, 32);
            this.SingerMgr_SingerExport_Button.TabIndex = 1;
            this.SingerMgr_SingerExport_Button.Text = "匯出歌手資料";
            this.SingerMgr_SingerExport_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_SingerExport_Button.Click += new System.EventHandler(this.SingerMgr_SingerExport_Button_Click);
            // 
            // SingerMgr_SingerImport_Button
            // 
            this.SingerMgr_SingerImport_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_SingerImport_Button.Location = new System.Drawing.Point(16, 39);
            this.SingerMgr_SingerImport_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SingerMgr_SingerImport_Button.Name = "SingerMgr_SingerImport_Button";
            this.SingerMgr_SingerImport_Button.Size = new System.Drawing.Size(147, 32);
            this.SingerMgr_SingerImport_Button.TabIndex = 0;
            this.SingerMgr_SingerImport_Button.Text = "匯入歌手資料";
            this.SingerMgr_SingerImport_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_SingerImport_Button.Click += new System.EventHandler(this.SingerMgr_SingerImport_Button_Click);
            // 
            // SingerMgr_Statistics_GroupBox
            // 
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics10Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics10_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics9Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics8Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics7Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics9_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics8_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics7_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics6Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics5Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics4Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics3Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics2Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics1Value_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics6_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics5_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics4_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics3_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics2_Label);
            this.SingerMgr_Statistics_GroupBox.Controls.Add(this.SingerMgr_Statistics1_Label);
            this.SingerMgr_Statistics_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics_GroupBox.Location = new System.Drawing.Point(507, 23);
            this.SingerMgr_Statistics_GroupBox.Margin = new System.Windows.Forms.Padding(13, 3, 3, 3);
            this.SingerMgr_Statistics_GroupBox.Name = "SingerMgr_Statistics_GroupBox";
            this.SingerMgr_Statistics_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SingerMgr_Statistics_GroupBox.Size = new System.Drawing.Size(468, 270);
            this.SingerMgr_Statistics_GroupBox.TabIndex = 3;
            this.SingerMgr_Statistics_GroupBox.TabStop = false;
            this.SingerMgr_Statistics_GroupBox.Text = "歌手統計";
            // 
            // SingerMgr_Statistics10Value_Label
            // 
            this.SingerMgr_Statistics10Value_Label.AutoSize = true;
            this.SingerMgr_Statistics10Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics10Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics10Value_Label.Location = new System.Drawing.Point(344, 188);
            this.SingerMgr_Statistics10Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics10Value_Label.Name = "SingerMgr_Statistics10Value_Label";
            this.SingerMgr_Statistics10Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics10Value_Label.TabIndex = 19;
            this.SingerMgr_Statistics10Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics10_Label
            // 
            this.SingerMgr_Statistics10_Label.AutoSize = true;
            this.SingerMgr_Statistics10_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics10_Label.Location = new System.Drawing.Point(250, 188);
            this.SingerMgr_Statistics10_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics10_Label.Name = "SingerMgr_Statistics10_Label";
            this.SingerMgr_Statistics10_Label.Size = new System.Drawing.Size(82, 22);
            this.SingerMgr_Statistics10_Label.TabIndex = 18;
            this.SingerMgr_Statistics10_Label.Text = "新進歌星:";
            // 
            // SingerMgr_Statistics9Value_Label
            // 
            this.SingerMgr_Statistics9Value_Label.AutoSize = true;
            this.SingerMgr_Statistics9Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics9Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics9Value_Label.Location = new System.Drawing.Point(344, 152);
            this.SingerMgr_Statistics9Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics9Value_Label.Name = "SingerMgr_Statistics9Value_Label";
            this.SingerMgr_Statistics9Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics9Value_Label.TabIndex = 17;
            this.SingerMgr_Statistics9Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics8Value_Label
            // 
            this.SingerMgr_Statistics8Value_Label.AutoSize = true;
            this.SingerMgr_Statistics8Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics8Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics8Value_Label.Location = new System.Drawing.Point(344, 116);
            this.SingerMgr_Statistics8Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics8Value_Label.Name = "SingerMgr_Statistics8Value_Label";
            this.SingerMgr_Statistics8Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics8Value_Label.TabIndex = 15;
            this.SingerMgr_Statistics8Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics7Value_Label
            // 
            this.SingerMgr_Statistics7Value_Label.AutoSize = true;
            this.SingerMgr_Statistics7Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics7Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics7Value_Label.Location = new System.Drawing.Point(344, 80);
            this.SingerMgr_Statistics7Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics7Value_Label.Name = "SingerMgr_Statistics7Value_Label";
            this.SingerMgr_Statistics7Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics7Value_Label.TabIndex = 13;
            this.SingerMgr_Statistics7Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics9_Label
            // 
            this.SingerMgr_Statistics9_Label.AutoSize = true;
            this.SingerMgr_Statistics9_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics9_Label.Location = new System.Drawing.Point(250, 152);
            this.SingerMgr_Statistics9_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics9_Label.Name = "SingerMgr_Statistics9_Label";
            this.SingerMgr_Statistics9_Label.Size = new System.Drawing.Size(48, 22);
            this.SingerMgr_Statistics9_Label.TabIndex = 16;
            this.SingerMgr_Statistics9_Label.Text = "其它:";
            // 
            // SingerMgr_Statistics8_Label
            // 
            this.SingerMgr_Statistics8_Label.AutoSize = true;
            this.SingerMgr_Statistics8_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics8_Label.Location = new System.Drawing.Point(250, 116);
            this.SingerMgr_Statistics8_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics8_Label.Name = "SingerMgr_Statistics8_Label";
            this.SingerMgr_Statistics8_Label.Size = new System.Drawing.Size(82, 22);
            this.SingerMgr_Statistics8_Label.TabIndex = 14;
            this.SingerMgr_Statistics8_Label.Text = "外國樂團:";
            // 
            // SingerMgr_Statistics7_Label
            // 
            this.SingerMgr_Statistics7_Label.AutoSize = true;
            this.SingerMgr_Statistics7_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics7_Label.Location = new System.Drawing.Point(250, 80);
            this.SingerMgr_Statistics7_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics7_Label.Name = "SingerMgr_Statistics7_Label";
            this.SingerMgr_Statistics7_Label.Size = new System.Drawing.Size(65, 22);
            this.SingerMgr_Statistics7_Label.TabIndex = 12;
            this.SingerMgr_Statistics7_Label.Text = "外國女:";
            // 
            // SingerMgr_Statistics6Value_Label
            // 
            this.SingerMgr_Statistics6Value_Label.AutoSize = true;
            this.SingerMgr_Statistics6Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics6Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics6Value_Label.Location = new System.Drawing.Point(110, 224);
            this.SingerMgr_Statistics6Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics6Value_Label.Name = "SingerMgr_Statistics6Value_Label";
            this.SingerMgr_Statistics6Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics6Value_Label.TabIndex = 11;
            this.SingerMgr_Statistics6Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics5Value_Label
            // 
            this.SingerMgr_Statistics5Value_Label.AutoSize = true;
            this.SingerMgr_Statistics5Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics5Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics5Value_Label.Location = new System.Drawing.Point(110, 188);
            this.SingerMgr_Statistics5Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics5Value_Label.Name = "SingerMgr_Statistics5Value_Label";
            this.SingerMgr_Statistics5Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics5Value_Label.TabIndex = 9;
            this.SingerMgr_Statistics5Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics4Value_Label
            // 
            this.SingerMgr_Statistics4Value_Label.AutoSize = true;
            this.SingerMgr_Statistics4Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics4Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics4Value_Label.Location = new System.Drawing.Point(110, 152);
            this.SingerMgr_Statistics4Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics4Value_Label.Name = "SingerMgr_Statistics4Value_Label";
            this.SingerMgr_Statistics4Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics4Value_Label.TabIndex = 7;
            this.SingerMgr_Statistics4Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics3Value_Label
            // 
            this.SingerMgr_Statistics3Value_Label.AutoSize = true;
            this.SingerMgr_Statistics3Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics3Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics3Value_Label.Location = new System.Drawing.Point(110, 116);
            this.SingerMgr_Statistics3Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics3Value_Label.Name = "SingerMgr_Statistics3Value_Label";
            this.SingerMgr_Statistics3Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics3Value_Label.TabIndex = 5;
            this.SingerMgr_Statistics3Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics2Value_Label
            // 
            this.SingerMgr_Statistics2Value_Label.AutoSize = true;
            this.SingerMgr_Statistics2Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics2Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics2Value_Label.Location = new System.Drawing.Point(110, 80);
            this.SingerMgr_Statistics2Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics2Value_Label.Name = "SingerMgr_Statistics2Value_Label";
            this.SingerMgr_Statistics2Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics2Value_Label.TabIndex = 3;
            this.SingerMgr_Statistics2Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics1Value_Label
            // 
            this.SingerMgr_Statistics1Value_Label.AutoSize = true;
            this.SingerMgr_Statistics1Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics1Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SingerMgr_Statistics1Value_Label.Location = new System.Drawing.Point(110, 44);
            this.SingerMgr_Statistics1Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics1Value_Label.Name = "SingerMgr_Statistics1Value_Label";
            this.SingerMgr_Statistics1Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SingerMgr_Statistics1Value_Label.TabIndex = 1;
            this.SingerMgr_Statistics1Value_Label.Text = "0 位";
            // 
            // SingerMgr_Statistics6_Label
            // 
            this.SingerMgr_Statistics6_Label.AutoSize = true;
            this.SingerMgr_Statistics6_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics6_Label.Location = new System.Drawing.Point(16, 224);
            this.SingerMgr_Statistics6_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics6_Label.Name = "SingerMgr_Statistics6_Label";
            this.SingerMgr_Statistics6_Label.Size = new System.Drawing.Size(65, 22);
            this.SingerMgr_Statistics6_Label.TabIndex = 10;
            this.SingerMgr_Statistics6_Label.Text = "外國男:";
            // 
            // SingerMgr_Statistics5_Label
            // 
            this.SingerMgr_Statistics5_Label.AutoSize = true;
            this.SingerMgr_Statistics5_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics5_Label.Location = new System.Drawing.Point(16, 188);
            this.SingerMgr_Statistics5_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics5_Label.Name = "SingerMgr_Statistics5_Label";
            this.SingerMgr_Statistics5_Label.Size = new System.Drawing.Size(48, 22);
            this.SingerMgr_Statistics5_Label.TabIndex = 8;
            this.SingerMgr_Statistics5_Label.Text = "合唱:";
            // 
            // SingerMgr_Statistics4_Label
            // 
            this.SingerMgr_Statistics4_Label.AutoSize = true;
            this.SingerMgr_Statistics4_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics4_Label.Location = new System.Drawing.Point(16, 152);
            this.SingerMgr_Statistics4_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics4_Label.Name = "SingerMgr_Statistics4_Label";
            this.SingerMgr_Statistics4_Label.Size = new System.Drawing.Size(48, 22);
            this.SingerMgr_Statistics4_Label.TabIndex = 6;
            this.SingerMgr_Statistics4_Label.Text = "樂團:";
            // 
            // SingerMgr_Statistics3_Label
            // 
            this.SingerMgr_Statistics3_Label.AutoSize = true;
            this.SingerMgr_Statistics3_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics3_Label.Location = new System.Drawing.Point(16, 116);
            this.SingerMgr_Statistics3_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics3_Label.Name = "SingerMgr_Statistics3_Label";
            this.SingerMgr_Statistics3_Label.Size = new System.Drawing.Size(65, 22);
            this.SingerMgr_Statistics3_Label.TabIndex = 4;
            this.SingerMgr_Statistics3_Label.Text = "女歌星:";
            // 
            // SingerMgr_Statistics2_Label
            // 
            this.SingerMgr_Statistics2_Label.AutoSize = true;
            this.SingerMgr_Statistics2_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics2_Label.Location = new System.Drawing.Point(16, 80);
            this.SingerMgr_Statistics2_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics2_Label.Name = "SingerMgr_Statistics2_Label";
            this.SingerMgr_Statistics2_Label.Size = new System.Drawing.Size(65, 22);
            this.SingerMgr_Statistics2_Label.TabIndex = 2;
            this.SingerMgr_Statistics2_Label.Text = "男歌星:";
            // 
            // SingerMgr_Statistics1_Label
            // 
            this.SingerMgr_Statistics1_Label.AutoSize = true;
            this.SingerMgr_Statistics1_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Statistics1_Label.ForeColor = System.Drawing.Color.SaddleBrown;
            this.SingerMgr_Statistics1_Label.Location = new System.Drawing.Point(16, 44);
            this.SingerMgr_Statistics1_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 0);
            this.SingerMgr_Statistics1_Label.Name = "SingerMgr_Statistics1_Label";
            this.SingerMgr_Statistics1_Label.Size = new System.Drawing.Size(48, 22);
            this.SingerMgr_Statistics1_Label.TabIndex = 0;
            this.SingerMgr_Statistics1_Label.Text = "總計:";
            // 
            // SingerMgr_Tooltip_GroupBox
            // 
            this.SingerMgr_Tooltip_GroupBox.Controls.Add(this.SingerMgr_Tooltip_Label);
            this.SingerMgr_Tooltip_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 1.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Tooltip_GroupBox.Location = new System.Drawing.Point(23, 621);
            this.SingerMgr_Tooltip_GroupBox.Margin = new System.Windows.Forms.Padding(3, 12, 6, 3);
            this.SingerMgr_Tooltip_GroupBox.Name = "SingerMgr_Tooltip_GroupBox";
            this.SingerMgr_Tooltip_GroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.SingerMgr_Tooltip_GroupBox.Size = new System.Drawing.Size(949, 35);
            this.SingerMgr_Tooltip_GroupBox.TabIndex = 6;
            this.SingerMgr_Tooltip_GroupBox.TabStop = false;
            // 
            // SingerMgr_Tooltip_Label
            // 
            this.SingerMgr_Tooltip_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Tooltip_Label.ForeColor = System.Drawing.Color.Red;
            this.SingerMgr_Tooltip_Label.Location = new System.Drawing.Point(0, 0);
            this.SingerMgr_Tooltip_Label.Margin = new System.Windows.Forms.Padding(0);
            this.SingerMgr_Tooltip_Label.Name = "SingerMgr_Tooltip_Label";
            this.SingerMgr_Tooltip_Label.Size = new System.Drawing.Size(949, 35);
            this.SingerMgr_Tooltip_Label.TabIndex = 0;
            this.SingerMgr_Tooltip_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SingerMgr_Tooltip_Label.UseMnemonic = false;
            // 
            // SingerMgr_DataGridView
            // 
            this.SingerMgr_DataGridView.AllowUserToAddRows = false;
            this.SingerMgr_DataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            this.SingerMgr_DataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.SingerMgr_DataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.SingerMgr_DataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SingerMgr_DataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.SingerMgr_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SingerMgr_DataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.SingerMgr_DataGridView.EnableHeadersVisualStyles = false;
            this.SingerMgr_DataGridView.Location = new System.Drawing.Point(23, 279);
            this.SingerMgr_DataGridView.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.SingerMgr_DataGridView.MultiSelect = false;
            this.SingerMgr_DataGridView.Name = "SingerMgr_DataGridView";
            dataGridViewCellStyle9.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_DataGridView.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.SingerMgr_DataGridView.RowTemplate.Height = 27;
            this.SingerMgr_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SingerMgr_DataGridView.Size = new System.Drawing.Size(468, 327);
            this.SingerMgr_DataGridView.TabIndex = 2;
            this.SingerMgr_DataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.SingerMgr_DataGridView_CellBeginEdit);
            this.SingerMgr_DataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.SingerMgr_DataGridView_CellEndEdit);
            this.SingerMgr_DataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.SingerMgr_DataGridView_CellFormatting);
            this.SingerMgr_DataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.SingerMgr_DataGridView_CellMouseClick);
            this.SingerMgr_DataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.SingerMgr_DataGridView_CellValidating);
            this.SingerMgr_DataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SingerMgr_DataGridView_KeyDown);
            // 
            // SingerMgr_Query_GroupBox
            // 
            this.SingerMgr_Query_GroupBox.Controls.Add(this.SingerMgr_DefaultSingerDataTable_ComboBox);
            this.SingerMgr_Query_GroupBox.Controls.Add(this.SingerMgr_DefaultSingerDataTable_Label);
            this.SingerMgr_Query_GroupBox.Controls.Add(this.SingerMgr_QueryClear_Button);
            this.SingerMgr_Query_GroupBox.Controls.Add(this.SingerMgr_QueryPaste_Button);
            this.SingerMgr_Query_GroupBox.Controls.Add(this.SingerMgr_Query_Button);
            this.SingerMgr_Query_GroupBox.Controls.Add(this.SingerMgr_QueryValue_Label);
            this.SingerMgr_Query_GroupBox.Controls.Add(this.SingerMgr_QueryValue_TextBox);
            this.SingerMgr_Query_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Query_GroupBox.Location = new System.Drawing.Point(23, 23);
            this.SingerMgr_Query_GroupBox.Name = "SingerMgr_Query_GroupBox";
            this.SingerMgr_Query_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SingerMgr_Query_GroupBox.Size = new System.Drawing.Size(468, 138);
            this.SingerMgr_Query_GroupBox.TabIndex = 0;
            this.SingerMgr_Query_GroupBox.TabStop = false;
            this.SingerMgr_Query_GroupBox.Text = "歌手查詢";
            // 
            // SingerMgr_DefaultSingerDataTable_ComboBox
            // 
            this.SingerMgr_DefaultSingerDataTable_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SingerMgr_DefaultSingerDataTable_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_DefaultSingerDataTable_ComboBox.FormattingEnabled = true;
            this.SingerMgr_DefaultSingerDataTable_ComboBox.Location = new System.Drawing.Point(127, 86);
            this.SingerMgr_DefaultSingerDataTable_ComboBox.Margin = new System.Windows.Forms.Padding(6, 6, 6, 10);
            this.SingerMgr_DefaultSingerDataTable_ComboBox.Name = "SingerMgr_DefaultSingerDataTable_ComboBox";
            this.SingerMgr_DefaultSingerDataTable_ComboBox.Size = new System.Drawing.Size(181, 30);
            this.SingerMgr_DefaultSingerDataTable_ComboBox.TabIndex = 5;
            this.SingerMgr_DefaultSingerDataTable_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SingerMgr_DefaultSingerDataTable_ComboBox_SelectedIndexChanged);
            // 
            // SingerMgr_DefaultSingerDataTable_Label
            // 
            this.SingerMgr_DefaultSingerDataTable_Label.AutoSize = true;
            this.SingerMgr_DefaultSingerDataTable_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_DefaultSingerDataTable_Label.Location = new System.Drawing.Point(16, 90);
            this.SingerMgr_DefaultSingerDataTable_Label.Margin = new System.Windows.Forms.Padding(6, 10, 6, 14);
            this.SingerMgr_DefaultSingerDataTable_Label.Name = "SingerMgr_DefaultSingerDataTable_Label";
            this.SingerMgr_DefaultSingerDataTable_Label.Size = new System.Drawing.Size(99, 22);
            this.SingerMgr_DefaultSingerDataTable_Label.TabIndex = 4;
            this.SingerMgr_DefaultSingerDataTable_Label.Text = "歌手資料表:";
            // 
            // SingerMgr_QueryClear_Button
            // 
            this.SingerMgr_QueryClear_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_QueryClear_Button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SingerMgr_QueryClear_Button.Location = new System.Drawing.Point(392, 40);
            this.SingerMgr_QueryClear_Button.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SingerMgr_QueryClear_Button.Name = "SingerMgr_QueryClear_Button";
            this.SingerMgr_QueryClear_Button.Size = new System.Drawing.Size(60, 30);
            this.SingerMgr_QueryClear_Button.TabIndex = 3;
            this.SingerMgr_QueryClear_Button.Text = "清空";
            this.SingerMgr_QueryClear_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_QueryClear_Button.Click += new System.EventHandler(this.SingerMgr_QueryClear_Button_Click);
            // 
            // SingerMgr_QueryPaste_Button
            // 
            this.SingerMgr_QueryPaste_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_QueryPaste_Button.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SingerMgr_QueryPaste_Button.Location = new System.Drawing.Point(320, 40);
            this.SingerMgr_QueryPaste_Button.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SingerMgr_QueryPaste_Button.Name = "SingerMgr_QueryPaste_Button";
            this.SingerMgr_QueryPaste_Button.Size = new System.Drawing.Size(60, 30);
            this.SingerMgr_QueryPaste_Button.TabIndex = 2;
            this.SingerMgr_QueryPaste_Button.Text = "貼上";
            this.SingerMgr_QueryPaste_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_QueryPaste_Button.Click += new System.EventHandler(this.SingerMgr_QueryPaste_Button_Click);
            // 
            // SingerMgr_Query_Button
            // 
            this.SingerMgr_Query_Button.AutoSize = true;
            this.SingerMgr_Query_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_Query_Button.Location = new System.Drawing.Point(382, 85);
            this.SingerMgr_Query_Button.Margin = new System.Windows.Forms.Padding(6, 5, 6, 14);
            this.SingerMgr_Query_Button.Name = "SingerMgr_Query_Button";
            this.SingerMgr_Query_Button.Size = new System.Drawing.Size(70, 32);
            this.SingerMgr_Query_Button.TabIndex = 6;
            this.SingerMgr_Query_Button.Text = "查詢";
            this.SingerMgr_Query_Button.UseVisualStyleBackColor = true;
            this.SingerMgr_Query_Button.Click += new System.EventHandler(this.SingerMgr_Query_Button_Click);
            // 
            // SingerMgr_QueryValue_Label
            // 
            this.SingerMgr_QueryValue_Label.AutoSize = true;
            this.SingerMgr_QueryValue_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_QueryValue_Label.Location = new System.Drawing.Point(16, 44);
            this.SingerMgr_QueryValue_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SingerMgr_QueryValue_Label.Name = "SingerMgr_QueryValue_Label";
            this.SingerMgr_QueryValue_Label.Size = new System.Drawing.Size(82, 22);
            this.SingerMgr_QueryValue_Label.TabIndex = 0;
            this.SingerMgr_QueryValue_Label.Text = "歌手查詢:";
            // 
            // SingerMgr_QueryValue_TextBox
            // 
            this.SingerMgr_QueryValue_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SingerMgr_QueryValue_TextBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SingerMgr_QueryValue_TextBox.Location = new System.Drawing.Point(110, 40);
            this.SingerMgr_QueryValue_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SingerMgr_QueryValue_TextBox.Name = "SingerMgr_QueryValue_TextBox";
            this.SingerMgr_QueryValue_TextBox.Size = new System.Drawing.Size(198, 30);
            this.SingerMgr_QueryValue_TextBox.TabIndex = 1;
            this.SingerMgr_QueryValue_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SingerMgr_QueryValue_TextBox_KeyPress);
            // 
            // SongMgrCfg_TabPage
            // 
            this.SongMgrCfg_TabPage.Controls.Add(this.SongMgrCfg_SongType_GroupBox);
            this.SongMgrCfg_TabPage.Controls.Add(this.SongMgrCfg_SongStructure_TabControl);
            this.SongMgrCfg_TabPage.Controls.Add(this.SongMgrCfg_Tooltip_GroupBox);
            this.SongMgrCfg_TabPage.Controls.Add(this.SongMgrCfg_SongID_GroupBox);
            this.SongMgrCfg_TabPage.Controls.Add(this.SongMgrCfg_General_GroupBox);
            this.SongMgrCfg_TabPage.Controls.Add(this.SongMgrCfg_Save_Button);
            this.SongMgrCfg_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMgrCfg_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongMgrCfg_TabPage.Name = "SongMgrCfg_TabPage";
            this.SongMgrCfg_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.SongMgrCfg_TabPage.Size = new System.Drawing.Size(998, 684);
            this.SongMgrCfg_TabPage.TabIndex = 2;
            this.SongMgrCfg_TabPage.Text = "歌庫設定";
            this.SongMgrCfg_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMgrCfg_SongType_GroupBox
            // 
            this.SongMgrCfg_SongType_GroupBox.Controls.Add(this.SongMgrCfg_SongType_TextBox);
            this.SongMgrCfg_SongType_GroupBox.Controls.Add(this.SongMgrCfg_SongType_Button);
            this.SongMgrCfg_SongType_GroupBox.Controls.Add(this.SongMgrCfg_SongType_ListBox);
            this.SongMgrCfg_SongType_GroupBox.Location = new System.Drawing.Point(367, 286);
            this.SongMgrCfg_SongType_GroupBox.Margin = new System.Windows.Forms.Padding(14, 20, 3, 3);
            this.SongMgrCfg_SongType_GroupBox.Name = "SongMgrCfg_SongType_GroupBox";
            this.SongMgrCfg_SongType_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMgrCfg_SongType_GroupBox.Size = new System.Drawing.Size(245, 320);
            this.SongMgrCfg_SongType_GroupBox.TabIndex = 2;
            this.SongMgrCfg_SongType_GroupBox.TabStop = false;
            this.SongMgrCfg_SongType_GroupBox.Text = "歌曲類別";
            // 
            // SongMgrCfg_SongType_TextBox
            // 
            this.SongMgrCfg_SongType_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongType_TextBox.Location = new System.Drawing.Point(16, 277);
            this.SongMgrCfg_SongType_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMgrCfg_SongType_TextBox.Name = "SongMgrCfg_SongType_TextBox";
            this.SongMgrCfg_SongType_TextBox.Size = new System.Drawing.Size(131, 30);
            this.SongMgrCfg_SongType_TextBox.TabIndex = 1;
            this.SongMgrCfg_SongType_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_SongType_TextBox_Enter);
            this.SongMgrCfg_SongType_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMgrCfg_SongType_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_HasInvalidChar_TextBox_Validating);
            // 
            // SongMgrCfg_SongType_Button
            // 
            this.SongMgrCfg_SongType_Button.AutoSize = true;
            this.SongMgrCfg_SongType_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongType_Button.Location = new System.Drawing.Point(159, 276);
            this.SongMgrCfg_SongType_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMgrCfg_SongType_Button.Name = "SongMgrCfg_SongType_Button";
            this.SongMgrCfg_SongType_Button.Size = new System.Drawing.Size(70, 32);
            this.SongMgrCfg_SongType_Button.TabIndex = 2;
            this.SongMgrCfg_SongType_Button.Text = "加入";
            this.SongMgrCfg_SongType_Button.UseVisualStyleBackColor = true;
            this.SongMgrCfg_SongType_Button.Click += new System.EventHandler(this.SongMgrCfg_SongType_Button_Click);
            // 
            // SongMgrCfg_SongType_ListBox
            // 
            this.SongMgrCfg_SongType_ListBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongType_ListBox.FormattingEnabled = true;
            this.SongMgrCfg_SongType_ListBox.ItemHeight = 22;
            this.SongMgrCfg_SongType_ListBox.Location = new System.Drawing.Point(16, 40);
            this.SongMgrCfg_SongType_ListBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMgrCfg_SongType_ListBox.Name = "SongMgrCfg_SongType_ListBox";
            this.SongMgrCfg_SongType_ListBox.Size = new System.Drawing.Size(213, 224);
            this.SongMgrCfg_SongType_ListBox.TabIndex = 0;
            this.SongMgrCfg_SongType_ListBox.Enter += new System.EventHandler(this.SongMgrCfg_SongType_ListBox_Enter);
            // 
            // SongMgrCfg_SongStructure_TabControl
            // 
            this.SongMgrCfg_SongStructure_TabControl.Controls.Add(this.SongMgrCfg_SongStructure_TabPage);
            this.SongMgrCfg_SongStructure_TabControl.Controls.Add(this.SongMgrCfg_CustomStructure_TabPage);
            this.SongMgrCfg_SongStructure_TabControl.Location = new System.Drawing.Point(629, 286);
            this.SongMgrCfg_SongStructure_TabControl.Margin = new System.Windows.Forms.Padding(14, 20, 3, 3);
            this.SongMgrCfg_SongStructure_TabControl.Name = "SongMgrCfg_SongStructure_TabControl";
            this.SongMgrCfg_SongStructure_TabControl.SelectedIndex = 0;
            this.SongMgrCfg_SongStructure_TabControl.Size = new System.Drawing.Size(346, 320);
            this.SongMgrCfg_SongStructure_TabControl.TabIndex = 3;
            // 
            // SongMgrCfg_SongStructure_TabPage
            // 
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_CrtchorusMerge_CheckBox);
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_SongInfoSeparate_Label);
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_FileStructure_ComboBox);
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_SongInfoSeparate_ComboBox);
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_FileStructure_Label);
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_CrtchorusSeparate_Label);
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_FolderStructure_ComboBox);
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_CrtchorusSeparate_ComboBox);
            this.SongMgrCfg_SongStructure_TabPage.Controls.Add(this.SongMgrCfg_FolderStructure_Label);
            this.SongMgrCfg_SongStructure_TabPage.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongStructure_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMgrCfg_SongStructure_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongMgrCfg_SongStructure_TabPage.Name = "SongMgrCfg_SongStructure_TabPage";
            this.SongMgrCfg_SongStructure_TabPage.Padding = new System.Windows.Forms.Padding(8, 14, 12, 14);
            this.SongMgrCfg_SongStructure_TabPage.Size = new System.Drawing.Size(338, 282);
            this.SongMgrCfg_SongStructure_TabPage.TabIndex = 0;
            this.SongMgrCfg_SongStructure_TabPage.Text = "歌庫結構";
            this.SongMgrCfg_SongStructure_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMgrCfg_CrtchorusMerge_CheckBox
            // 
            this.SongMgrCfg_CrtchorusMerge_CheckBox.AutoSize = true;
            this.SongMgrCfg_CrtchorusMerge_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CrtchorusMerge_CheckBox.Location = new System.Drawing.Point(15, 226);
            this.SongMgrCfg_CrtchorusMerge_CheckBox.Margin = new System.Windows.Forms.Padding(3, 12, 6, 12);
            this.SongMgrCfg_CrtchorusMerge_CheckBox.Name = "SongMgrCfg_CrtchorusMerge_CheckBox";
            this.SongMgrCfg_CrtchorusMerge_CheckBox.Size = new System.Drawing.Size(236, 26);
            this.SongMgrCfg_CrtchorusMerge_CheckBox.TabIndex = 8;
            this.SongMgrCfg_CrtchorusMerge_CheckBox.Text = "合唱歌手存放在同一資料夾";
            this.SongMgrCfg_CrtchorusMerge_CheckBox.UseVisualStyleBackColor = true;
            this.SongMgrCfg_CrtchorusMerge_CheckBox.CheckedChanged += new System.EventHandler(this.SongMgrCfg_CrtchorusMerge_CheckBox_CheckedChanged);
            // 
            // SongMgrCfg_SongInfoSeparate_Label
            // 
            this.SongMgrCfg_SongInfoSeparate_Label.AutoSize = true;
            this.SongMgrCfg_SongInfoSeparate_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongInfoSeparate_Label.Location = new System.Drawing.Point(11, 28);
            this.SongMgrCfg_SongInfoSeparate_Label.Margin = new System.Windows.Forms.Padding(3, 14, 6, 14);
            this.SongMgrCfg_SongInfoSeparate_Label.Name = "SongMgrCfg_SongInfoSeparate_Label";
            this.SongMgrCfg_SongInfoSeparate_Label.Size = new System.Drawing.Size(150, 22);
            this.SongMgrCfg_SongInfoSeparate_Label.TabIndex = 0;
            this.SongMgrCfg_SongInfoSeparate_Label.Text = "歌曲資訊分隔字元:";
            // 
            // SongMgrCfg_FileStructure_ComboBox
            // 
            this.SongMgrCfg_FileStructure_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_FileStructure_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_FileStructure_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_FileStructure_ComboBox.Location = new System.Drawing.Point(122, 174);
            this.SongMgrCfg_FileStructure_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 3, 10);
            this.SongMgrCfg_FileStructure_ComboBox.Name = "SongMgrCfg_FileStructure_ComboBox";
            this.SongMgrCfg_FileStructure_ComboBox.Size = new System.Drawing.Size(201, 30);
            this.SongMgrCfg_FileStructure_ComboBox.TabIndex = 7;
            this.SongMgrCfg_FileStructure_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_FileStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_SongInfoSeparate_ComboBox
            // 
            this.SongMgrCfg_SongInfoSeparate_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_SongInfoSeparate_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongInfoSeparate_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_SongInfoSeparate_ComboBox.Location = new System.Drawing.Point(173, 24);
            this.SongMgrCfg_SongInfoSeparate_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 3, 10);
            this.SongMgrCfg_SongInfoSeparate_ComboBox.Name = "SongMgrCfg_SongInfoSeparate_ComboBox";
            this.SongMgrCfg_SongInfoSeparate_ComboBox.Size = new System.Drawing.Size(150, 30);
            this.SongMgrCfg_SongInfoSeparate_ComboBox.TabIndex = 1;
            this.SongMgrCfg_SongInfoSeparate_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_SongInfoSeparate_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_FileStructure_Label
            // 
            this.SongMgrCfg_FileStructure_Label.AutoSize = true;
            this.SongMgrCfg_FileStructure_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_FileStructure_Label.Location = new System.Drawing.Point(11, 178);
            this.SongMgrCfg_FileStructure_Label.Margin = new System.Windows.Forms.Padding(3, 14, 6, 14);
            this.SongMgrCfg_FileStructure_Label.Name = "SongMgrCfg_FileStructure_Label";
            this.SongMgrCfg_FileStructure_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMgrCfg_FileStructure_Label.TabIndex = 6;
            this.SongMgrCfg_FileStructure_Label.Text = "檔案結構:";
            // 
            // SongMgrCfg_CrtchorusSeparate_Label
            // 
            this.SongMgrCfg_CrtchorusSeparate_Label.AutoSize = true;
            this.SongMgrCfg_CrtchorusSeparate_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CrtchorusSeparate_Label.Location = new System.Drawing.Point(11, 78);
            this.SongMgrCfg_CrtchorusSeparate_Label.Margin = new System.Windows.Forms.Padding(3, 14, 6, 14);
            this.SongMgrCfg_CrtchorusSeparate_Label.Name = "SongMgrCfg_CrtchorusSeparate_Label";
            this.SongMgrCfg_CrtchorusSeparate_Label.Size = new System.Drawing.Size(150, 22);
            this.SongMgrCfg_CrtchorusSeparate_Label.TabIndex = 2;
            this.SongMgrCfg_CrtchorusSeparate_Label.Text = "合唱歌手分隔字元:";
            // 
            // SongMgrCfg_FolderStructure_ComboBox
            // 
            this.SongMgrCfg_FolderStructure_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_FolderStructure_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_FolderStructure_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_FolderStructure_ComboBox.Location = new System.Drawing.Point(122, 124);
            this.SongMgrCfg_FolderStructure_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 3, 10);
            this.SongMgrCfg_FolderStructure_ComboBox.Name = "SongMgrCfg_FolderStructure_ComboBox";
            this.SongMgrCfg_FolderStructure_ComboBox.Size = new System.Drawing.Size(201, 30);
            this.SongMgrCfg_FolderStructure_ComboBox.TabIndex = 5;
            this.SongMgrCfg_FolderStructure_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_FolderStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_CrtchorusSeparate_ComboBox
            // 
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.Location = new System.Drawing.Point(173, 74);
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 3, 10);
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.Name = "SongMgrCfg_CrtchorusSeparate_ComboBox";
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.Size = new System.Drawing.Size(150, 30);
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.TabIndex = 3;
            this.SongMgrCfg_CrtchorusSeparate_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CrtchorusSeparate_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_FolderStructure_Label
            // 
            this.SongMgrCfg_FolderStructure_Label.AutoSize = true;
            this.SongMgrCfg_FolderStructure_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_FolderStructure_Label.Location = new System.Drawing.Point(11, 128);
            this.SongMgrCfg_FolderStructure_Label.Margin = new System.Windows.Forms.Padding(3, 14, 6, 14);
            this.SongMgrCfg_FolderStructure_Label.Name = "SongMgrCfg_FolderStructure_Label";
            this.SongMgrCfg_FolderStructure_Label.Size = new System.Drawing.Size(99, 22);
            this.SongMgrCfg_FolderStructure_Label.TabIndex = 4;
            this.SongMgrCfg_FolderStructure_Label.Text = "資料夾結構:";
            // 
            // SongMgrCfg_CustomStructure_TabPage
            // 
            this.SongMgrCfg_CustomStructure_TabPage.AutoScroll = true;
            this.SongMgrCfg_CustomStructure_TabPage.AutoScrollMargin = new System.Drawing.Size(0, 16);
            this.SongMgrCfg_CustomStructure_TabPage.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure_GroupBox);
            this.SongMgrCfg_CustomStructure_TabPage.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomStructure_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMgrCfg_CustomStructure_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongMgrCfg_CustomStructure_TabPage.Name = "SongMgrCfg_CustomStructure_TabPage";
            this.SongMgrCfg_CustomStructure_TabPage.Padding = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.SongMgrCfg_CustomStructure_TabPage.Size = new System.Drawing.Size(338, 282);
            this.SongMgrCfg_CustomStructure_TabPage.TabIndex = 1;
            this.SongMgrCfg_CustomStructure_TabPage.Text = "自訂結構";
            this.SongMgrCfg_CustomStructure_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMgrCfg_CustomSingerTypeStructure_GroupBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure1_Label);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure2_Label);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure3_Label);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure4_Label);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure5_Label);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure6_Label);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure7_Label);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure8_Label);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Controls.Add(this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Location = new System.Drawing.Point(13, 17);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Name = "SongMgrCfg_CustomSingerTypeStructure_GroupBox";
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Size = new System.Drawing.Size(291, 375);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.TabIndex = 0;
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.TabStop = false;
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.Text = "歌手類別";
            // 
            // SongMgrCfg_CustomSingerTypeStructure1_Label
            // 
            this.SongMgrCfg_CustomSingerTypeStructure1_Label.AutoSize = true;
            this.SongMgrCfg_CustomSingerTypeStructure1_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure1_Label.Location = new System.Drawing.Point(13, 40);
            this.SongMgrCfg_CustomSingerTypeStructure1_Label.Margin = new System.Windows.Forms.Padding(3, 10, 6, 10);
            this.SongMgrCfg_CustomSingerTypeStructure1_Label.Name = "SongMgrCfg_CustomSingerTypeStructure1_Label";
            this.SongMgrCfg_CustomSingerTypeStructure1_Label.Size = new System.Drawing.Size(65, 22);
            this.SongMgrCfg_CustomSingerTypeStructure1_Label.TabIndex = 0;
            this.SongMgrCfg_CustomSingerTypeStructure1_Label.Text = "男歌星:";
            // 
            // SongMgrCfg_CustomSingerTypeStructure8_ComboBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.Location = new System.Drawing.Point(107, 330);
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.Name = "SongMgrCfg_CustomSingerTypeStructure8_ComboBox";
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.Size = new System.Drawing.Size(168, 30);
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.TabIndex = 15;
            this.SongMgrCfg_CustomSingerTypeStructure8_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CustomSingerTypeStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_CustomSingerTypeStructure2_Label
            // 
            this.SongMgrCfg_CustomSingerTypeStructure2_Label.AutoSize = true;
            this.SongMgrCfg_CustomSingerTypeStructure2_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure2_Label.Location = new System.Drawing.Point(13, 82);
            this.SongMgrCfg_CustomSingerTypeStructure2_Label.Margin = new System.Windows.Forms.Padding(3, 10, 6, 10);
            this.SongMgrCfg_CustomSingerTypeStructure2_Label.Name = "SongMgrCfg_CustomSingerTypeStructure2_Label";
            this.SongMgrCfg_CustomSingerTypeStructure2_Label.Size = new System.Drawing.Size(65, 22);
            this.SongMgrCfg_CustomSingerTypeStructure2_Label.TabIndex = 2;
            this.SongMgrCfg_CustomSingerTypeStructure2_Label.Text = "女歌星:";
            // 
            // SongMgrCfg_CustomSingerTypeStructure6_ComboBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.Location = new System.Drawing.Point(107, 246);
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.Name = "SongMgrCfg_CustomSingerTypeStructure6_ComboBox";
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.Size = new System.Drawing.Size(168, 30);
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.TabIndex = 11;
            this.SongMgrCfg_CustomSingerTypeStructure6_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CustomSingerTypeStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_CustomSingerTypeStructure3_Label
            // 
            this.SongMgrCfg_CustomSingerTypeStructure3_Label.AutoSize = true;
            this.SongMgrCfg_CustomSingerTypeStructure3_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure3_Label.Location = new System.Drawing.Point(13, 124);
            this.SongMgrCfg_CustomSingerTypeStructure3_Label.Margin = new System.Windows.Forms.Padding(3, 10, 6, 10);
            this.SongMgrCfg_CustomSingerTypeStructure3_Label.Name = "SongMgrCfg_CustomSingerTypeStructure3_Label";
            this.SongMgrCfg_CustomSingerTypeStructure3_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_CustomSingerTypeStructure3_Label.TabIndex = 4;
            this.SongMgrCfg_CustomSingerTypeStructure3_Label.Text = "樂團:";
            // 
            // SongMgrCfg_CustomSingerTypeStructure5_ComboBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.Location = new System.Drawing.Point(107, 204);
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.Name = "SongMgrCfg_CustomSingerTypeStructure5_ComboBox";
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.Size = new System.Drawing.Size(168, 30);
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.TabIndex = 9;
            this.SongMgrCfg_CustomSingerTypeStructure5_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CustomSingerTypeStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_CustomSingerTypeStructure4_Label
            // 
            this.SongMgrCfg_CustomSingerTypeStructure4_Label.AutoSize = true;
            this.SongMgrCfg_CustomSingerTypeStructure4_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure4_Label.Location = new System.Drawing.Point(13, 166);
            this.SongMgrCfg_CustomSingerTypeStructure4_Label.Margin = new System.Windows.Forms.Padding(3, 10, 6, 10);
            this.SongMgrCfg_CustomSingerTypeStructure4_Label.Name = "SongMgrCfg_CustomSingerTypeStructure4_Label";
            this.SongMgrCfg_CustomSingerTypeStructure4_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_CustomSingerTypeStructure4_Label.TabIndex = 6;
            this.SongMgrCfg_CustomSingerTypeStructure4_Label.Text = "合唱:";
            // 
            // SongMgrCfg_CustomSingerTypeStructure4_ComboBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.Location = new System.Drawing.Point(107, 162);
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.Name = "SongMgrCfg_CustomSingerTypeStructure4_ComboBox";
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.Size = new System.Drawing.Size(168, 30);
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.TabIndex = 7;
            this.SongMgrCfg_CustomSingerTypeStructure4_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CustomSingerTypeStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_CustomSingerTypeStructure5_Label
            // 
            this.SongMgrCfg_CustomSingerTypeStructure5_Label.AutoSize = true;
            this.SongMgrCfg_CustomSingerTypeStructure5_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure5_Label.Location = new System.Drawing.Point(13, 208);
            this.SongMgrCfg_CustomSingerTypeStructure5_Label.Margin = new System.Windows.Forms.Padding(3, 10, 6, 10);
            this.SongMgrCfg_CustomSingerTypeStructure5_Label.Name = "SongMgrCfg_CustomSingerTypeStructure5_Label";
            this.SongMgrCfg_CustomSingerTypeStructure5_Label.Size = new System.Drawing.Size(65, 22);
            this.SongMgrCfg_CustomSingerTypeStructure5_Label.TabIndex = 8;
            this.SongMgrCfg_CustomSingerTypeStructure5_Label.Text = "外國男:";
            // 
            // SongMgrCfg_CustomSingerTypeStructure3_ComboBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.Location = new System.Drawing.Point(107, 120);
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.Name = "SongMgrCfg_CustomSingerTypeStructure3_ComboBox";
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.Size = new System.Drawing.Size(168, 30);
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.TabIndex = 5;
            this.SongMgrCfg_CustomSingerTypeStructure3_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CustomSingerTypeStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_CustomSingerTypeStructure6_Label
            // 
            this.SongMgrCfg_CustomSingerTypeStructure6_Label.AutoSize = true;
            this.SongMgrCfg_CustomSingerTypeStructure6_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure6_Label.Location = new System.Drawing.Point(13, 250);
            this.SongMgrCfg_CustomSingerTypeStructure6_Label.Margin = new System.Windows.Forms.Padding(3, 10, 6, 10);
            this.SongMgrCfg_CustomSingerTypeStructure6_Label.Name = "SongMgrCfg_CustomSingerTypeStructure6_Label";
            this.SongMgrCfg_CustomSingerTypeStructure6_Label.Size = new System.Drawing.Size(65, 22);
            this.SongMgrCfg_CustomSingerTypeStructure6_Label.TabIndex = 10;
            this.SongMgrCfg_CustomSingerTypeStructure6_Label.Text = "外國女:";
            // 
            // SongMgrCfg_CustomSingerTypeStructure7_ComboBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.Location = new System.Drawing.Point(107, 288);
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.Name = "SongMgrCfg_CustomSingerTypeStructure7_ComboBox";
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.Size = new System.Drawing.Size(168, 30);
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.TabIndex = 13;
            this.SongMgrCfg_CustomSingerTypeStructure7_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CustomSingerTypeStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_CustomSingerTypeStructure7_Label
            // 
            this.SongMgrCfg_CustomSingerTypeStructure7_Label.AutoSize = true;
            this.SongMgrCfg_CustomSingerTypeStructure7_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure7_Label.Location = new System.Drawing.Point(13, 292);
            this.SongMgrCfg_CustomSingerTypeStructure7_Label.Margin = new System.Windows.Forms.Padding(3, 10, 6, 10);
            this.SongMgrCfg_CustomSingerTypeStructure7_Label.Name = "SongMgrCfg_CustomSingerTypeStructure7_Label";
            this.SongMgrCfg_CustomSingerTypeStructure7_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMgrCfg_CustomSingerTypeStructure7_Label.TabIndex = 12;
            this.SongMgrCfg_CustomSingerTypeStructure7_Label.Text = "外國樂團:";
            // 
            // SongMgrCfg_CustomSingerTypeStructure2_ComboBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.Location = new System.Drawing.Point(107, 78);
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.Name = "SongMgrCfg_CustomSingerTypeStructure2_ComboBox";
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.Size = new System.Drawing.Size(168, 30);
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.TabIndex = 3;
            this.SongMgrCfg_CustomSingerTypeStructure2_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CustomSingerTypeStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_CustomSingerTypeStructure8_Label
            // 
            this.SongMgrCfg_CustomSingerTypeStructure8_Label.AutoSize = true;
            this.SongMgrCfg_CustomSingerTypeStructure8_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure8_Label.Location = new System.Drawing.Point(13, 334);
            this.SongMgrCfg_CustomSingerTypeStructure8_Label.Margin = new System.Windows.Forms.Padding(3, 10, 6, 10);
            this.SongMgrCfg_CustomSingerTypeStructure8_Label.Name = "SongMgrCfg_CustomSingerTypeStructure8_Label";
            this.SongMgrCfg_CustomSingerTypeStructure8_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_CustomSingerTypeStructure8_Label.TabIndex = 14;
            this.SongMgrCfg_CustomSingerTypeStructure8_Label.Text = "其它:";
            // 
            // SongMgrCfg_CustomSingerTypeStructure1_ComboBox
            // 
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.Location = new System.Drawing.Point(107, 36);
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.Name = "SongMgrCfg_CustomSingerTypeStructure1_ComboBox";
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.Size = new System.Drawing.Size(168, 30);
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.TabIndex = 1;
            this.SongMgrCfg_CustomSingerTypeStructure1_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_CustomSingerTypeStructure_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_Tooltip_GroupBox
            // 
            this.SongMgrCfg_Tooltip_GroupBox.Controls.Add(this.SongMgrCfg_Tooltip_Label);
            this.SongMgrCfg_Tooltip_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 1.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Tooltip_GroupBox.Location = new System.Drawing.Point(23, 621);
            this.SongMgrCfg_Tooltip_GroupBox.Margin = new System.Windows.Forms.Padding(3, 12, 6, 3);
            this.SongMgrCfg_Tooltip_GroupBox.Name = "SongMgrCfg_Tooltip_GroupBox";
            this.SongMgrCfg_Tooltip_GroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.SongMgrCfg_Tooltip_GroupBox.Size = new System.Drawing.Size(830, 35);
            this.SongMgrCfg_Tooltip_GroupBox.TabIndex = 4;
            this.SongMgrCfg_Tooltip_GroupBox.TabStop = false;
            // 
            // SongMgrCfg_Tooltip_Label
            // 
            this.SongMgrCfg_Tooltip_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Tooltip_Label.ForeColor = System.Drawing.Color.Red;
            this.SongMgrCfg_Tooltip_Label.Location = new System.Drawing.Point(0, 0);
            this.SongMgrCfg_Tooltip_Label.Margin = new System.Windows.Forms.Padding(0);
            this.SongMgrCfg_Tooltip_Label.Name = "SongMgrCfg_Tooltip_Label";
            this.SongMgrCfg_Tooltip_Label.Size = new System.Drawing.Size(830, 35);
            this.SongMgrCfg_Tooltip_Label.TabIndex = 0;
            this.SongMgrCfg_Tooltip_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SongMgrCfg_Tooltip_Label.UseMnemonic = false;
            // 
            // SongMgrCfg_SongID_GroupBox
            // 
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang10Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang10Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang9Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang9Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang8Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang8Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang7Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang7Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang6Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang6Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang5Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang5Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang4Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang4Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang3Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang3Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang2Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang2Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang1Code_TextBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_Lang1Code_Label);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_MaxDigitCode_ComboBox);
            this.SongMgrCfg_SongID_GroupBox.Controls.Add(this.SongMgrCfg_MaxDigitCode_Label);
            this.SongMgrCfg_SongID_GroupBox.Location = new System.Drawing.Point(23, 286);
            this.SongMgrCfg_SongID_GroupBox.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.SongMgrCfg_SongID_GroupBox.Name = "SongMgrCfg_SongID_GroupBox";
            this.SongMgrCfg_SongID_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMgrCfg_SongID_GroupBox.Size = new System.Drawing.Size(327, 320);
            this.SongMgrCfg_SongID_GroupBox.TabIndex = 1;
            this.SongMgrCfg_SongID_GroupBox.TabStop = false;
            this.SongMgrCfg_SongID_GroupBox.Text = "歌曲編號";
            // 
            // SongMgrCfg_Lang10Code_TextBox
            // 
            this.SongMgrCfg_Lang10Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang10Code_TextBox.Location = new System.Drawing.Point(230, 274);
            this.SongMgrCfg_Lang10Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.SongMgrCfg_Lang10Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang10Code_TextBox.Name = "SongMgrCfg_Lang10Code_TextBox";
            this.SongMgrCfg_Lang10Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang10Code_TextBox.TabIndex = 21;
            this.SongMgrCfg_Lang10Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang10Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang10Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang10Code_Label
            // 
            this.SongMgrCfg_Lang10Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang10Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang10Code_Label.Location = new System.Drawing.Point(170, 278);
            this.SongMgrCfg_Lang10Code_Label.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_Lang10Code_Label.Name = "SongMgrCfg_Lang10Code_Label";
            this.SongMgrCfg_Lang10Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang10Code_Label.TabIndex = 20;
            this.SongMgrCfg_Lang10Code_Label.Text = "其它:";
            // 
            // SongMgrCfg_Lang9Code_TextBox
            // 
            this.SongMgrCfg_Lang9Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang9Code_TextBox.Location = new System.Drawing.Point(230, 228);
            this.SongMgrCfg_Lang9Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.SongMgrCfg_Lang9Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang9Code_TextBox.Name = "SongMgrCfg_Lang9Code_TextBox";
            this.SongMgrCfg_Lang9Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang9Code_TextBox.TabIndex = 19;
            this.SongMgrCfg_Lang9Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang9Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang9Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang9Code_Label
            // 
            this.SongMgrCfg_Lang9Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang9Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang9Code_Label.Location = new System.Drawing.Point(170, 232);
            this.SongMgrCfg_Lang9Code_Label.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_Lang9Code_Label.Name = "SongMgrCfg_Lang9Code_Label";
            this.SongMgrCfg_Lang9Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang9Code_Label.TabIndex = 18;
            this.SongMgrCfg_Lang9Code_Label.Text = "兒歌:";
            // 
            // SongMgrCfg_Lang8Code_TextBox
            // 
            this.SongMgrCfg_Lang8Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang8Code_TextBox.Location = new System.Drawing.Point(230, 182);
            this.SongMgrCfg_Lang8Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.SongMgrCfg_Lang8Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang8Code_TextBox.Name = "SongMgrCfg_Lang8Code_TextBox";
            this.SongMgrCfg_Lang8Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang8Code_TextBox.TabIndex = 17;
            this.SongMgrCfg_Lang8Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang8Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang8Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang8Code_Label
            // 
            this.SongMgrCfg_Lang8Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang8Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang8Code_Label.Location = new System.Drawing.Point(170, 186);
            this.SongMgrCfg_Lang8Code_Label.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_Lang8Code_Label.Name = "SongMgrCfg_Lang8Code_Label";
            this.SongMgrCfg_Lang8Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang8Code_Label.TabIndex = 16;
            this.SongMgrCfg_Lang8Code_Label.Text = "韓語:";
            // 
            // SongMgrCfg_Lang7Code_TextBox
            // 
            this.SongMgrCfg_Lang7Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang7Code_TextBox.Location = new System.Drawing.Point(230, 136);
            this.SongMgrCfg_Lang7Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.SongMgrCfg_Lang7Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang7Code_TextBox.Name = "SongMgrCfg_Lang7Code_TextBox";
            this.SongMgrCfg_Lang7Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang7Code_TextBox.TabIndex = 15;
            this.SongMgrCfg_Lang7Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang7Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang7Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang7Code_Label
            // 
            this.SongMgrCfg_Lang7Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang7Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang7Code_Label.Location = new System.Drawing.Point(170, 140);
            this.SongMgrCfg_Lang7Code_Label.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_Lang7Code_Label.Name = "SongMgrCfg_Lang7Code_Label";
            this.SongMgrCfg_Lang7Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang7Code_Label.TabIndex = 14;
            this.SongMgrCfg_Lang7Code_Label.Text = "原民:";
            // 
            // SongMgrCfg_Lang6Code_TextBox
            // 
            this.SongMgrCfg_Lang6Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang6Code_TextBox.Location = new System.Drawing.Point(230, 90);
            this.SongMgrCfg_Lang6Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongMgrCfg_Lang6Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang6Code_TextBox.Name = "SongMgrCfg_Lang6Code_TextBox";
            this.SongMgrCfg_Lang6Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang6Code_TextBox.TabIndex = 13;
            this.SongMgrCfg_Lang6Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang6Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang6Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang6Code_Label
            // 
            this.SongMgrCfg_Lang6Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang6Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang6Code_Label.Location = new System.Drawing.Point(170, 94);
            this.SongMgrCfg_Lang6Code_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongMgrCfg_Lang6Code_Label.Name = "SongMgrCfg_Lang6Code_Label";
            this.SongMgrCfg_Lang6Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang6Code_Label.TabIndex = 12;
            this.SongMgrCfg_Lang6Code_Label.Text = "客語:";
            // 
            // SongMgrCfg_Lang5Code_TextBox
            // 
            this.SongMgrCfg_Lang5Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang5Code_TextBox.Location = new System.Drawing.Point(76, 274);
            this.SongMgrCfg_Lang5Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.SongMgrCfg_Lang5Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang5Code_TextBox.Name = "SongMgrCfg_Lang5Code_TextBox";
            this.SongMgrCfg_Lang5Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang5Code_TextBox.TabIndex = 11;
            this.SongMgrCfg_Lang5Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang5Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang5Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang5Code_Label
            // 
            this.SongMgrCfg_Lang5Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang5Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang5Code_Label.Location = new System.Drawing.Point(16, 278);
            this.SongMgrCfg_Lang5Code_Label.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_Lang5Code_Label.Name = "SongMgrCfg_Lang5Code_Label";
            this.SongMgrCfg_Lang5Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang5Code_Label.TabIndex = 10;
            this.SongMgrCfg_Lang5Code_Label.Text = "英語:";
            // 
            // SongMgrCfg_Lang4Code_TextBox
            // 
            this.SongMgrCfg_Lang4Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang4Code_TextBox.Location = new System.Drawing.Point(76, 228);
            this.SongMgrCfg_Lang4Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.SongMgrCfg_Lang4Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang4Code_TextBox.Name = "SongMgrCfg_Lang4Code_TextBox";
            this.SongMgrCfg_Lang4Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang4Code_TextBox.TabIndex = 9;
            this.SongMgrCfg_Lang4Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang4Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang4Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang4Code_Label
            // 
            this.SongMgrCfg_Lang4Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang4Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang4Code_Label.Location = new System.Drawing.Point(16, 232);
            this.SongMgrCfg_Lang4Code_Label.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_Lang4Code_Label.Name = "SongMgrCfg_Lang4Code_Label";
            this.SongMgrCfg_Lang4Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang4Code_Label.TabIndex = 8;
            this.SongMgrCfg_Lang4Code_Label.Text = "日語:";
            // 
            // SongMgrCfg_Lang3Code_TextBox
            // 
            this.SongMgrCfg_Lang3Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang3Code_TextBox.Location = new System.Drawing.Point(76, 182);
            this.SongMgrCfg_Lang3Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.SongMgrCfg_Lang3Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang3Code_TextBox.Name = "SongMgrCfg_Lang3Code_TextBox";
            this.SongMgrCfg_Lang3Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang3Code_TextBox.TabIndex = 7;
            this.SongMgrCfg_Lang3Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang3Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang3Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang3Code_Label
            // 
            this.SongMgrCfg_Lang3Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang3Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang3Code_Label.Location = new System.Drawing.Point(16, 186);
            this.SongMgrCfg_Lang3Code_Label.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_Lang3Code_Label.Name = "SongMgrCfg_Lang3Code_Label";
            this.SongMgrCfg_Lang3Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang3Code_Label.TabIndex = 6;
            this.SongMgrCfg_Lang3Code_Label.Text = "粵語:";
            // 
            // SongMgrCfg_Lang2Code_TextBox
            // 
            this.SongMgrCfg_Lang2Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang2Code_TextBox.Location = new System.Drawing.Point(76, 136);
            this.SongMgrCfg_Lang2Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.SongMgrCfg_Lang2Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang2Code_TextBox.Name = "SongMgrCfg_Lang2Code_TextBox";
            this.SongMgrCfg_Lang2Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang2Code_TextBox.TabIndex = 5;
            this.SongMgrCfg_Lang2Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang2Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang2Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang2Code_Label
            // 
            this.SongMgrCfg_Lang2Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang2Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang2Code_Label.Location = new System.Drawing.Point(16, 140);
            this.SongMgrCfg_Lang2Code_Label.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_Lang2Code_Label.Name = "SongMgrCfg_Lang2Code_Label";
            this.SongMgrCfg_Lang2Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang2Code_Label.TabIndex = 4;
            this.SongMgrCfg_Lang2Code_Label.Text = "台語:";
            // 
            // SongMgrCfg_Lang1Code_TextBox
            // 
            this.SongMgrCfg_Lang1Code_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang1Code_TextBox.Location = new System.Drawing.Point(76, 90);
            this.SongMgrCfg_Lang1Code_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongMgrCfg_Lang1Code_TextBox.MaxLength = 6;
            this.SongMgrCfg_Lang1Code_TextBox.Name = "SongMgrCfg_Lang1Code_TextBox";
            this.SongMgrCfg_Lang1Code_TextBox.Size = new System.Drawing.Size(80, 30);
            this.SongMgrCfg_Lang1Code_TextBox.TabIndex = 3;
            this.SongMgrCfg_Lang1Code_TextBox.Enter += new System.EventHandler(this.SongMgrCfg_LangCode_TextBox_Enter);
            this.SongMgrCfg_Lang1Code_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMgrCfg_Lang1Code_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMgrCfg_LangCode_TextBox_Validating);
            // 
            // SongMgrCfg_Lang1Code_Label
            // 
            this.SongMgrCfg_Lang1Code_Label.AutoSize = true;
            this.SongMgrCfg_Lang1Code_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_Lang1Code_Label.Location = new System.Drawing.Point(16, 94);
            this.SongMgrCfg_Lang1Code_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongMgrCfg_Lang1Code_Label.Name = "SongMgrCfg_Lang1Code_Label";
            this.SongMgrCfg_Lang1Code_Label.Size = new System.Drawing.Size(48, 22);
            this.SongMgrCfg_Lang1Code_Label.TabIndex = 2;
            this.SongMgrCfg_Lang1Code_Label.Text = "國語:";
            // 
            // SongMgrCfg_MaxDigitCode_ComboBox
            // 
            this.SongMgrCfg_MaxDigitCode_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_MaxDigitCode_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_MaxDigitCode_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_MaxDigitCode_ComboBox.Location = new System.Drawing.Point(144, 40);
            this.SongMgrCfg_MaxDigitCode_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMgrCfg_MaxDigitCode_ComboBox.Name = "SongMgrCfg_MaxDigitCode_ComboBox";
            this.SongMgrCfg_MaxDigitCode_ComboBox.Size = new System.Drawing.Size(167, 30);
            this.SongMgrCfg_MaxDigitCode_ComboBox.TabIndex = 1;
            this.SongMgrCfg_MaxDigitCode_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_MaxDigitCode_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_MaxDigitCode_Label
            // 
            this.SongMgrCfg_MaxDigitCode_Label.AutoSize = true;
            this.SongMgrCfg_MaxDigitCode_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_MaxDigitCode_Label.Location = new System.Drawing.Point(16, 44);
            this.SongMgrCfg_MaxDigitCode_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMgrCfg_MaxDigitCode_Label.Name = "SongMgrCfg_MaxDigitCode_Label";
            this.SongMgrCfg_MaxDigitCode_Label.Size = new System.Drawing.Size(116, 22);
            this.SongMgrCfg_MaxDigitCode_Label.TabIndex = 0;
            this.SongMgrCfg_MaxDigitCode_Label.Text = "歌庫編碼位數:";
            // 
            // SongMgrCfg_General_GroupBox
            // 
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_BackupRemoveSong_CheckBox);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_SongTrackMode_CheckBox);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_SongAddMode_ComboBox);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_SongAddMode_Label);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_DestFolder_Button);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_DestFolder_TextBox);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_DestFolder_Label);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_SupportFormat_TextBox);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_SupportFormat_Label);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_DBFile_Button);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_DBFile_TextBox);
            this.SongMgrCfg_General_GroupBox.Controls.Add(this.SongMgrCfg_DBFile_Label);
            this.SongMgrCfg_General_GroupBox.Location = new System.Drawing.Point(23, 23);
            this.SongMgrCfg_General_GroupBox.Name = "SongMgrCfg_General_GroupBox";
            this.SongMgrCfg_General_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMgrCfg_General_GroupBox.Size = new System.Drawing.Size(952, 240);
            this.SongMgrCfg_General_GroupBox.TabIndex = 0;
            this.SongMgrCfg_General_GroupBox.TabStop = false;
            this.SongMgrCfg_General_GroupBox.Text = "歌庫設定";
            // 
            // SongMgrCfg_BackupRemoveSong_CheckBox
            // 
            this.SongMgrCfg_BackupRemoveSong_CheckBox.AutoSize = true;
            this.SongMgrCfg_BackupRemoveSong_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_BackupRemoveSong_CheckBox.Location = new System.Drawing.Point(499, 192);
            this.SongMgrCfg_BackupRemoveSong_CheckBox.Margin = new System.Windows.Forms.Padding(6, 12, 12, 12);
            this.SongMgrCfg_BackupRemoveSong_CheckBox.Name = "SongMgrCfg_BackupRemoveSong_CheckBox";
            this.SongMgrCfg_BackupRemoveSong_CheckBox.Size = new System.Drawing.Size(134, 26);
            this.SongMgrCfg_BackupRemoveSong_CheckBox.TabIndex = 10;
            this.SongMgrCfg_BackupRemoveSong_CheckBox.Text = "備份移除歌曲";
            this.SongMgrCfg_BackupRemoveSong_CheckBox.UseVisualStyleBackColor = true;
            this.SongMgrCfg_BackupRemoveSong_CheckBox.CheckedChanged += new System.EventHandler(this.SongMgrCfg_BackupRemoveSong_CheckBox_CheckedChanged);
            // 
            // SongMgrCfg_SongTrackMode_CheckBox
            // 
            this.SongMgrCfg_SongTrackMode_CheckBox.AutoSize = true;
            this.SongMgrCfg_SongTrackMode_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongTrackMode_CheckBox.Location = new System.Drawing.Point(651, 192);
            this.SongMgrCfg_SongTrackMode_CheckBox.Margin = new System.Windows.Forms.Padding(6, 12, 6, 12);
            this.SongMgrCfg_SongTrackMode_CheckBox.Name = "SongMgrCfg_SongTrackMode_CheckBox";
            this.SongMgrCfg_SongTrackMode_CheckBox.Size = new System.Drawing.Size(285, 26);
            this.SongMgrCfg_SongTrackMode_CheckBox.TabIndex = 11;
            this.SongMgrCfg_SongTrackMode_CheckBox.Text = "聲道設定相容於 GodLiu 加歌程式";
            this.SongMgrCfg_SongTrackMode_CheckBox.UseVisualStyleBackColor = true;
            this.SongMgrCfg_SongTrackMode_CheckBox.CheckedChanged += new System.EventHandler(this.SongMgrCfg_SongTrackMode_CheckBox_CheckedChanged);
            // 
            // SongMgrCfg_SongAddMode_ComboBox
            // 
            this.SongMgrCfg_SongAddMode_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongMgrCfg_SongAddMode_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongAddMode_ComboBox.FormattingEnabled = true;
            this.SongMgrCfg_SongAddMode_ComboBox.Location = new System.Drawing.Point(144, 190);
            this.SongMgrCfg_SongAddMode_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMgrCfg_SongAddMode_ComboBox.Name = "SongMgrCfg_SongAddMode_ComboBox";
            this.SongMgrCfg_SongAddMode_ComboBox.Size = new System.Drawing.Size(320, 30);
            this.SongMgrCfg_SongAddMode_ComboBox.TabIndex = 9;
            this.SongMgrCfg_SongAddMode_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongMgrCfg_SongAddMode_ComboBox_SelectedIndexChanged);
            // 
            // SongMgrCfg_SongAddMode_Label
            // 
            this.SongMgrCfg_SongAddMode_Label.AutoSize = true;
            this.SongMgrCfg_SongAddMode_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SongAddMode_Label.Location = new System.Drawing.Point(16, 194);
            this.SongMgrCfg_SongAddMode_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMgrCfg_SongAddMode_Label.Name = "SongMgrCfg_SongAddMode_Label";
            this.SongMgrCfg_SongAddMode_Label.Size = new System.Drawing.Size(116, 22);
            this.SongMgrCfg_SongAddMode_Label.TabIndex = 8;
            this.SongMgrCfg_SongAddMode_Label.Text = "歌庫加歌模式:";
            // 
            // SongMgrCfg_DestFolder_Button
            // 
            this.SongMgrCfg_DestFolder_Button.AutoSize = true;
            this.SongMgrCfg_DestFolder_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_DestFolder_Button.Location = new System.Drawing.Point(866, 139);
            this.SongMgrCfg_DestFolder_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMgrCfg_DestFolder_Button.Name = "SongMgrCfg_DestFolder_Button";
            this.SongMgrCfg_DestFolder_Button.Size = new System.Drawing.Size(70, 32);
            this.SongMgrCfg_DestFolder_Button.TabIndex = 7;
            this.SongMgrCfg_DestFolder_Button.Text = "瀏覽";
            this.SongMgrCfg_DestFolder_Button.UseVisualStyleBackColor = true;
            this.SongMgrCfg_DestFolder_Button.Click += new System.EventHandler(this.SongMgrCfg_DestFolder_Button_Click);
            // 
            // SongMgrCfg_DestFolder_TextBox
            // 
            this.SongMgrCfg_DestFolder_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_DestFolder_TextBox.Location = new System.Drawing.Point(127, 140);
            this.SongMgrCfg_DestFolder_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMgrCfg_DestFolder_TextBox.Name = "SongMgrCfg_DestFolder_TextBox";
            this.SongMgrCfg_DestFolder_TextBox.ReadOnly = true;
            this.SongMgrCfg_DestFolder_TextBox.Size = new System.Drawing.Size(727, 30);
            this.SongMgrCfg_DestFolder_TextBox.TabIndex = 6;
            // 
            // SongMgrCfg_DestFolder_Label
            // 
            this.SongMgrCfg_DestFolder_Label.AutoSize = true;
            this.SongMgrCfg_DestFolder_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_DestFolder_Label.Location = new System.Drawing.Point(16, 144);
            this.SongMgrCfg_DestFolder_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMgrCfg_DestFolder_Label.Name = "SongMgrCfg_DestFolder_Label";
            this.SongMgrCfg_DestFolder_Label.Size = new System.Drawing.Size(99, 22);
            this.SongMgrCfg_DestFolder_Label.TabIndex = 5;
            this.SongMgrCfg_DestFolder_Label.Text = "歌庫資料夾:";
            // 
            // SongMgrCfg_SupportFormat_TextBox
            // 
            this.SongMgrCfg_SupportFormat_TextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.SongMgrCfg_SupportFormat_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SongMgrCfg_SupportFormat_TextBox.Location = new System.Drawing.Point(127, 90);
            this.SongMgrCfg_SupportFormat_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMgrCfg_SupportFormat_TextBox.Name = "SongMgrCfg_SupportFormat_TextBox";
            this.SongMgrCfg_SupportFormat_TextBox.Size = new System.Drawing.Size(809, 30);
            this.SongMgrCfg_SupportFormat_TextBox.TabIndex = 4;
            this.SongMgrCfg_SupportFormat_TextBox.TextChanged += new System.EventHandler(this.SongMgrCfg_SupportFormat_TextBox_TextChanged);
            // 
            // SongMgrCfg_SupportFormat_Label
            // 
            this.SongMgrCfg_SupportFormat_Label.AutoSize = true;
            this.SongMgrCfg_SupportFormat_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_SupportFormat_Label.Location = new System.Drawing.Point(16, 94);
            this.SongMgrCfg_SupportFormat_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMgrCfg_SupportFormat_Label.Name = "SongMgrCfg_SupportFormat_Label";
            this.SongMgrCfg_SupportFormat_Label.Size = new System.Drawing.Size(99, 22);
            this.SongMgrCfg_SupportFormat_Label.TabIndex = 3;
            this.SongMgrCfg_SupportFormat_Label.Text = "支援影音檔:";
            // 
            // SongMgrCfg_DBFile_Button
            // 
            this.SongMgrCfg_DBFile_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_DBFile_Button.Location = new System.Drawing.Point(866, 39);
            this.SongMgrCfg_DBFile_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMgrCfg_DBFile_Button.Name = "SongMgrCfg_DBFile_Button";
            this.SongMgrCfg_DBFile_Button.Size = new System.Drawing.Size(70, 32);
            this.SongMgrCfg_DBFile_Button.TabIndex = 2;
            this.SongMgrCfg_DBFile_Button.Text = "瀏覽";
            this.SongMgrCfg_DBFile_Button.UseVisualStyleBackColor = true;
            this.SongMgrCfg_DBFile_Button.Click += new System.EventHandler(this.SongMgrCfg_DBFile_Button_Click);
            // 
            // SongMgrCfg_DBFile_TextBox
            // 
            this.SongMgrCfg_DBFile_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_DBFile_TextBox.Location = new System.Drawing.Point(127, 40);
            this.SongMgrCfg_DBFile_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMgrCfg_DBFile_TextBox.Name = "SongMgrCfg_DBFile_TextBox";
            this.SongMgrCfg_DBFile_TextBox.ReadOnly = true;
            this.SongMgrCfg_DBFile_TextBox.Size = new System.Drawing.Size(727, 30);
            this.SongMgrCfg_DBFile_TextBox.TabIndex = 1;
            // 
            // SongMgrCfg_DBFile_Label
            // 
            this.SongMgrCfg_DBFile_Label.AutoSize = true;
            this.SongMgrCfg_DBFile_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMgrCfg_DBFile_Label.Location = new System.Drawing.Point(16, 44);
            this.SongMgrCfg_DBFile_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMgrCfg_DBFile_Label.Name = "SongMgrCfg_DBFile_Label";
            this.SongMgrCfg_DBFile_Label.Size = new System.Drawing.Size(99, 22);
            this.SongMgrCfg_DBFile_Label.TabIndex = 0;
            this.SongMgrCfg_DBFile_Label.Text = "資料庫檔案:";
            // 
            // SongMgrCfg_Save_Button
            // 
            this.SongMgrCfg_Save_Button.AutoSize = true;
            this.SongMgrCfg_Save_Button.Location = new System.Drawing.Point(865, 621);
            this.SongMgrCfg_Save_Button.Margin = new System.Windows.Forms.Padding(6);
            this.SongMgrCfg_Save_Button.Name = "SongMgrCfg_Save_Button";
            this.SongMgrCfg_Save_Button.Size = new System.Drawing.Size(110, 35);
            this.SongMgrCfg_Save_Button.TabIndex = 5;
            this.SongMgrCfg_Save_Button.Text = "儲存設定";
            this.SongMgrCfg_Save_Button.UseVisualStyleBackColor = true;
            this.SongMgrCfg_Save_Button.Click += new System.EventHandler(this.SongMgrCfg_Save_Button_Click);
            // 
            // SongMaintenance_TabPage
            // 
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_Save_Button);
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_SongPathChange_GroupBox);
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_PlayCount_GroupBox);
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_TabControl);
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_VolumeChange_GroupBox);
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_TrackExchange_GroupBox);
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_CodeConv_GroupBox);
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_Tooltip_GroupBox);
            this.SongMaintenance_TabPage.Controls.Add(this.SongMaintenance_SpellCorrect_GroupBox);
            this.SongMaintenance_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMaintenance_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongMaintenance_TabPage.Name = "SongMaintenance_TabPage";
            this.SongMaintenance_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.SongMaintenance_TabPage.Size = new System.Drawing.Size(998, 684);
            this.SongMaintenance_TabPage.TabIndex = 7;
            this.SongMaintenance_TabPage.Text = "歌庫維護";
            this.SongMaintenance_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMaintenance_Save_Button
            // 
            this.SongMaintenance_Save_Button.AutoSize = true;
            this.SongMaintenance_Save_Button.Enabled = false;
            this.SongMaintenance_Save_Button.Location = new System.Drawing.Point(865, 621);
            this.SongMaintenance_Save_Button.Margin = new System.Windows.Forms.Padding(6, 12, 6, 6);
            this.SongMaintenance_Save_Button.Name = "SongMaintenance_Save_Button";
            this.SongMaintenance_Save_Button.Size = new System.Drawing.Size(110, 35);
            this.SongMaintenance_Save_Button.TabIndex = 8;
            this.SongMaintenance_Save_Button.Text = "儲存設定";
            this.SongMaintenance_Save_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_Save_Button.Click += new System.EventHandler(this.SongMaintenance_Save_Button_Click);
            // 
            // SongMaintenance_SongPathChange_GroupBox
            // 
            this.SongMaintenance_SongPathChange_GroupBox.Controls.Add(this.SongMaintenance_DestSongPath_TextBox);
            this.SongMaintenance_SongPathChange_GroupBox.Controls.Add(this.SongMaintenance_DestSongPath_Label);
            this.SongMaintenance_SongPathChange_GroupBox.Controls.Add(this.SongMaintenance_SrcSongPath_TextBox);
            this.SongMaintenance_SongPathChange_GroupBox.Controls.Add(this.SongMaintenance_SrcSongPath_Label);
            this.SongMaintenance_SongPathChange_GroupBox.Controls.Add(this.SongMaintenance_SongPathChange_Button);
            this.SongMaintenance_SongPathChange_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_SongPathChange_GroupBox.Location = new System.Drawing.Point(23, 431);
            this.SongMaintenance_SongPathChange_GroupBox.Name = "SongMaintenance_SongPathChange_GroupBox";
            this.SongMaintenance_SongPathChange_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_SongPathChange_GroupBox.Size = new System.Drawing.Size(342, 175);
            this.SongMaintenance_SongPathChange_GroupBox.TabIndex = 4;
            this.SongMaintenance_SongPathChange_GroupBox.TabStop = false;
            this.SongMaintenance_SongPathChange_GroupBox.Text = "歌曲路徑變更";
            // 
            // SongMaintenance_DestSongPath_TextBox
            // 
            this.SongMaintenance_DestSongPath_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DestSongPath_TextBox.Location = new System.Drawing.Point(110, 88);
            this.SongMaintenance_DestSongPath_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongMaintenance_DestSongPath_TextBox.Name = "SongMaintenance_DestSongPath_TextBox";
            this.SongMaintenance_DestSongPath_TextBox.ReadOnly = true;
            this.SongMaintenance_DestSongPath_TextBox.Size = new System.Drawing.Size(216, 30);
            this.SongMaintenance_DestSongPath_TextBox.TabIndex = 3;
            // 
            // SongMaintenance_DestSongPath_Label
            // 
            this.SongMaintenance_DestSongPath_Label.AutoSize = true;
            this.SongMaintenance_DestSongPath_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DestSongPath_Label.Location = new System.Drawing.Point(16, 92);
            this.SongMaintenance_DestSongPath_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongMaintenance_DestSongPath_Label.Name = "SongMaintenance_DestSongPath_Label";
            this.SongMaintenance_DestSongPath_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_DestSongPath_Label.TabIndex = 2;
            this.SongMaintenance_DestSongPath_Label.Text = "目標路徑:";
            // 
            // SongMaintenance_SrcSongPath_TextBox
            // 
            this.SongMaintenance_SrcSongPath_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_SrcSongPath_TextBox.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.SongMaintenance_SrcSongPath_TextBox.Location = new System.Drawing.Point(110, 40);
            this.SongMaintenance_SrcSongPath_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongMaintenance_SrcSongPath_TextBox.Name = "SongMaintenance_SrcSongPath_TextBox";
            this.SongMaintenance_SrcSongPath_TextBox.Size = new System.Drawing.Size(216, 30);
            this.SongMaintenance_SrcSongPath_TextBox.TabIndex = 1;
            // 
            // SongMaintenance_SrcSongPath_Label
            // 
            this.SongMaintenance_SrcSongPath_Label.AutoSize = true;
            this.SongMaintenance_SrcSongPath_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_SrcSongPath_Label.Location = new System.Drawing.Point(16, 44);
            this.SongMaintenance_SrcSongPath_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongMaintenance_SrcSongPath_Label.Name = "SongMaintenance_SrcSongPath_Label";
            this.SongMaintenance_SrcSongPath_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_SrcSongPath_Label.TabIndex = 0;
            this.SongMaintenance_SrcSongPath_Label.Text = "原始路徑:";
            // 
            // SongMaintenance_SongPathChange_Button
            // 
            this.SongMaintenance_SongPathChange_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_SongPathChange_Button.Location = new System.Drawing.Point(256, 131);
            this.SongMaintenance_SongPathChange_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_SongPathChange_Button.Name = "SongMaintenance_SongPathChange_Button";
            this.SongMaintenance_SongPathChange_Button.Size = new System.Drawing.Size(70, 32);
            this.SongMaintenance_SongPathChange_Button.TabIndex = 4;
            this.SongMaintenance_SongPathChange_Button.Text = "瀏覽";
            this.SongMaintenance_SongPathChange_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_SongPathChange_Button.Click += new System.EventHandler(this.SongMaintenance_SongPathChange_Button_Click);
            // 
            // SongMaintenance_PlayCount_GroupBox
            // 
            this.SongMaintenance_PlayCount_GroupBox.Controls.Add(this.SongMaintenance_PlayCountReset_Button);
            this.SongMaintenance_PlayCount_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_PlayCount_GroupBox.Location = new System.Drawing.Point(23, 329);
            this.SongMaintenance_PlayCount_GroupBox.Name = "SongMaintenance_PlayCount_GroupBox";
            this.SongMaintenance_PlayCount_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_PlayCount_GroupBox.Size = new System.Drawing.Size(342, 96);
            this.SongMaintenance_PlayCount_GroupBox.TabIndex = 3;
            this.SongMaintenance_PlayCount_GroupBox.TabStop = false;
            this.SongMaintenance_PlayCount_GroupBox.Text = "播放次數";
            // 
            // SongMaintenance_PlayCountReset_Button
            // 
            this.SongMaintenance_PlayCountReset_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_PlayCountReset_Button.Location = new System.Drawing.Point(16, 39);
            this.SongMaintenance_PlayCountReset_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_PlayCountReset_Button.Name = "SongMaintenance_PlayCountReset_Button";
            this.SongMaintenance_PlayCountReset_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_PlayCountReset_Button.TabIndex = 0;
            this.SongMaintenance_PlayCountReset_Button.Text = "重置播放次數";
            this.SongMaintenance_PlayCountReset_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_PlayCountReset_Button.Click += new System.EventHandler(this.SongMaintenance_PlayCountReset_Button_Click);
            // 
            // SongMaintenance_TabControl
            // 
            this.SongMaintenance_TabControl.Controls.Add(this.SongMaintenance_Misc_TabPage);
            this.SongMaintenance_TabControl.Controls.Add(this.SongMaintenance_Favorite_TabPage);
            this.SongMaintenance_TabControl.Controls.Add(this.SongMaintenance_CustomLang_TabPage);
            this.SongMaintenance_TabControl.Controls.Add(this.SongMaintenance_MultiSongPath_TabPage);
            this.SongMaintenance_TabControl.Controls.Add(this.SongMaintenance_DBVer_TabPage);
            this.SongMaintenance_TabControl.Location = new System.Drawing.Point(382, 136);
            this.SongMaintenance_TabControl.Margin = new System.Windows.Forms.Padding(14, 14, 3, 3);
            this.SongMaintenance_TabControl.Multiline = true;
            this.SongMaintenance_TabControl.Name = "SongMaintenance_TabControl";
            this.SongMaintenance_TabControl.SelectedIndex = 0;
            this.SongMaintenance_TabControl.Size = new System.Drawing.Size(593, 470);
            this.SongMaintenance_TabControl.TabIndex = 6;
            this.SongMaintenance_TabControl.SelectedIndexChanged += new System.EventHandler(this.SongMaintenance_TabControl_SelectedIndexChanged);
            // 
            // SongMaintenance_Misc_TabPage
            // 
            this.SongMaintenance_Misc_TabPage.Controls.Add(this.SongMaintenance_RebuildSongStructure_GroupBox);
            this.SongMaintenance_Misc_TabPage.Controls.Add(this.SongMaintenance_Phonetics_GroupBox);
            this.SongMaintenance_Misc_TabPage.Controls.Add(this.SongMaintenance_RemoteCfg_GroupBox);
            this.SongMaintenance_Misc_TabPage.Controls.Add(this.SongMaintenance_Misc_GroupBox);
            this.SongMaintenance_Misc_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMaintenance_Misc_TabPage.Name = "SongMaintenance_Misc_TabPage";
            this.SongMaintenance_Misc_TabPage.Padding = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.SongMaintenance_Misc_TabPage.Size = new System.Drawing.Size(585, 432);
            this.SongMaintenance_Misc_TabPage.TabIndex = 2;
            this.SongMaintenance_Misc_TabPage.Text = "維護雜項";
            this.SongMaintenance_Misc_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMaintenance_RebuildSongStructure_GroupBox
            // 
            this.SongMaintenance_RebuildSongStructure_GroupBox.Controls.Add(this.SongMaintenance_RebuildSongStructure_Label);
            this.SongMaintenance_RebuildSongStructure_GroupBox.Controls.Add(this.SongMaintenance_RebuildSongStructure_TextBox);
            this.SongMaintenance_RebuildSongStructure_GroupBox.Controls.Add(this.SongMaintenance_RebuildSongStructure_Button);
            this.SongMaintenance_RebuildSongStructure_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_RebuildSongStructure_GroupBox.Location = new System.Drawing.Point(16, 323);
            this.SongMaintenance_RebuildSongStructure_GroupBox.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.SongMaintenance_RebuildSongStructure_GroupBox.Name = "SongMaintenance_RebuildSongStructure_GroupBox";
            this.SongMaintenance_RebuildSongStructure_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_RebuildSongStructure_GroupBox.Size = new System.Drawing.Size(553, 96);
            this.SongMaintenance_RebuildSongStructure_GroupBox.TabIndex = 3;
            this.SongMaintenance_RebuildSongStructure_GroupBox.TabStop = false;
            this.SongMaintenance_RebuildSongStructure_GroupBox.Text = "歌庫結構重建";
            // 
            // SongMaintenance_RebuildSongStructure_Label
            // 
            this.SongMaintenance_RebuildSongStructure_Label.AutoSize = true;
            this.SongMaintenance_RebuildSongStructure_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_RebuildSongStructure_Label.Location = new System.Drawing.Point(16, 44);
            this.SongMaintenance_RebuildSongStructure_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 13);
            this.SongMaintenance_RebuildSongStructure_Label.Name = "SongMaintenance_RebuildSongStructure_Label";
            this.SongMaintenance_RebuildSongStructure_Label.Size = new System.Drawing.Size(99, 22);
            this.SongMaintenance_RebuildSongStructure_Label.TabIndex = 0;
            this.SongMaintenance_RebuildSongStructure_Label.Text = "重建資料夾:";
            // 
            // SongMaintenance_RebuildSongStructure_TextBox
            // 
            this.SongMaintenance_RebuildSongStructure_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_RebuildSongStructure_TextBox.Location = new System.Drawing.Point(127, 40);
            this.SongMaintenance_RebuildSongStructure_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMaintenance_RebuildSongStructure_TextBox.Name = "SongMaintenance_RebuildSongStructure_TextBox";
            this.SongMaintenance_RebuildSongStructure_TextBox.ReadOnly = true;
            this.SongMaintenance_RebuildSongStructure_TextBox.Size = new System.Drawing.Size(328, 30);
            this.SongMaintenance_RebuildSongStructure_TextBox.TabIndex = 1;
            // 
            // SongMaintenance_RebuildSongStructure_Button
            // 
            this.SongMaintenance_RebuildSongStructure_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_RebuildSongStructure_Button.Location = new System.Drawing.Point(467, 39);
            this.SongMaintenance_RebuildSongStructure_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_RebuildSongStructure_Button.Name = "SongMaintenance_RebuildSongStructure_Button";
            this.SongMaintenance_RebuildSongStructure_Button.Size = new System.Drawing.Size(70, 32);
            this.SongMaintenance_RebuildSongStructure_Button.TabIndex = 2;
            this.SongMaintenance_RebuildSongStructure_Button.Text = "瀏覽";
            this.SongMaintenance_RebuildSongStructure_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_RebuildSongStructure_Button.Click += new System.EventHandler(this.SongMaintenance_RebuildSongStructure_Button_Click);
            // 
            // SongMaintenance_Phonetics_GroupBox
            // 
            this.SongMaintenance_Phonetics_GroupBox.Controls.Add(this.SongMaintenance_NonPhoneticsWordLog_Button);
            this.SongMaintenance_Phonetics_GroupBox.Controls.Add(this.SongMaintenance_PhoneticsExport_Button);
            this.SongMaintenance_Phonetics_GroupBox.Controls.Add(this.SongMaintenance_PhoneticsImport_Button);
            this.SongMaintenance_Phonetics_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Phonetics_GroupBox.Location = new System.Drawing.Point(16, 221);
            this.SongMaintenance_Phonetics_GroupBox.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.SongMaintenance_Phonetics_GroupBox.Name = "SongMaintenance_Phonetics_GroupBox";
            this.SongMaintenance_Phonetics_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_Phonetics_GroupBox.Size = new System.Drawing.Size(553, 96);
            this.SongMaintenance_Phonetics_GroupBox.TabIndex = 2;
            this.SongMaintenance_Phonetics_GroupBox.TabStop = false;
            this.SongMaintenance_Phonetics_GroupBox.Text = "拼音資料";
            // 
            // SongMaintenance_NonPhoneticsWordLog_Button
            // 
            this.SongMaintenance_NonPhoneticsWordLog_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_NonPhoneticsWordLog_Button.Location = new System.Drawing.Point(338, 39);
            this.SongMaintenance_NonPhoneticsWordLog_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_NonPhoneticsWordLog_Button.Name = "SongMaintenance_NonPhoneticsWordLog_Button";
            this.SongMaintenance_NonPhoneticsWordLog_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_NonPhoneticsWordLog_Button.TabIndex = 2;
            this.SongMaintenance_NonPhoneticsWordLog_Button.Text = "記錄無拼音字";
            this.SongMaintenance_NonPhoneticsWordLog_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_NonPhoneticsWordLog_Button.Click += new System.EventHandler(this.SongMaintenance_NonPhoneticsWordLog_Button_Click);
            // 
            // SongMaintenance_PhoneticsExport_Button
            // 
            this.SongMaintenance_PhoneticsExport_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_PhoneticsExport_Button.Location = new System.Drawing.Point(177, 39);
            this.SongMaintenance_PhoneticsExport_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_PhoneticsExport_Button.Name = "SongMaintenance_PhoneticsExport_Button";
            this.SongMaintenance_PhoneticsExport_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_PhoneticsExport_Button.TabIndex = 1;
            this.SongMaintenance_PhoneticsExport_Button.Text = "匯出拼音資料";
            this.SongMaintenance_PhoneticsExport_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_PhoneticsExport_Button.Click += new System.EventHandler(this.SongMaintenance_PhoneticsExport_Button_Click);
            // 
            // SongMaintenance_PhoneticsImport_Button
            // 
            this.SongMaintenance_PhoneticsImport_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_PhoneticsImport_Button.Location = new System.Drawing.Point(16, 39);
            this.SongMaintenance_PhoneticsImport_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_PhoneticsImport_Button.Name = "SongMaintenance_PhoneticsImport_Button";
            this.SongMaintenance_PhoneticsImport_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_PhoneticsImport_Button.TabIndex = 0;
            this.SongMaintenance_PhoneticsImport_Button.Text = "匯入拼音資料";
            this.SongMaintenance_PhoneticsImport_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_PhoneticsImport_Button.Click += new System.EventHandler(this.SongMaintenance_PhoneticsImport_Button_Click);
            // 
            // SongMaintenance_RemoteCfg_GroupBox
            // 
            this.SongMaintenance_RemoteCfg_GroupBox.Controls.Add(this.SongMaintenance_RemoteCfgExport_Button);
            this.SongMaintenance_RemoteCfg_GroupBox.Controls.Add(this.SongMaintenance_RemoteCfgImport_Button);
            this.SongMaintenance_RemoteCfg_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_RemoteCfg_GroupBox.Location = new System.Drawing.Point(16, 119);
            this.SongMaintenance_RemoteCfg_GroupBox.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.SongMaintenance_RemoteCfg_GroupBox.Name = "SongMaintenance_RemoteCfg_GroupBox";
            this.SongMaintenance_RemoteCfg_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_RemoteCfg_GroupBox.Size = new System.Drawing.Size(553, 96);
            this.SongMaintenance_RemoteCfg_GroupBox.TabIndex = 1;
            this.SongMaintenance_RemoteCfg_GroupBox.TabStop = false;
            this.SongMaintenance_RemoteCfg_GroupBox.Text = "遙控設定";
            // 
            // SongMaintenance_RemoteCfgExport_Button
            // 
            this.SongMaintenance_RemoteCfgExport_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_RemoteCfgExport_Button.Location = new System.Drawing.Point(177, 39);
            this.SongMaintenance_RemoteCfgExport_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_RemoteCfgExport_Button.Name = "SongMaintenance_RemoteCfgExport_Button";
            this.SongMaintenance_RemoteCfgExport_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_RemoteCfgExport_Button.TabIndex = 1;
            this.SongMaintenance_RemoteCfgExport_Button.Text = "匯出遙控設定";
            this.SongMaintenance_RemoteCfgExport_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_RemoteCfgExport_Button.Click += new System.EventHandler(this.SongMaintenance_RemoteCfgExport_Button_Click);
            // 
            // SongMaintenance_RemoteCfgImport_Button
            // 
            this.SongMaintenance_RemoteCfgImport_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_RemoteCfgImport_Button.Location = new System.Drawing.Point(16, 39);
            this.SongMaintenance_RemoteCfgImport_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_RemoteCfgImport_Button.Name = "SongMaintenance_RemoteCfgImport_Button";
            this.SongMaintenance_RemoteCfgImport_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_RemoteCfgImport_Button.TabIndex = 0;
            this.SongMaintenance_RemoteCfgImport_Button.Text = "匯入遙控設定";
            this.SongMaintenance_RemoteCfgImport_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_RemoteCfgImport_Button.Click += new System.EventHandler(this.SongMaintenance_RemoteCfgImport_Button_Click);
            // 
            // SongMaintenance_Misc_GroupBox
            // 
            this.SongMaintenance_Misc_GroupBox.Controls.Add(this.SongMaintenance_RemoveEmptyDirs_Button);
            this.SongMaintenance_Misc_GroupBox.Controls.Add(this.SongMaintenance_CompactAccessDB_Button);
            this.SongMaintenance_Misc_GroupBox.Controls.Add(this.SongMaintenance_SongWordCountCorrect_Button);
            this.SongMaintenance_Misc_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Misc_GroupBox.Location = new System.Drawing.Point(16, 17);
            this.SongMaintenance_Misc_GroupBox.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.SongMaintenance_Misc_GroupBox.Name = "SongMaintenance_Misc_GroupBox";
            this.SongMaintenance_Misc_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_Misc_GroupBox.Size = new System.Drawing.Size(553, 96);
            this.SongMaintenance_Misc_GroupBox.TabIndex = 0;
            this.SongMaintenance_Misc_GroupBox.TabStop = false;
            this.SongMaintenance_Misc_GroupBox.Text = "雜項";
            // 
            // SongMaintenance_RemoveEmptyDirs_Button
            // 
            this.SongMaintenance_RemoveEmptyDirs_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_RemoveEmptyDirs_Button.Location = new System.Drawing.Point(177, 39);
            this.SongMaintenance_RemoveEmptyDirs_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_RemoveEmptyDirs_Button.Name = "SongMaintenance_RemoveEmptyDirs_Button";
            this.SongMaintenance_RemoveEmptyDirs_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_RemoveEmptyDirs_Button.TabIndex = 1;
            this.SongMaintenance_RemoveEmptyDirs_Button.Text = "移除空資料夾";
            this.SongMaintenance_RemoveEmptyDirs_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_RemoveEmptyDirs_Button.Click += new System.EventHandler(this.SongMaintenance_RemoveEmptyDirs_Button_Click);
            // 
            // SongMaintenance_CompactAccessDB_Button
            // 
            this.SongMaintenance_CompactAccessDB_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_CompactAccessDB_Button.Location = new System.Drawing.Point(338, 39);
            this.SongMaintenance_CompactAccessDB_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_CompactAccessDB_Button.Name = "SongMaintenance_CompactAccessDB_Button";
            this.SongMaintenance_CompactAccessDB_Button.Size = new System.Drawing.Size(169, 32);
            this.SongMaintenance_CompactAccessDB_Button.TabIndex = 2;
            this.SongMaintenance_CompactAccessDB_Button.Text = "壓縮並修復資料庫";
            this.SongMaintenance_CompactAccessDB_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_CompactAccessDB_Button.Click += new System.EventHandler(this.SongMaintenance_CompactAccessDB_Button_Click);
            // 
            // SongMaintenance_SongWordCountCorrect_Button
            // 
            this.SongMaintenance_SongWordCountCorrect_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_SongWordCountCorrect_Button.Location = new System.Drawing.Point(16, 39);
            this.SongMaintenance_SongWordCountCorrect_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_SongWordCountCorrect_Button.Name = "SongMaintenance_SongWordCountCorrect_Button";
            this.SongMaintenance_SongWordCountCorrect_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_SongWordCountCorrect_Button.TabIndex = 0;
            this.SongMaintenance_SongWordCountCorrect_Button.Text = "校正歌曲字數";
            this.SongMaintenance_SongWordCountCorrect_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_SongWordCountCorrect_Button.Click += new System.EventHandler(this.SongMaintenance_SongWordCountCorrect_Button_Click);
            // 
            // SongMaintenance_Favorite_TabPage
            // 
            this.SongMaintenance_Favorite_TabPage.Controls.Add(this.SongMaintenance_FavoriteExport_Button);
            this.SongMaintenance_Favorite_TabPage.Controls.Add(this.SongMaintenance_FavoriteImport_Button);
            this.SongMaintenance_Favorite_TabPage.Controls.Add(this.SongMaintenance_Favorite_TextBox);
            this.SongMaintenance_Favorite_TabPage.Controls.Add(this.SongMaintenance_Favorite_Button);
            this.SongMaintenance_Favorite_TabPage.Controls.Add(this.SongMaintenance_Favorite_ListBox);
            this.SongMaintenance_Favorite_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMaintenance_Favorite_TabPage.Name = "SongMaintenance_Favorite_TabPage";
            this.SongMaintenance_Favorite_TabPage.Padding = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.SongMaintenance_Favorite_TabPage.Size = new System.Drawing.Size(585, 432);
            this.SongMaintenance_Favorite_TabPage.TabIndex = 0;
            this.SongMaintenance_Favorite_TabPage.Text = "我的最愛";
            this.SongMaintenance_Favorite_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMaintenance_FavoriteExport_Button
            // 
            this.SongMaintenance_FavoriteExport_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_FavoriteExport_Button.Location = new System.Drawing.Point(175, 383);
            this.SongMaintenance_FavoriteExport_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_FavoriteExport_Button.Name = "SongMaintenance_FavoriteExport_Button";
            this.SongMaintenance_FavoriteExport_Button.Size = new System.Drawing.Size(147, 32);
            this.SongMaintenance_FavoriteExport_Button.TabIndex = 4;
            this.SongMaintenance_FavoriteExport_Button.Text = "匯出我的最愛";
            this.SongMaintenance_FavoriteExport_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_FavoriteExport_Button.Click += new System.EventHandler(this.SongMaintenance_FavoriteExport_Button_Click);
            // 
            // SongMaintenance_FavoriteImport_Button
            // 
            this.SongMaintenance_FavoriteImport_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_FavoriteImport_Button.Location = new System.Drawing.Point(16, 383);
            this.SongMaintenance_FavoriteImport_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_FavoriteImport_Button.Name = "SongMaintenance_FavoriteImport_Button";
            this.SongMaintenance_FavoriteImport_Button.Size = new System.Drawing.Size(147, 32);
            this.SongMaintenance_FavoriteImport_Button.TabIndex = 3;
            this.SongMaintenance_FavoriteImport_Button.Text = "匯入我的最愛";
            this.SongMaintenance_FavoriteImport_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_FavoriteImport_Button.Click += new System.EventHandler(this.SongMaintenance_FavoriteImport_Button_Click);
            // 
            // SongMaintenance_Favorite_TextBox
            // 
            this.SongMaintenance_Favorite_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Favorite_TextBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SongMaintenance_Favorite_TextBox.Location = new System.Drawing.Point(16, 334);
            this.SongMaintenance_Favorite_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMaintenance_Favorite_TextBox.Name = "SongMaintenance_Favorite_TextBox";
            this.SongMaintenance_Favorite_TextBox.Size = new System.Drawing.Size(224, 30);
            this.SongMaintenance_Favorite_TextBox.TabIndex = 1;
            this.SongMaintenance_Favorite_TextBox.Enter += new System.EventHandler(this.SongMaintenance_Favorite_TextBox_Enter);
            // 
            // SongMaintenance_Favorite_Button
            // 
            this.SongMaintenance_Favorite_Button.AutoSize = true;
            this.SongMaintenance_Favorite_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Favorite_Button.Location = new System.Drawing.Point(252, 333);
            this.SongMaintenance_Favorite_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Favorite_Button.Name = "SongMaintenance_Favorite_Button";
            this.SongMaintenance_Favorite_Button.Size = new System.Drawing.Size(70, 32);
            this.SongMaintenance_Favorite_Button.TabIndex = 2;
            this.SongMaintenance_Favorite_Button.Text = "加入";
            this.SongMaintenance_Favorite_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_Favorite_Button.Click += new System.EventHandler(this.SongMaintenance_Favorite_Button_Click);
            // 
            // SongMaintenance_Favorite_ListBox
            // 
            this.SongMaintenance_Favorite_ListBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Favorite_ListBox.FormattingEnabled = true;
            this.SongMaintenance_Favorite_ListBox.ItemHeight = 22;
            this.SongMaintenance_Favorite_ListBox.Location = new System.Drawing.Point(16, 24);
            this.SongMaintenance_Favorite_ListBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMaintenance_Favorite_ListBox.Name = "SongMaintenance_Favorite_ListBox";
            this.SongMaintenance_Favorite_ListBox.Size = new System.Drawing.Size(306, 290);
            this.SongMaintenance_Favorite_ListBox.TabIndex = 0;
            this.SongMaintenance_Favorite_ListBox.Enter += new System.EventHandler(this.SongMaintenance_Favorite_ListBox_Enter);
            // 
            // SongMaintenance_CustomLang_TabPage
            // 
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang10IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang10IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang10_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang10_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang9IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang9IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang9_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang9_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang8IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang8IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang8_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang8_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang7IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang7IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang7_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang7_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang6IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang6IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang6_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang6_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang5IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang5IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang5_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang5_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang4IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang4IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang4_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang4_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang3IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang3IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang3_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang3_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang2IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang2IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang2_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang2_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang1IDStr_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang1IDStr_Label);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang1_TextBox);
            this.SongMaintenance_CustomLang_TabPage.Controls.Add(this.SongMaintenance_Lang1_Label);
            this.SongMaintenance_CustomLang_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMaintenance_CustomLang_TabPage.Name = "SongMaintenance_CustomLang_TabPage";
            this.SongMaintenance_CustomLang_TabPage.Padding = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.SongMaintenance_CustomLang_TabPage.Size = new System.Drawing.Size(585, 432);
            this.SongMaintenance_CustomLang_TabPage.TabIndex = 1;
            this.SongMaintenance_CustomLang_TabPage.Text = "自訂語系";
            this.SongMaintenance_CustomLang_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMaintenance_Lang10IDStr_TextBox
            // 
            this.SongMaintenance_Lang10IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang10IDStr_TextBox.Location = new System.Drawing.Point(312, 384);
            this.SongMaintenance_Lang10IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang10IDStr_TextBox.Name = "SongMaintenance_Lang10IDStr_TextBox";
            this.SongMaintenance_Lang10IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang10IDStr_TextBox.TabIndex = 39;
            this.SongMaintenance_Lang10IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang10IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang10IDStr_Label
            // 
            this.SongMaintenance_Lang10IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang10IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang10IDStr_Label.Location = new System.Drawing.Point(218, 388);
            this.SongMaintenance_Lang10IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang10IDStr_Label.Name = "SongMaintenance_Lang10IDStr_Label";
            this.SongMaintenance_Lang10IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang10IDStr_Label.TabIndex = 38;
            this.SongMaintenance_Lang10IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang10_TextBox
            // 
            this.SongMaintenance_Lang10_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang10_TextBox.Location = new System.Drawing.Point(86, 384);
            this.SongMaintenance_Lang10_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang10_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang10_TextBox.Name = "SongMaintenance_Lang10_TextBox";
            this.SongMaintenance_Lang10_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang10_TextBox.TabIndex = 37;
            this.SongMaintenance_Lang10_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang10_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang10_Label
            // 
            this.SongMaintenance_Lang10_Label.AutoSize = true;
            this.SongMaintenance_Lang10_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang10_Label.Location = new System.Drawing.Point(16, 388);
            this.SongMaintenance_Lang10_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang10_Label.Name = "SongMaintenance_Lang10_Label";
            this.SongMaintenance_Lang10_Label.Size = new System.Drawing.Size(68, 22);
            this.SongMaintenance_Lang10_Label.TabIndex = 36;
            this.SongMaintenance_Lang10_Label.Text = "語系10:";
            // 
            // SongMaintenance_Lang9IDStr_TextBox
            // 
            this.SongMaintenance_Lang9IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang9IDStr_TextBox.Location = new System.Drawing.Point(312, 344);
            this.SongMaintenance_Lang9IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang9IDStr_TextBox.Name = "SongMaintenance_Lang9IDStr_TextBox";
            this.SongMaintenance_Lang9IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang9IDStr_TextBox.TabIndex = 35;
            this.SongMaintenance_Lang9IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang9IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang9IDStr_Label
            // 
            this.SongMaintenance_Lang9IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang9IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang9IDStr_Label.Location = new System.Drawing.Point(218, 348);
            this.SongMaintenance_Lang9IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang9IDStr_Label.Name = "SongMaintenance_Lang9IDStr_Label";
            this.SongMaintenance_Lang9IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang9IDStr_Label.TabIndex = 34;
            this.SongMaintenance_Lang9IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang9_TextBox
            // 
            this.SongMaintenance_Lang9_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang9_TextBox.Location = new System.Drawing.Point(86, 344);
            this.SongMaintenance_Lang9_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang9_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang9_TextBox.Name = "SongMaintenance_Lang9_TextBox";
            this.SongMaintenance_Lang9_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang9_TextBox.TabIndex = 33;
            this.SongMaintenance_Lang9_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang9_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang9_Label
            // 
            this.SongMaintenance_Lang9_Label.AutoSize = true;
            this.SongMaintenance_Lang9_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang9_Label.Location = new System.Drawing.Point(16, 348);
            this.SongMaintenance_Lang9_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang9_Label.Name = "SongMaintenance_Lang9_Label";
            this.SongMaintenance_Lang9_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang9_Label.TabIndex = 32;
            this.SongMaintenance_Lang9_Label.Text = "語系9:";
            // 
            // SongMaintenance_Lang8IDStr_TextBox
            // 
            this.SongMaintenance_Lang8IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang8IDStr_TextBox.Location = new System.Drawing.Point(312, 304);
            this.SongMaintenance_Lang8IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang8IDStr_TextBox.Name = "SongMaintenance_Lang8IDStr_TextBox";
            this.SongMaintenance_Lang8IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang8IDStr_TextBox.TabIndex = 31;
            this.SongMaintenance_Lang8IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang8IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang8IDStr_Label
            // 
            this.SongMaintenance_Lang8IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang8IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang8IDStr_Label.Location = new System.Drawing.Point(218, 308);
            this.SongMaintenance_Lang8IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang8IDStr_Label.Name = "SongMaintenance_Lang8IDStr_Label";
            this.SongMaintenance_Lang8IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang8IDStr_Label.TabIndex = 30;
            this.SongMaintenance_Lang8IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang8_TextBox
            // 
            this.SongMaintenance_Lang8_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang8_TextBox.Location = new System.Drawing.Point(86, 304);
            this.SongMaintenance_Lang8_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang8_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang8_TextBox.Name = "SongMaintenance_Lang8_TextBox";
            this.SongMaintenance_Lang8_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang8_TextBox.TabIndex = 29;
            this.SongMaintenance_Lang8_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang8_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang8_Label
            // 
            this.SongMaintenance_Lang8_Label.AutoSize = true;
            this.SongMaintenance_Lang8_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang8_Label.Location = new System.Drawing.Point(16, 308);
            this.SongMaintenance_Lang8_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang8_Label.Name = "SongMaintenance_Lang8_Label";
            this.SongMaintenance_Lang8_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang8_Label.TabIndex = 28;
            this.SongMaintenance_Lang8_Label.Text = "語系8:";
            // 
            // SongMaintenance_Lang7IDStr_TextBox
            // 
            this.SongMaintenance_Lang7IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang7IDStr_TextBox.Location = new System.Drawing.Point(312, 264);
            this.SongMaintenance_Lang7IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang7IDStr_TextBox.Name = "SongMaintenance_Lang7IDStr_TextBox";
            this.SongMaintenance_Lang7IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang7IDStr_TextBox.TabIndex = 27;
            this.SongMaintenance_Lang7IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang7IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang7IDStr_Label
            // 
            this.SongMaintenance_Lang7IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang7IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang7IDStr_Label.Location = new System.Drawing.Point(218, 268);
            this.SongMaintenance_Lang7IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang7IDStr_Label.Name = "SongMaintenance_Lang7IDStr_Label";
            this.SongMaintenance_Lang7IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang7IDStr_Label.TabIndex = 26;
            this.SongMaintenance_Lang7IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang7_TextBox
            // 
            this.SongMaintenance_Lang7_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang7_TextBox.Location = new System.Drawing.Point(86, 264);
            this.SongMaintenance_Lang7_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang7_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang7_TextBox.Name = "SongMaintenance_Lang7_TextBox";
            this.SongMaintenance_Lang7_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang7_TextBox.TabIndex = 25;
            this.SongMaintenance_Lang7_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang7_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang7_Label
            // 
            this.SongMaintenance_Lang7_Label.AutoSize = true;
            this.SongMaintenance_Lang7_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang7_Label.Location = new System.Drawing.Point(16, 268);
            this.SongMaintenance_Lang7_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang7_Label.Name = "SongMaintenance_Lang7_Label";
            this.SongMaintenance_Lang7_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang7_Label.TabIndex = 24;
            this.SongMaintenance_Lang7_Label.Text = "語系7:";
            // 
            // SongMaintenance_Lang6IDStr_TextBox
            // 
            this.SongMaintenance_Lang6IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang6IDStr_TextBox.Location = new System.Drawing.Point(312, 224);
            this.SongMaintenance_Lang6IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang6IDStr_TextBox.Name = "SongMaintenance_Lang6IDStr_TextBox";
            this.SongMaintenance_Lang6IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang6IDStr_TextBox.TabIndex = 23;
            this.SongMaintenance_Lang6IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang6IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang6IDStr_Label
            // 
            this.SongMaintenance_Lang6IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang6IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang6IDStr_Label.Location = new System.Drawing.Point(218, 228);
            this.SongMaintenance_Lang6IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang6IDStr_Label.Name = "SongMaintenance_Lang6IDStr_Label";
            this.SongMaintenance_Lang6IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang6IDStr_Label.TabIndex = 22;
            this.SongMaintenance_Lang6IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang6_TextBox
            // 
            this.SongMaintenance_Lang6_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang6_TextBox.Location = new System.Drawing.Point(86, 224);
            this.SongMaintenance_Lang6_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang6_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang6_TextBox.Name = "SongMaintenance_Lang6_TextBox";
            this.SongMaintenance_Lang6_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang6_TextBox.TabIndex = 21;
            this.SongMaintenance_Lang6_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang6_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang6_Label
            // 
            this.SongMaintenance_Lang6_Label.AutoSize = true;
            this.SongMaintenance_Lang6_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang6_Label.Location = new System.Drawing.Point(16, 228);
            this.SongMaintenance_Lang6_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang6_Label.Name = "SongMaintenance_Lang6_Label";
            this.SongMaintenance_Lang6_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang6_Label.TabIndex = 20;
            this.SongMaintenance_Lang6_Label.Text = "語系6:";
            // 
            // SongMaintenance_Lang5IDStr_TextBox
            // 
            this.SongMaintenance_Lang5IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang5IDStr_TextBox.Location = new System.Drawing.Point(312, 184);
            this.SongMaintenance_Lang5IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang5IDStr_TextBox.Name = "SongMaintenance_Lang5IDStr_TextBox";
            this.SongMaintenance_Lang5IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang5IDStr_TextBox.TabIndex = 19;
            this.SongMaintenance_Lang5IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang5IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang5IDStr_Label
            // 
            this.SongMaintenance_Lang5IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang5IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang5IDStr_Label.Location = new System.Drawing.Point(218, 188);
            this.SongMaintenance_Lang5IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang5IDStr_Label.Name = "SongMaintenance_Lang5IDStr_Label";
            this.SongMaintenance_Lang5IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang5IDStr_Label.TabIndex = 18;
            this.SongMaintenance_Lang5IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang5_TextBox
            // 
            this.SongMaintenance_Lang5_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang5_TextBox.Location = new System.Drawing.Point(86, 184);
            this.SongMaintenance_Lang5_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang5_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang5_TextBox.Name = "SongMaintenance_Lang5_TextBox";
            this.SongMaintenance_Lang5_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang5_TextBox.TabIndex = 17;
            this.SongMaintenance_Lang5_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang5_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang5_Label
            // 
            this.SongMaintenance_Lang5_Label.AutoSize = true;
            this.SongMaintenance_Lang5_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang5_Label.Location = new System.Drawing.Point(16, 188);
            this.SongMaintenance_Lang5_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang5_Label.Name = "SongMaintenance_Lang5_Label";
            this.SongMaintenance_Lang5_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang5_Label.TabIndex = 16;
            this.SongMaintenance_Lang5_Label.Text = "語系5:";
            // 
            // SongMaintenance_Lang4IDStr_TextBox
            // 
            this.SongMaintenance_Lang4IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang4IDStr_TextBox.Location = new System.Drawing.Point(312, 144);
            this.SongMaintenance_Lang4IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang4IDStr_TextBox.Name = "SongMaintenance_Lang4IDStr_TextBox";
            this.SongMaintenance_Lang4IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang4IDStr_TextBox.TabIndex = 15;
            this.SongMaintenance_Lang4IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang4IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang4IDStr_Label
            // 
            this.SongMaintenance_Lang4IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang4IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang4IDStr_Label.Location = new System.Drawing.Point(218, 148);
            this.SongMaintenance_Lang4IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang4IDStr_Label.Name = "SongMaintenance_Lang4IDStr_Label";
            this.SongMaintenance_Lang4IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang4IDStr_Label.TabIndex = 14;
            this.SongMaintenance_Lang4IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang4_TextBox
            // 
            this.SongMaintenance_Lang4_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang4_TextBox.Location = new System.Drawing.Point(86, 144);
            this.SongMaintenance_Lang4_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang4_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang4_TextBox.Name = "SongMaintenance_Lang4_TextBox";
            this.SongMaintenance_Lang4_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang4_TextBox.TabIndex = 13;
            this.SongMaintenance_Lang4_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang4_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang4_Label
            // 
            this.SongMaintenance_Lang4_Label.AutoSize = true;
            this.SongMaintenance_Lang4_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang4_Label.Location = new System.Drawing.Point(16, 148);
            this.SongMaintenance_Lang4_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang4_Label.Name = "SongMaintenance_Lang4_Label";
            this.SongMaintenance_Lang4_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang4_Label.TabIndex = 12;
            this.SongMaintenance_Lang4_Label.Text = "語系4:";
            // 
            // SongMaintenance_Lang3IDStr_TextBox
            // 
            this.SongMaintenance_Lang3IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang3IDStr_TextBox.Location = new System.Drawing.Point(312, 104);
            this.SongMaintenance_Lang3IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang3IDStr_TextBox.Name = "SongMaintenance_Lang3IDStr_TextBox";
            this.SongMaintenance_Lang3IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang3IDStr_TextBox.TabIndex = 11;
            this.SongMaintenance_Lang3IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang3IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang3IDStr_Label
            // 
            this.SongMaintenance_Lang3IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang3IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang3IDStr_Label.Location = new System.Drawing.Point(218, 108);
            this.SongMaintenance_Lang3IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang3IDStr_Label.Name = "SongMaintenance_Lang3IDStr_Label";
            this.SongMaintenance_Lang3IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang3IDStr_Label.TabIndex = 10;
            this.SongMaintenance_Lang3IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang3_TextBox
            // 
            this.SongMaintenance_Lang3_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang3_TextBox.Location = new System.Drawing.Point(86, 104);
            this.SongMaintenance_Lang3_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang3_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang3_TextBox.Name = "SongMaintenance_Lang3_TextBox";
            this.SongMaintenance_Lang3_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang3_TextBox.TabIndex = 9;
            this.SongMaintenance_Lang3_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang3_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang3_Label
            // 
            this.SongMaintenance_Lang3_Label.AutoSize = true;
            this.SongMaintenance_Lang3_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang3_Label.Location = new System.Drawing.Point(16, 108);
            this.SongMaintenance_Lang3_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang3_Label.Name = "SongMaintenance_Lang3_Label";
            this.SongMaintenance_Lang3_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang3_Label.TabIndex = 8;
            this.SongMaintenance_Lang3_Label.Text = "語系3:";
            // 
            // SongMaintenance_Lang2IDStr_TextBox
            // 
            this.SongMaintenance_Lang2IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang2IDStr_TextBox.Location = new System.Drawing.Point(312, 64);
            this.SongMaintenance_Lang2IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang2IDStr_TextBox.Name = "SongMaintenance_Lang2IDStr_TextBox";
            this.SongMaintenance_Lang2IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang2IDStr_TextBox.TabIndex = 7;
            this.SongMaintenance_Lang2IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang2IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang2IDStr_Label
            // 
            this.SongMaintenance_Lang2IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang2IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang2IDStr_Label.Location = new System.Drawing.Point(218, 68);
            this.SongMaintenance_Lang2IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang2IDStr_Label.Name = "SongMaintenance_Lang2IDStr_Label";
            this.SongMaintenance_Lang2IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang2IDStr_Label.TabIndex = 6;
            this.SongMaintenance_Lang2IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang2_TextBox
            // 
            this.SongMaintenance_Lang2_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang2_TextBox.Location = new System.Drawing.Point(86, 64);
            this.SongMaintenance_Lang2_TextBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.SongMaintenance_Lang2_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang2_TextBox.Name = "SongMaintenance_Lang2_TextBox";
            this.SongMaintenance_Lang2_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang2_TextBox.TabIndex = 5;
            this.SongMaintenance_Lang2_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang2_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang2_Label
            // 
            this.SongMaintenance_Lang2_Label.AutoSize = true;
            this.SongMaintenance_Lang2_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang2_Label.Location = new System.Drawing.Point(16, 68);
            this.SongMaintenance_Lang2_Label.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_Lang2_Label.Name = "SongMaintenance_Lang2_Label";
            this.SongMaintenance_Lang2_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang2_Label.TabIndex = 4;
            this.SongMaintenance_Lang2_Label.Text = "語系2:";
            // 
            // SongMaintenance_Lang1IDStr_TextBox
            // 
            this.SongMaintenance_Lang1IDStr_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang1IDStr_TextBox.Location = new System.Drawing.Point(312, 24);
            this.SongMaintenance_Lang1IDStr_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 5);
            this.SongMaintenance_Lang1IDStr_TextBox.Name = "SongMaintenance_Lang1IDStr_TextBox";
            this.SongMaintenance_Lang1IDStr_TextBox.Size = new System.Drawing.Size(257, 30);
            this.SongMaintenance_Lang1IDStr_TextBox.TabIndex = 3;
            this.SongMaintenance_Lang1IDStr_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang1IDStr_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang1IDStr_Label
            // 
            this.SongMaintenance_Lang1IDStr_Label.AutoSize = true;
            this.SongMaintenance_Lang1IDStr_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang1IDStr_Label.Location = new System.Drawing.Point(218, 28);
            this.SongMaintenance_Lang1IDStr_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 9);
            this.SongMaintenance_Lang1IDStr_Label.Name = "SongMaintenance_Lang1IDStr_Label";
            this.SongMaintenance_Lang1IDStr_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_Lang1IDStr_Label.TabIndex = 2;
            this.SongMaintenance_Lang1IDStr_Label.Text = "辨識字串:";
            // 
            // SongMaintenance_Lang1_TextBox
            // 
            this.SongMaintenance_Lang1_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang1_TextBox.Location = new System.Drawing.Point(86, 24);
            this.SongMaintenance_Lang1_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 5);
            this.SongMaintenance_Lang1_TextBox.MaxLength = 12;
            this.SongMaintenance_Lang1_TextBox.Name = "SongMaintenance_Lang1_TextBox";
            this.SongMaintenance_Lang1_TextBox.Size = new System.Drawing.Size(120, 30);
            this.SongMaintenance_Lang1_TextBox.TabIndex = 1;
            this.SongMaintenance_Lang1_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_TextBox_KeyPress);
            this.SongMaintenance_Lang1_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.Common_IsNullOrEmpty_TextBox_Validating);
            // 
            // SongMaintenance_Lang1_Label
            // 
            this.SongMaintenance_Lang1_Label.AutoSize = true;
            this.SongMaintenance_Lang1_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Lang1_Label.Location = new System.Drawing.Point(16, 28);
            this.SongMaintenance_Lang1_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 9);
            this.SongMaintenance_Lang1_Label.Name = "SongMaintenance_Lang1_Label";
            this.SongMaintenance_Lang1_Label.Size = new System.Drawing.Size(58, 22);
            this.SongMaintenance_Lang1_Label.TabIndex = 0;
            this.SongMaintenance_Lang1_Label.Text = "語系1:";
            // 
            // SongMaintenance_MultiSongPath_TabPage
            // 
            this.SongMaintenance_MultiSongPath_TabPage.Controls.Add(this.SongMaintenance_MultiSongPath_TextBox);
            this.SongMaintenance_MultiSongPath_TabPage.Controls.Add(this.SongMaintenance_MultiSongPath_Button);
            this.SongMaintenance_MultiSongPath_TabPage.Controls.Add(this.SongMaintenance_MultiSongPath_ListBox);
            this.SongMaintenance_MultiSongPath_TabPage.Controls.Add(this.SongMaintenance_EnableMultiSongPath_CheckBox);
            this.SongMaintenance_MultiSongPath_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMaintenance_MultiSongPath_TabPage.Name = "SongMaintenance_MultiSongPath_TabPage";
            this.SongMaintenance_MultiSongPath_TabPage.Padding = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.SongMaintenance_MultiSongPath_TabPage.Size = new System.Drawing.Size(585, 432);
            this.SongMaintenance_MultiSongPath_TabPage.TabIndex = 4;
            this.SongMaintenance_MultiSongPath_TabPage.Text = "多重歌庫";
            this.SongMaintenance_MultiSongPath_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMaintenance_MultiSongPath_TextBox
            // 
            this.SongMaintenance_MultiSongPath_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_MultiSongPath_TextBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SongMaintenance_MultiSongPath_TextBox.Location = new System.Drawing.Point(16, 380);
            this.SongMaintenance_MultiSongPath_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMaintenance_MultiSongPath_TextBox.Name = "SongMaintenance_MultiSongPath_TextBox";
            this.SongMaintenance_MultiSongPath_TextBox.ReadOnly = true;
            this.SongMaintenance_MultiSongPath_TextBox.Size = new System.Drawing.Size(471, 30);
            this.SongMaintenance_MultiSongPath_TextBox.TabIndex = 2;
            this.SongMaintenance_MultiSongPath_TextBox.Enter += new System.EventHandler(this.SongMaintenance_MultiSongPath_TextBox_Enter);
            // 
            // SongMaintenance_MultiSongPath_Button
            // 
            this.SongMaintenance_MultiSongPath_Button.AutoSize = true;
            this.SongMaintenance_MultiSongPath_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_MultiSongPath_Button.Location = new System.Drawing.Point(499, 379);
            this.SongMaintenance_MultiSongPath_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_MultiSongPath_Button.Name = "SongMaintenance_MultiSongPath_Button";
            this.SongMaintenance_MultiSongPath_Button.Size = new System.Drawing.Size(70, 32);
            this.SongMaintenance_MultiSongPath_Button.TabIndex = 3;
            this.SongMaintenance_MultiSongPath_Button.Text = "瀏覽";
            this.SongMaintenance_MultiSongPath_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_MultiSongPath_Button.Click += new System.EventHandler(this.SongMaintenance_MultiSongPath_Button_Click);
            // 
            // SongMaintenance_MultiSongPath_ListBox
            // 
            this.SongMaintenance_MultiSongPath_ListBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_MultiSongPath_ListBox.FormattingEnabled = true;
            this.SongMaintenance_MultiSongPath_ListBox.ItemHeight = 22;
            this.SongMaintenance_MultiSongPath_ListBox.Location = new System.Drawing.Point(16, 70);
            this.SongMaintenance_MultiSongPath_ListBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongMaintenance_MultiSongPath_ListBox.Name = "SongMaintenance_MultiSongPath_ListBox";
            this.SongMaintenance_MultiSongPath_ListBox.Size = new System.Drawing.Size(553, 290);
            this.SongMaintenance_MultiSongPath_ListBox.TabIndex = 1;
            this.SongMaintenance_MultiSongPath_ListBox.Enter += new System.EventHandler(this.SongMaintenance_MultiSongPath_ListBox_Enter);
            // 
            // SongMaintenance_EnableMultiSongPath_CheckBox
            // 
            this.SongMaintenance_EnableMultiSongPath_CheckBox.AutoSize = true;
            this.SongMaintenance_EnableMultiSongPath_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_EnableMultiSongPath_CheckBox.Location = new System.Drawing.Point(16, 26);
            this.SongMaintenance_EnableMultiSongPath_CheckBox.Margin = new System.Windows.Forms.Padding(6, 12, 6, 8);
            this.SongMaintenance_EnableMultiSongPath_CheckBox.Name = "SongMaintenance_EnableMultiSongPath_CheckBox";
            this.SongMaintenance_EnableMultiSongPath_CheckBox.Size = new System.Drawing.Size(219, 26);
            this.SongMaintenance_EnableMultiSongPath_CheckBox.TabIndex = 0;
            this.SongMaintenance_EnableMultiSongPath_CheckBox.Text = "啟用多重歌庫資料夾支援";
            this.SongMaintenance_EnableMultiSongPath_CheckBox.UseVisualStyleBackColor = true;
            this.SongMaintenance_EnableMultiSongPath_CheckBox.CheckedChanged += new System.EventHandler(this.SongMaintenance_EnableMultiSongPath_CheckBox_CheckedChanged);
            // 
            // SongMaintenance_DBVer_TabPage
            // 
            this.SongMaintenance_DBVer_TabPage.Controls.Add(this.SongMaintenance_DBVerUpdate_GroupBox);
            this.SongMaintenance_DBVer_TabPage.Controls.Add(this.SongMaintenance_DBVerTooltip_GroupBox);
            this.SongMaintenance_DBVer_TabPage.Controls.Add(this.SongMaintenance_DBVer3Value_Label);
            this.SongMaintenance_DBVer_TabPage.Controls.Add(this.SongMaintenance_DBVer2Value_Label);
            this.SongMaintenance_DBVer_TabPage.Controls.Add(this.SongMaintenance_DBVer1Value_Label);
            this.SongMaintenance_DBVer_TabPage.Controls.Add(this.SongMaintenance_DBVer3_Label);
            this.SongMaintenance_DBVer_TabPage.Controls.Add(this.SongMaintenance_DBVer2_Label);
            this.SongMaintenance_DBVer_TabPage.Controls.Add(this.SongMaintenance_DBVer1_Label);
            this.SongMaintenance_DBVer_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongMaintenance_DBVer_TabPage.Name = "SongMaintenance_DBVer_TabPage";
            this.SongMaintenance_DBVer_TabPage.Padding = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.SongMaintenance_DBVer_TabPage.Size = new System.Drawing.Size(585, 432);
            this.SongMaintenance_DBVer_TabPage.TabIndex = 3;
            this.SongMaintenance_DBVer_TabPage.Text = "歌庫版本";
            this.SongMaintenance_DBVer_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongMaintenance_DBVerUpdate_GroupBox
            // 
            this.SongMaintenance_DBVerUpdate_GroupBox.Controls.Add(this.SongMaintenance_EnableRebuildSingerData_CheckBox);
            this.SongMaintenance_DBVerUpdate_GroupBox.Controls.Add(this.SongMaintenance_EnableDBVerUpdate_CheckBox);
            this.SongMaintenance_DBVerUpdate_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVerUpdate_GroupBox.Location = new System.Drawing.Point(13, 235);
            this.SongMaintenance_DBVerUpdate_GroupBox.Name = "SongMaintenance_DBVerUpdate_GroupBox";
            this.SongMaintenance_DBVerUpdate_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_DBVerUpdate_GroupBox.Size = new System.Drawing.Size(556, 130);
            this.SongMaintenance_DBVerUpdate_GroupBox.TabIndex = 6;
            this.SongMaintenance_DBVerUpdate_GroupBox.TabStop = false;
            this.SongMaintenance_DBVerUpdate_GroupBox.Text = "版本更新";
            // 
            // SongMaintenance_EnableRebuildSingerData_CheckBox
            // 
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.AutoSize = true;
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.Location = new System.Drawing.Point(44, 82);
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.Margin = new System.Windows.Forms.Padding(34, 6, 6, 6);
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.Name = "SongMaintenance_EnableRebuildSingerData_CheckBox";
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.Size = new System.Drawing.Size(236, 26);
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.TabIndex = 1;
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.Text = "更新後重建歌庫歌手資料表";
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.UseVisualStyleBackColor = true;
            this.SongMaintenance_EnableRebuildSingerData_CheckBox.CheckedChanged += new System.EventHandler(this.SongMaintenance_EnableRebuildSingerData_CheckBox_CheckedChanged);
            // 
            // SongMaintenance_EnableDBVerUpdate_CheckBox
            // 
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.AutoSize = true;
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.Location = new System.Drawing.Point(20, 42);
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.Margin = new System.Windows.Forms.Padding(10, 12, 6, 8);
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.Name = "SongMaintenance_EnableDBVerUpdate_CheckBox";
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.Size = new System.Drawing.Size(202, 26);
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.TabIndex = 0;
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.Text = "啟用歌庫版本自動更新";
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.UseVisualStyleBackColor = true;
            this.SongMaintenance_EnableDBVerUpdate_CheckBox.CheckedChanged += new System.EventHandler(this.SongMaintenance_EnableDBVerUpdate_CheckBox_CheckedChanged);
            // 
            // SongMaintenance_DBVerTooltip_GroupBox
            // 
            this.SongMaintenance_DBVerTooltip_GroupBox.Controls.Add(this.SongMaintenance_DBVerTooltip_Label);
            this.SongMaintenance_DBVerTooltip_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 1.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVerTooltip_GroupBox.Location = new System.Drawing.Point(13, 380);
            this.SongMaintenance_DBVerTooltip_GroupBox.Margin = new System.Windows.Forms.Padding(3, 12, 6, 3);
            this.SongMaintenance_DBVerTooltip_GroupBox.Name = "SongMaintenance_DBVerTooltip_GroupBox";
            this.SongMaintenance_DBVerTooltip_GroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.SongMaintenance_DBVerTooltip_GroupBox.Size = new System.Drawing.Size(556, 35);
            this.SongMaintenance_DBVerTooltip_GroupBox.TabIndex = 32;
            this.SongMaintenance_DBVerTooltip_GroupBox.TabStop = false;
            // 
            // SongMaintenance_DBVerTooltip_Label
            // 
            this.SongMaintenance_DBVerTooltip_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVerTooltip_Label.ForeColor = System.Drawing.Color.Red;
            this.SongMaintenance_DBVerTooltip_Label.Location = new System.Drawing.Point(0, 0);
            this.SongMaintenance_DBVerTooltip_Label.Margin = new System.Windows.Forms.Padding(0);
            this.SongMaintenance_DBVerTooltip_Label.Name = "SongMaintenance_DBVerTooltip_Label";
            this.SongMaintenance_DBVerTooltip_Label.Size = new System.Drawing.Size(556, 35);
            this.SongMaintenance_DBVerTooltip_Label.TabIndex = 0;
            this.SongMaintenance_DBVerTooltip_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SongMaintenance_DBVerTooltip_Label.UseMnemonic = false;
            // 
            // SongMaintenance_DBVer3Value_Label
            // 
            this.SongMaintenance_DBVer3Value_Label.AutoSize = true;
            this.SongMaintenance_DBVer3Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVer3Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongMaintenance_DBVer3Value_Label.Location = new System.Drawing.Point(127, 128);
            this.SongMaintenance_DBVer3Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMaintenance_DBVer3Value_Label.Name = "SongMaintenance_DBVer3Value_Label";
            this.SongMaintenance_DBVer3Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongMaintenance_DBVer3Value_Label.TabIndex = 5;
            this.SongMaintenance_DBVer3Value_Label.Text = "0 版";
            // 
            // SongMaintenance_DBVer2Value_Label
            // 
            this.SongMaintenance_DBVer2Value_Label.AutoSize = true;
            this.SongMaintenance_DBVer2Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVer2Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongMaintenance_DBVer2Value_Label.Location = new System.Drawing.Point(127, 78);
            this.SongMaintenance_DBVer2Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMaintenance_DBVer2Value_Label.Name = "SongMaintenance_DBVer2Value_Label";
            this.SongMaintenance_DBVer2Value_Label.Size = new System.Drawing.Size(41, 22);
            this.SongMaintenance_DBVer2Value_Label.TabIndex = 3;
            this.SongMaintenance_DBVer2Value_Label.Text = "0 版";
            // 
            // SongMaintenance_DBVer1Value_Label
            // 
            this.SongMaintenance_DBVer1Value_Label.AutoSize = true;
            this.SongMaintenance_DBVer1Value_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVer1Value_Label.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.SongMaintenance_DBVer1Value_Label.Location = new System.Drawing.Point(127, 28);
            this.SongMaintenance_DBVer1Value_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMaintenance_DBVer1Value_Label.Name = "SongMaintenance_DBVer1Value_Label";
            this.SongMaintenance_DBVer1Value_Label.Size = new System.Drawing.Size(65, 22);
            this.SongMaintenance_DBVer1Value_Label.TabIndex = 1;
            this.SongMaintenance_DBVer1Value_Label.Text = "0.00 版";
            // 
            // SongMaintenance_DBVer3_Label
            // 
            this.SongMaintenance_DBVer3_Label.AutoSize = true;
            this.SongMaintenance_DBVer3_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVer3_Label.Location = new System.Drawing.Point(16, 128);
            this.SongMaintenance_DBVer3_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMaintenance_DBVer3_Label.Name = "SongMaintenance_DBVer3_Label";
            this.SongMaintenance_DBVer3_Label.Size = new System.Drawing.Size(99, 22);
            this.SongMaintenance_DBVer3_Label.TabIndex = 4;
            this.SongMaintenance_DBVer3_Label.Text = "拼音資料庫:";
            // 
            // SongMaintenance_DBVer2_Label
            // 
            this.SongMaintenance_DBVer2_Label.AutoSize = true;
            this.SongMaintenance_DBVer2_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVer2_Label.Location = new System.Drawing.Point(16, 78);
            this.SongMaintenance_DBVer2_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMaintenance_DBVer2_Label.Name = "SongMaintenance_DBVer2_Label";
            this.SongMaintenance_DBVer2_Label.Size = new System.Drawing.Size(99, 22);
            this.SongMaintenance_DBVer2_Label.TabIndex = 2;
            this.SongMaintenance_DBVer2_Label.Text = "歌手資料庫:";
            // 
            // SongMaintenance_DBVer1_Label
            // 
            this.SongMaintenance_DBVer1_Label.AutoSize = true;
            this.SongMaintenance_DBVer1_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_DBVer1_Label.ForeColor = System.Drawing.Color.SaddleBrown;
            this.SongMaintenance_DBVer1_Label.Location = new System.Drawing.Point(16, 28);
            this.SongMaintenance_DBVer1_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongMaintenance_DBVer1_Label.Name = "SongMaintenance_DBVer1_Label";
            this.SongMaintenance_DBVer1_Label.Size = new System.Drawing.Size(99, 22);
            this.SongMaintenance_DBVer1_Label.TabIndex = 0;
            this.SongMaintenance_DBVer1_Label.Text = "資料庫版本:";
            // 
            // SongMaintenance_VolumeChange_GroupBox
            // 
            this.SongMaintenance_VolumeChange_GroupBox.Controls.Add(this.SongMaintenance_VolumeChange_TextBox);
            this.SongMaintenance_VolumeChange_GroupBox.Controls.Add(this.SongMaintenance_VolumeChange_Label);
            this.SongMaintenance_VolumeChange_GroupBox.Controls.Add(this.SongMaintenance_VolumeChange_Button);
            this.SongMaintenance_VolumeChange_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_VolumeChange_GroupBox.Location = new System.Drawing.Point(23, 227);
            this.SongMaintenance_VolumeChange_GroupBox.Name = "SongMaintenance_VolumeChange_GroupBox";
            this.SongMaintenance_VolumeChange_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_VolumeChange_GroupBox.Size = new System.Drawing.Size(342, 96);
            this.SongMaintenance_VolumeChange_GroupBox.TabIndex = 2;
            this.SongMaintenance_VolumeChange_GroupBox.TabStop = false;
            this.SongMaintenance_VolumeChange_GroupBox.Text = "音量變更";
            // 
            // SongMaintenance_VolumeChange_TextBox
            // 
            this.SongMaintenance_VolumeChange_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_VolumeChange_TextBox.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.SongMaintenance_VolumeChange_TextBox.Location = new System.Drawing.Point(110, 40);
            this.SongMaintenance_VolumeChange_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 9);
            this.SongMaintenance_VolumeChange_TextBox.MaxLength = 3;
            this.SongMaintenance_VolumeChange_TextBox.Name = "SongMaintenance_VolumeChange_TextBox";
            this.SongMaintenance_VolumeChange_TextBox.Size = new System.Drawing.Size(134, 30);
            this.SongMaintenance_VolumeChange_TextBox.TabIndex = 1;
            this.SongMaintenance_VolumeChange_TextBox.Text = "100";
            this.SongMaintenance_VolumeChange_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Common_NumericOnly_TextBox_KeyPress);
            this.SongMaintenance_VolumeChange_TextBox.Validating += new System.ComponentModel.CancelEventHandler(this.SongMaintenance_VolumeChange_TextBox_Validating);
            // 
            // SongMaintenance_VolumeChange_Label
            // 
            this.SongMaintenance_VolumeChange_Label.AutoSize = true;
            this.SongMaintenance_VolumeChange_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_VolumeChange_Label.Location = new System.Drawing.Point(16, 44);
            this.SongMaintenance_VolumeChange_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 13);
            this.SongMaintenance_VolumeChange_Label.Name = "SongMaintenance_VolumeChange_Label";
            this.SongMaintenance_VolumeChange_Label.Size = new System.Drawing.Size(82, 22);
            this.SongMaintenance_VolumeChange_Label.TabIndex = 0;
            this.SongMaintenance_VolumeChange_Label.Text = "歌曲音量:";
            // 
            // SongMaintenance_VolumeChange_Button
            // 
            this.SongMaintenance_VolumeChange_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_VolumeChange_Button.Location = new System.Drawing.Point(256, 39);
            this.SongMaintenance_VolumeChange_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_VolumeChange_Button.Name = "SongMaintenance_VolumeChange_Button";
            this.SongMaintenance_VolumeChange_Button.Size = new System.Drawing.Size(70, 32);
            this.SongMaintenance_VolumeChange_Button.TabIndex = 2;
            this.SongMaintenance_VolumeChange_Button.Text = "變更";
            this.SongMaintenance_VolumeChange_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_VolumeChange_Button.Click += new System.EventHandler(this.SongMaintenance_VolumeChange_Button_Click);
            // 
            // SongMaintenance_TrackExchange_GroupBox
            // 
            this.SongMaintenance_TrackExchange_GroupBox.Controls.Add(this.SongMaintenance_LRTrackExchange_Button);
            this.SongMaintenance_TrackExchange_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_TrackExchange_GroupBox.Location = new System.Drawing.Point(23, 125);
            this.SongMaintenance_TrackExchange_GroupBox.Name = "SongMaintenance_TrackExchange_GroupBox";
            this.SongMaintenance_TrackExchange_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_TrackExchange_GroupBox.Size = new System.Drawing.Size(342, 96);
            this.SongMaintenance_TrackExchange_GroupBox.TabIndex = 1;
            this.SongMaintenance_TrackExchange_GroupBox.TabStop = false;
            this.SongMaintenance_TrackExchange_GroupBox.Text = "聲道互換";
            // 
            // SongMaintenance_LRTrackExchange_Button
            // 
            this.SongMaintenance_LRTrackExchange_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_LRTrackExchange_Button.Location = new System.Drawing.Point(17, 39);
            this.SongMaintenance_LRTrackExchange_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_LRTrackExchange_Button.Name = "SongMaintenance_LRTrackExchange_Button";
            this.SongMaintenance_LRTrackExchange_Button.Size = new System.Drawing.Size(180, 32);
            this.SongMaintenance_LRTrackExchange_Button.TabIndex = 0;
            this.SongMaintenance_LRTrackExchange_Button.Text = "互換左右聲道數值";
            this.SongMaintenance_LRTrackExchange_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_LRTrackExchange_Button.Click += new System.EventHandler(this.SongMaintenance_LRTrackExchange_Button_Click);
            // 
            // SongMaintenance_CodeConv_GroupBox
            // 
            this.SongMaintenance_CodeConv_GroupBox.Controls.Add(this.SongMaintenance_CodeCorrect_Button);
            this.SongMaintenance_CodeConv_GroupBox.Controls.Add(this.SongMaintenance_CodeConvTo6_Button);
            this.SongMaintenance_CodeConv_GroupBox.Controls.Add(this.SongMaintenance_CodeConvTo5_Button);
            this.SongMaintenance_CodeConv_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_CodeConv_GroupBox.Location = new System.Drawing.Point(382, 23);
            this.SongMaintenance_CodeConv_GroupBox.Margin = new System.Windows.Forms.Padding(14, 3, 3, 3);
            this.SongMaintenance_CodeConv_GroupBox.Name = "SongMaintenance_CodeConv_GroupBox";
            this.SongMaintenance_CodeConv_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_CodeConv_GroupBox.Size = new System.Drawing.Size(593, 96);
            this.SongMaintenance_CodeConv_GroupBox.TabIndex = 5;
            this.SongMaintenance_CodeConv_GroupBox.TabStop = false;
            this.SongMaintenance_CodeConv_GroupBox.Text = "編碼位數轉換";
            // 
            // SongMaintenance_CodeCorrect_Button
            // 
            this.SongMaintenance_CodeCorrect_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_CodeCorrect_Button.Location = new System.Drawing.Point(338, 39);
            this.SongMaintenance_CodeCorrect_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_CodeCorrect_Button.Name = "SongMaintenance_CodeCorrect_Button";
            this.SongMaintenance_CodeCorrect_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_CodeCorrect_Button.TabIndex = 2;
            this.SongMaintenance_CodeCorrect_Button.Text = "校正編碼位數";
            this.SongMaintenance_CodeCorrect_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_CodeCorrect_Button.Click += new System.EventHandler(this.SongMaintenance_CodeCorrect_Button_Click);
            // 
            // SongMaintenance_CodeConvTo6_Button
            // 
            this.SongMaintenance_CodeConvTo6_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_CodeConvTo6_Button.Location = new System.Drawing.Point(177, 39);
            this.SongMaintenance_CodeConvTo6_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_CodeConvTo6_Button.Name = "SongMaintenance_CodeConvTo6_Button";
            this.SongMaintenance_CodeConvTo6_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_CodeConvTo6_Button.TabIndex = 1;
            this.SongMaintenance_CodeConvTo6_Button.Text = "轉換為 6 位數";
            this.SongMaintenance_CodeConvTo6_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_CodeConvTo6_Button.Click += new System.EventHandler(this.SongMaintenance_CodeConvTo6_Button_Click);
            // 
            // SongMaintenance_CodeConvTo5_Button
            // 
            this.SongMaintenance_CodeConvTo5_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_CodeConvTo5_Button.Location = new System.Drawing.Point(16, 39);
            this.SongMaintenance_CodeConvTo5_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_CodeConvTo5_Button.Name = "SongMaintenance_CodeConvTo5_Button";
            this.SongMaintenance_CodeConvTo5_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_CodeConvTo5_Button.TabIndex = 0;
            this.SongMaintenance_CodeConvTo5_Button.Text = "轉換為 5 位數";
            this.SongMaintenance_CodeConvTo5_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_CodeConvTo5_Button.Click += new System.EventHandler(this.SongMaintenance_CodeConvTo5_Button_Click);
            // 
            // SongMaintenance_Tooltip_GroupBox
            // 
            this.SongMaintenance_Tooltip_GroupBox.Controls.Add(this.SongMaintenance_Tooltip_Label);
            this.SongMaintenance_Tooltip_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 1.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Tooltip_GroupBox.Location = new System.Drawing.Point(23, 621);
            this.SongMaintenance_Tooltip_GroupBox.Margin = new System.Windows.Forms.Padding(3, 12, 6, 3);
            this.SongMaintenance_Tooltip_GroupBox.Name = "SongMaintenance_Tooltip_GroupBox";
            this.SongMaintenance_Tooltip_GroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.SongMaintenance_Tooltip_GroupBox.Size = new System.Drawing.Size(830, 35);
            this.SongMaintenance_Tooltip_GroupBox.TabIndex = 7;
            this.SongMaintenance_Tooltip_GroupBox.TabStop = false;
            // 
            // SongMaintenance_Tooltip_Label
            // 
            this.SongMaintenance_Tooltip_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_Tooltip_Label.ForeColor = System.Drawing.Color.Red;
            this.SongMaintenance_Tooltip_Label.Location = new System.Drawing.Point(0, 0);
            this.SongMaintenance_Tooltip_Label.Margin = new System.Windows.Forms.Padding(0);
            this.SongMaintenance_Tooltip_Label.Name = "SongMaintenance_Tooltip_Label";
            this.SongMaintenance_Tooltip_Label.Size = new System.Drawing.Size(830, 35);
            this.SongMaintenance_Tooltip_Label.TabIndex = 0;
            this.SongMaintenance_Tooltip_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SongMaintenance_Tooltip_Label.UseMnemonic = false;
            // 
            // SongMaintenance_SpellCorrect_GroupBox
            // 
            this.SongMaintenance_SpellCorrect_GroupBox.Controls.Add(this.SongMaintenance_SongSpellCorrect_Button);
            this.SongMaintenance_SpellCorrect_GroupBox.Controls.Add(this.SongMaintenance_SingerSpellCorrect_Button);
            this.SongMaintenance_SpellCorrect_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_SpellCorrect_GroupBox.Location = new System.Drawing.Point(23, 23);
            this.SongMaintenance_SpellCorrect_GroupBox.Name = "SongMaintenance_SpellCorrect_GroupBox";
            this.SongMaintenance_SpellCorrect_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongMaintenance_SpellCorrect_GroupBox.Size = new System.Drawing.Size(342, 96);
            this.SongMaintenance_SpellCorrect_GroupBox.TabIndex = 0;
            this.SongMaintenance_SpellCorrect_GroupBox.TabStop = false;
            this.SongMaintenance_SpellCorrect_GroupBox.Text = "拼音校正";
            // 
            // SongMaintenance_SongSpellCorrect_Button
            // 
            this.SongMaintenance_SongSpellCorrect_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_SongSpellCorrect_Button.Location = new System.Drawing.Point(177, 39);
            this.SongMaintenance_SongSpellCorrect_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_SongSpellCorrect_Button.Name = "SongMaintenance_SongSpellCorrect_Button";
            this.SongMaintenance_SongSpellCorrect_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_SongSpellCorrect_Button.TabIndex = 1;
            this.SongMaintenance_SongSpellCorrect_Button.Text = "校正歌曲拼音";
            this.SongMaintenance_SongSpellCorrect_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_SongSpellCorrect_Button.Click += new System.EventHandler(this.SongMaintenance_SongSpellCorrect_Button_Click);
            // 
            // SongMaintenance_SingerSpellCorrect_Button
            // 
            this.SongMaintenance_SingerSpellCorrect_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongMaintenance_SingerSpellCorrect_Button.Location = new System.Drawing.Point(16, 39);
            this.SongMaintenance_SingerSpellCorrect_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongMaintenance_SingerSpellCorrect_Button.Name = "SongMaintenance_SingerSpellCorrect_Button";
            this.SongMaintenance_SingerSpellCorrect_Button.Size = new System.Drawing.Size(149, 32);
            this.SongMaintenance_SingerSpellCorrect_Button.TabIndex = 0;
            this.SongMaintenance_SingerSpellCorrect_Button.Text = "校正歌手拼音";
            this.SongMaintenance_SingerSpellCorrect_Button.UseVisualStyleBackColor = true;
            this.SongMaintenance_SingerSpellCorrect_Button.Click += new System.EventHandler(this.SongMaintenance_SingerSpellCorrect_Button_Click);
            // 
            // MainCfg_TabPage
            // 
            this.MainCfg_TabPage.Controls.Add(this.MainCfg_Save_Button);
            this.MainCfg_TabPage.Controls.Add(this.MainCfg_Tooltip_GroupBox);
            this.MainCfg_TabPage.Controls.Add(this.MainCfg_General_ＧroupBox);
            this.MainCfg_TabPage.Location = new System.Drawing.Point(4, 34);
            this.MainCfg_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.MainCfg_TabPage.Name = "MainCfg_TabPage";
            this.MainCfg_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.MainCfg_TabPage.Size = new System.Drawing.Size(998, 684);
            this.MainCfg_TabPage.TabIndex = 9;
            this.MainCfg_TabPage.Text = "程式設定";
            this.MainCfg_TabPage.UseVisualStyleBackColor = true;
            // 
            // MainCfg_Save_Button
            // 
            this.MainCfg_Save_Button.AutoSize = true;
            this.MainCfg_Save_Button.Location = new System.Drawing.Point(865, 621);
            this.MainCfg_Save_Button.Margin = new System.Windows.Forms.Padding(6);
            this.MainCfg_Save_Button.Name = "MainCfg_Save_Button";
            this.MainCfg_Save_Button.Size = new System.Drawing.Size(110, 35);
            this.MainCfg_Save_Button.TabIndex = 2;
            this.MainCfg_Save_Button.Text = "儲存設定";
            this.MainCfg_Save_Button.UseVisualStyleBackColor = true;
            this.MainCfg_Save_Button.Click += new System.EventHandler(this.MainCfg_Save_Button_Click);
            // 
            // MainCfg_Tooltip_GroupBox
            // 
            this.MainCfg_Tooltip_GroupBox.Controls.Add(this.MainCfg_Tooltip_Label);
            this.MainCfg_Tooltip_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 1.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_Tooltip_GroupBox.Location = new System.Drawing.Point(23, 621);
            this.MainCfg_Tooltip_GroupBox.Margin = new System.Windows.Forms.Padding(3, 12, 6, 3);
            this.MainCfg_Tooltip_GroupBox.Name = "MainCfg_Tooltip_GroupBox";
            this.MainCfg_Tooltip_GroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.MainCfg_Tooltip_GroupBox.Size = new System.Drawing.Size(830, 35);
            this.MainCfg_Tooltip_GroupBox.TabIndex = 1;
            this.MainCfg_Tooltip_GroupBox.TabStop = false;
            // 
            // MainCfg_Tooltip_Label
            // 
            this.MainCfg_Tooltip_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_Tooltip_Label.ForeColor = System.Drawing.Color.Red;
            this.MainCfg_Tooltip_Label.Location = new System.Drawing.Point(0, 0);
            this.MainCfg_Tooltip_Label.Margin = new System.Windows.Forms.Padding(0);
            this.MainCfg_Tooltip_Label.Name = "MainCfg_Tooltip_Label";
            this.MainCfg_Tooltip_Label.Size = new System.Drawing.Size(830, 35);
            this.MainCfg_Tooltip_Label.TabIndex = 0;
            this.MainCfg_Tooltip_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MainCfg_Tooltip_Label.UseMnemonic = false;
            // 
            // MainCfg_General_ＧroupBox
            // 
            this.MainCfg_General_ＧroupBox.Controls.Add(this.MainCfg_EnableAutoUpdate_CheckBox);
            this.MainCfg_General_ＧroupBox.Controls.Add(this.MainCfg_BackupRemoveSongDays_ComboBox);
            this.MainCfg_General_ＧroupBox.Controls.Add(this.MainCfg_BackupRemoveSongDays_Label);
            this.MainCfg_General_ＧroupBox.Controls.Add(this.MainCfg_HideSongLogTab_CheckBox);
            this.MainCfg_General_ＧroupBox.Controls.Add(this.MainCfg_HideSongAddResultTab_CheckBox);
            this.MainCfg_General_ＧroupBox.Controls.Add(this.MainCfg_HideSongDBConvTab_CheckBox);
            this.MainCfg_General_ＧroupBox.Controls.Add(this.MainCfg_HideTab_Label);
            this.MainCfg_General_ＧroupBox.Controls.Add(this.MainCfg_AlwaysOnTop_CheckBox);
            this.MainCfg_General_ＧroupBox.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_General_ＧroupBox.Location = new System.Drawing.Point(23, 23);
            this.MainCfg_General_ＧroupBox.Name = "MainCfg_General_ＧroupBox";
            this.MainCfg_General_ＧroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.MainCfg_General_ＧroupBox.Size = new System.Drawing.Size(952, 583);
            this.MainCfg_General_ＧroupBox.TabIndex = 0;
            this.MainCfg_General_ＧroupBox.TabStop = false;
            this.MainCfg_General_ＧroupBox.Text = "程式設定";
            // 
            // MainCfg_EnableAutoUpdate_CheckBox
            // 
            this.MainCfg_EnableAutoUpdate_CheckBox.AutoSize = true;
            this.MainCfg_EnableAutoUpdate_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_EnableAutoUpdate_CheckBox.Location = new System.Drawing.Point(200, 42);
            this.MainCfg_EnableAutoUpdate_CheckBox.Margin = new System.Windows.Forms.Padding(6, 12, 6, 8);
            this.MainCfg_EnableAutoUpdate_CheckBox.Name = "MainCfg_EnableAutoUpdate_CheckBox";
            this.MainCfg_EnableAutoUpdate_CheckBox.Size = new System.Drawing.Size(168, 26);
            this.MainCfg_EnableAutoUpdate_CheckBox.TabIndex = 7;
            this.MainCfg_EnableAutoUpdate_CheckBox.Text = "啟用程式自動更新";
            this.MainCfg_EnableAutoUpdate_CheckBox.UseVisualStyleBackColor = true;
            this.MainCfg_EnableAutoUpdate_CheckBox.CheckedChanged += new System.EventHandler(this.MainCfg_EnableAutoUpdate_CheckBox_CheckedChanged);
            // 
            // MainCfg_BackupRemoveSongDays_ComboBox
            // 
            this.MainCfg_BackupRemoveSongDays_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MainCfg_BackupRemoveSongDays_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_BackupRemoveSongDays_ComboBox.FormattingEnabled = true;
            this.MainCfg_BackupRemoveSongDays_ComboBox.Location = new System.Drawing.Point(212, 118);
            this.MainCfg_BackupRemoveSongDays_ComboBox.Margin = new System.Windows.Forms.Padding(6, 4, 6, 4);
            this.MainCfg_BackupRemoveSongDays_ComboBox.Name = "MainCfg_BackupRemoveSongDays_ComboBox";
            this.MainCfg_BackupRemoveSongDays_ComboBox.Size = new System.Drawing.Size(74, 30);
            this.MainCfg_BackupRemoveSongDays_ComboBox.TabIndex = 6;
            this.MainCfg_BackupRemoveSongDays_ComboBox.SelectedIndexChanged += new System.EventHandler(this.MainCfg_BackupRemoveSongDays_ComboBox_SelectedIndexChanged);
            // 
            // MainCfg_BackupRemoveSongDays_Label
            // 
            this.MainCfg_BackupRemoveSongDays_Label.AutoSize = true;
            this.MainCfg_BackupRemoveSongDays_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_BackupRemoveSongDays_Label.Location = new System.Drawing.Point(16, 122);
            this.MainCfg_BackupRemoveSongDays_Label.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.MainCfg_BackupRemoveSongDays_Label.Name = "MainCfg_BackupRemoveSongDays_Label";
            this.MainCfg_BackupRemoveSongDays_Label.Size = new System.Drawing.Size(184, 22);
            this.MainCfg_BackupRemoveSongDays_Label.TabIndex = 5;
            this.MainCfg_BackupRemoveSongDays_Label.Text = "備份移除歌曲保留天數:";
            // 
            // MainCfg_HideSongLogTab_CheckBox
            // 
            this.MainCfg_HideSongLogTab_CheckBox.AutoSize = true;
            this.MainCfg_HideSongLogTab_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_HideSongLogTab_CheckBox.Location = new System.Drawing.Point(368, 82);
            this.MainCfg_HideSongLogTab_CheckBox.Margin = new System.Windows.Forms.Padding(6);
            this.MainCfg_HideSongLogTab_CheckBox.Name = "MainCfg_HideSongLogTab_CheckBox";
            this.MainCfg_HideSongLogTab_CheckBox.Size = new System.Drawing.Size(100, 26);
            this.MainCfg_HideSongLogTab_CheckBox.TabIndex = 4;
            this.MainCfg_HideSongLogTab_CheckBox.Text = "操作記錄";
            this.MainCfg_HideSongLogTab_CheckBox.UseVisualStyleBackColor = true;
            this.MainCfg_HideSongLogTab_CheckBox.CheckedChanged += new System.EventHandler(this.MainCfg_HideTab_CheckBox_CheckedChanged);
            // 
            // MainCfg_HideSongAddResultTab_CheckBox
            // 
            this.MainCfg_HideSongAddResultTab_CheckBox.AutoSize = true;
            this.MainCfg_HideSongAddResultTab_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_HideSongAddResultTab_CheckBox.Location = new System.Drawing.Point(256, 82);
            this.MainCfg_HideSongAddResultTab_CheckBox.Margin = new System.Windows.Forms.Padding(6);
            this.MainCfg_HideSongAddResultTab_CheckBox.Name = "MainCfg_HideSongAddResultTab_CheckBox";
            this.MainCfg_HideSongAddResultTab_CheckBox.Size = new System.Drawing.Size(100, 26);
            this.MainCfg_HideSongAddResultTab_CheckBox.TabIndex = 3;
            this.MainCfg_HideSongAddResultTab_CheckBox.Text = "加歌結果";
            this.MainCfg_HideSongAddResultTab_CheckBox.UseVisualStyleBackColor = true;
            this.MainCfg_HideSongAddResultTab_CheckBox.CheckedChanged += new System.EventHandler(this.MainCfg_HideTab_CheckBox_CheckedChanged);
            // 
            // MainCfg_HideSongDBConvTab_CheckBox
            // 
            this.MainCfg_HideSongDBConvTab_CheckBox.AutoSize = true;
            this.MainCfg_HideSongDBConvTab_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_HideSongDBConvTab_CheckBox.Location = new System.Drawing.Point(144, 82);
            this.MainCfg_HideSongDBConvTab_CheckBox.Margin = new System.Windows.Forms.Padding(6);
            this.MainCfg_HideSongDBConvTab_CheckBox.Name = "MainCfg_HideSongDBConvTab_CheckBox";
            this.MainCfg_HideSongDBConvTab_CheckBox.Size = new System.Drawing.Size(100, 26);
            this.MainCfg_HideSongDBConvTab_CheckBox.TabIndex = 2;
            this.MainCfg_HideSongDBConvTab_CheckBox.Text = "歌庫轉換";
            this.MainCfg_HideSongDBConvTab_CheckBox.UseVisualStyleBackColor = true;
            this.MainCfg_HideSongDBConvTab_CheckBox.CheckedChanged += new System.EventHandler(this.MainCfg_HideTab_CheckBox_CheckedChanged);
            // 
            // MainCfg_HideTab_Label
            // 
            this.MainCfg_HideTab_Label.AutoSize = true;
            this.MainCfg_HideTab_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_HideTab_Label.Location = new System.Drawing.Point(16, 84);
            this.MainCfg_HideTab_Label.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.MainCfg_HideTab_Label.Name = "MainCfg_HideTab_Label";
            this.MainCfg_HideTab_Label.Size = new System.Drawing.Size(116, 22);
            this.MainCfg_HideTab_Label.TabIndex = 1;
            this.MainCfg_HideTab_Label.Text = "隱藏功能頁面:";
            // 
            // MainCfg_AlwaysOnTop_CheckBox
            // 
            this.MainCfg_AlwaysOnTop_CheckBox.AutoSize = true;
            this.MainCfg_AlwaysOnTop_CheckBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MainCfg_AlwaysOnTop_CheckBox.Location = new System.Drawing.Point(20, 42);
            this.MainCfg_AlwaysOnTop_CheckBox.Margin = new System.Windows.Forms.Padding(10, 12, 6, 8);
            this.MainCfg_AlwaysOnTop_CheckBox.Name = "MainCfg_AlwaysOnTop_CheckBox";
            this.MainCfg_AlwaysOnTop_CheckBox.Size = new System.Drawing.Size(168, 26);
            this.MainCfg_AlwaysOnTop_CheckBox.TabIndex = 0;
            this.MainCfg_AlwaysOnTop_CheckBox.Text = "程式視窗置頂顯示";
            this.MainCfg_AlwaysOnTop_CheckBox.UseVisualStyleBackColor = true;
            this.MainCfg_AlwaysOnTop_CheckBox.CheckedChanged += new System.EventHandler(this.MainCfg_AlwaysOnTop_CheckBox_CheckedChanged);
            // 
            // SongDBConverter_TabPage
            // 
            this.SongDBConverter_TabPage.Controls.Add(this.SongDBConverter_ConvHelp_GroupBox);
            this.SongDBConverter_TabPage.Controls.Add(this.SongDBConverter_StartConv_Button);
            this.SongDBConverter_TabPage.Controls.Add(this.SongDBConverter_Tooltip_GroupBox);
            this.SongDBConverter_TabPage.Controls.Add(this.SongDBConverter_Converter_GroupBox);
            this.SongDBConverter_TabPage.Controls.Add(this.SongDBConverter_JetktvLangCfg_GroupBox);
            this.SongDBConverter_TabPage.Controls.Add(this.SongDBConverter_JetktvPathCfg_GroupBox);
            this.SongDBConverter_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongDBConverter_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongDBConverter_TabPage.Name = "SongDBConverter_TabPage";
            this.SongDBConverter_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.SongDBConverter_TabPage.Size = new System.Drawing.Size(998, 684);
            this.SongDBConverter_TabPage.TabIndex = 5;
            this.SongDBConverter_TabPage.Text = "歌庫轉換";
            this.SongDBConverter_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongDBConverter_ConvHelp_GroupBox
            // 
            this.SongDBConverter_ConvHelp_GroupBox.Controls.Add(this.SongDBConverter_ConvHelp_RichTextBox);
            this.SongDBConverter_ConvHelp_GroupBox.Location = new System.Drawing.Point(23, 380);
            this.SongDBConverter_ConvHelp_GroupBox.MinimumSize = new System.Drawing.Size(952, 276);
            this.SongDBConverter_ConvHelp_GroupBox.Name = "SongDBConverter_ConvHelp_GroupBox";
            this.SongDBConverter_ConvHelp_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongDBConverter_ConvHelp_GroupBox.Size = new System.Drawing.Size(952, 276);
            this.SongDBConverter_ConvHelp_GroupBox.TabIndex = 4;
            this.SongDBConverter_ConvHelp_GroupBox.TabStop = false;
            this.SongDBConverter_ConvHelp_GroupBox.Text = "轉換說明";
            // 
            // SongDBConverter_ConvHelp_RichTextBox
            // 
            this.SongDBConverter_ConvHelp_RichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.SongDBConverter_ConvHelp_RichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SongDBConverter_ConvHelp_RichTextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_ConvHelp_RichTextBox.Location = new System.Drawing.Point(16, 38);
            this.SongDBConverter_ConvHelp_RichTextBox.Margin = new System.Windows.Forms.Padding(6, 8, 6, 14);
            this.SongDBConverter_ConvHelp_RichTextBox.MinimumSize = new System.Drawing.Size(920, 221);
            this.SongDBConverter_ConvHelp_RichTextBox.Name = "SongDBConverter_ConvHelp_RichTextBox";
            this.SongDBConverter_ConvHelp_RichTextBox.ReadOnly = true;
            this.SongDBConverter_ConvHelp_RichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.SongDBConverter_ConvHelp_RichTextBox.Size = new System.Drawing.Size(920, 221);
            this.SongDBConverter_ConvHelp_RichTextBox.TabIndex = 0;
            this.SongDBConverter_ConvHelp_RichTextBox.Text = "";
            // 
            // SongDBConverter_StartConv_Button
            // 
            this.SongDBConverter_StartConv_Button.AutoSize = true;
            this.SongDBConverter_StartConv_Button.Enabled = false;
            this.SongDBConverter_StartConv_Button.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_StartConv_Button.Location = new System.Drawing.Point(865, 336);
            this.SongDBConverter_StartConv_Button.Margin = new System.Windows.Forms.Padding(6, 14, 6, 6);
            this.SongDBConverter_StartConv_Button.Name = "SongDBConverter_StartConv_Button";
            this.SongDBConverter_StartConv_Button.Size = new System.Drawing.Size(110, 35);
            this.SongDBConverter_StartConv_Button.TabIndex = 3;
            this.SongDBConverter_StartConv_Button.Text = "開始轉換";
            this.SongDBConverter_StartConv_Button.UseVisualStyleBackColor = true;
            this.SongDBConverter_StartConv_Button.Click += new System.EventHandler(this.SongDBConverter_StartConv_Button_Click);
            // 
            // SongDBConverter_Tooltip_GroupBox
            // 
            this.SongDBConverter_Tooltip_GroupBox.Controls.Add(this.SongDBConverter_Tooltip_Label);
            this.SongDBConverter_Tooltip_GroupBox.Font = new System.Drawing.Font("微軟正黑體", 1.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_Tooltip_GroupBox.Location = new System.Drawing.Point(23, 336);
            this.SongDBConverter_Tooltip_GroupBox.Margin = new System.Windows.Forms.Padding(3, 14, 6, 3);
            this.SongDBConverter_Tooltip_GroupBox.Name = "SongDBConverter_Tooltip_GroupBox";
            this.SongDBConverter_Tooltip_GroupBox.Padding = new System.Windows.Forms.Padding(0);
            this.SongDBConverter_Tooltip_GroupBox.Size = new System.Drawing.Size(830, 35);
            this.SongDBConverter_Tooltip_GroupBox.TabIndex = 1;
            this.SongDBConverter_Tooltip_GroupBox.TabStop = false;
            // 
            // SongDBConverter_Tooltip_Label
            // 
            this.SongDBConverter_Tooltip_Label.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_Tooltip_Label.ForeColor = System.Drawing.Color.Red;
            this.SongDBConverter_Tooltip_Label.Location = new System.Drawing.Point(0, 0);
            this.SongDBConverter_Tooltip_Label.Margin = new System.Windows.Forms.Padding(0);
            this.SongDBConverter_Tooltip_Label.Name = "SongDBConverter_Tooltip_Label";
            this.SongDBConverter_Tooltip_Label.Size = new System.Drawing.Size(830, 35);
            this.SongDBConverter_Tooltip_Label.TabIndex = 0;
            this.SongDBConverter_Tooltip_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SongDBConverter_Tooltip_Label.UseMnemonic = false;
            // 
            // SongDBConverter_Converter_GroupBox
            // 
            this.SongDBConverter_Converter_GroupBox.Controls.Add(this.SongDBConverter_SrcDBType_ComboBox);
            this.SongDBConverter_Converter_GroupBox.Controls.Add(this.SongDBConverter_SrcDBType_Label);
            this.SongDBConverter_Converter_GroupBox.Controls.Add(this.SongDBConverter_DestDBFile_Button);
            this.SongDBConverter_Converter_GroupBox.Controls.Add(this.SongDBConverter_DestDBFile_TextBox);
            this.SongDBConverter_Converter_GroupBox.Controls.Add(this.SongDBConverter_DestDBFile_Label);
            this.SongDBConverter_Converter_GroupBox.Controls.Add(this.SongDBConverter_SrcDBFile_Button);
            this.SongDBConverter_Converter_GroupBox.Controls.Add(this.SongDBConverter_SrcDBFile_TextBox);
            this.SongDBConverter_Converter_GroupBox.Controls.Add(this.SongDBConverter_SrcDBFile_Label);
            this.SongDBConverter_Converter_GroupBox.Location = new System.Drawing.Point(23, 23);
            this.SongDBConverter_Converter_GroupBox.Name = "SongDBConverter_Converter_GroupBox";
            this.SongDBConverter_Converter_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongDBConverter_Converter_GroupBox.Size = new System.Drawing.Size(952, 296);
            this.SongDBConverter_Converter_GroupBox.TabIndex = 0;
            this.SongDBConverter_Converter_GroupBox.TabStop = false;
            this.SongDBConverter_Converter_GroupBox.Text = "歌庫轉換";
            // 
            // SongDBConverter_SrcDBType_ComboBox
            // 
            this.SongDBConverter_SrcDBType_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_SrcDBType_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_SrcDBType_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_SrcDBType_ComboBox.Location = new System.Drawing.Point(127, 90);
            this.SongDBConverter_SrcDBType_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongDBConverter_SrcDBType_ComboBox.Name = "SongDBConverter_SrcDBType_ComboBox";
            this.SongDBConverter_SrcDBType_ComboBox.Size = new System.Drawing.Size(320, 30);
            this.SongDBConverter_SrcDBType_ComboBox.TabIndex = 4;
            this.SongDBConverter_SrcDBType_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_SrcDBType_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_SrcDBType_Label
            // 
            this.SongDBConverter_SrcDBType_Label.AutoSize = true;
            this.SongDBConverter_SrcDBType_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_SrcDBType_Label.Location = new System.Drawing.Point(16, 94);
            this.SongDBConverter_SrcDBType_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongDBConverter_SrcDBType_Label.Name = "SongDBConverter_SrcDBType_Label";
            this.SongDBConverter_SrcDBType_Label.Size = new System.Drawing.Size(99, 22);
            this.SongDBConverter_SrcDBType_Label.TabIndex = 3;
            this.SongDBConverter_SrcDBType_Label.Text = "資料庫類型:";
            // 
            // SongDBConverter_DestDBFile_Button
            // 
            this.SongDBConverter_DestDBFile_Button.AutoSize = true;
            this.SongDBConverter_DestDBFile_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_DestDBFile_Button.Location = new System.Drawing.Point(866, 139);
            this.SongDBConverter_DestDBFile_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongDBConverter_DestDBFile_Button.Name = "SongDBConverter_DestDBFile_Button";
            this.SongDBConverter_DestDBFile_Button.Size = new System.Drawing.Size(70, 32);
            this.SongDBConverter_DestDBFile_Button.TabIndex = 7;
            this.SongDBConverter_DestDBFile_Button.Text = "瀏覽";
            this.SongDBConverter_DestDBFile_Button.UseVisualStyleBackColor = true;
            this.SongDBConverter_DestDBFile_Button.Click += new System.EventHandler(this.SongDBConverter_DestDBFile_Button_Click);
            // 
            // SongDBConverter_DestDBFile_TextBox
            // 
            this.SongDBConverter_DestDBFile_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_DestDBFile_TextBox.Location = new System.Drawing.Point(127, 140);
            this.SongDBConverter_DestDBFile_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongDBConverter_DestDBFile_TextBox.Name = "SongDBConverter_DestDBFile_TextBox";
            this.SongDBConverter_DestDBFile_TextBox.ReadOnly = true;
            this.SongDBConverter_DestDBFile_TextBox.Size = new System.Drawing.Size(727, 30);
            this.SongDBConverter_DestDBFile_TextBox.TabIndex = 6;
            // 
            // SongDBConverter_DestDBFile_Label
            // 
            this.SongDBConverter_DestDBFile_Label.AutoSize = true;
            this.SongDBConverter_DestDBFile_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_DestDBFile_Label.Location = new System.Drawing.Point(16, 144);
            this.SongDBConverter_DestDBFile_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongDBConverter_DestDBFile_Label.Name = "SongDBConverter_DestDBFile_Label";
            this.SongDBConverter_DestDBFile_Label.Size = new System.Drawing.Size(99, 22);
            this.SongDBConverter_DestDBFile_Label.TabIndex = 5;
            this.SongDBConverter_DestDBFile_Label.Text = "目的資料庫:";
            // 
            // SongDBConverter_SrcDBFile_Button
            // 
            this.SongDBConverter_SrcDBFile_Button.Enabled = false;
            this.SongDBConverter_SrcDBFile_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_SrcDBFile_Button.Location = new System.Drawing.Point(866, 39);
            this.SongDBConverter_SrcDBFile_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongDBConverter_SrcDBFile_Button.Name = "SongDBConverter_SrcDBFile_Button";
            this.SongDBConverter_SrcDBFile_Button.Size = new System.Drawing.Size(70, 32);
            this.SongDBConverter_SrcDBFile_Button.TabIndex = 2;
            this.SongDBConverter_SrcDBFile_Button.Text = "瀏覽";
            this.SongDBConverter_SrcDBFile_Button.UseVisualStyleBackColor = true;
            this.SongDBConverter_SrcDBFile_Button.Click += new System.EventHandler(this.SongDBConverter_SrcDBFile_Button_Click);
            // 
            // SongDBConverter_SrcDBFile_TextBox
            // 
            this.SongDBConverter_SrcDBFile_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_SrcDBFile_TextBox.Location = new System.Drawing.Point(127, 40);
            this.SongDBConverter_SrcDBFile_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongDBConverter_SrcDBFile_TextBox.Name = "SongDBConverter_SrcDBFile_TextBox";
            this.SongDBConverter_SrcDBFile_TextBox.ReadOnly = true;
            this.SongDBConverter_SrcDBFile_TextBox.Size = new System.Drawing.Size(727, 30);
            this.SongDBConverter_SrcDBFile_TextBox.TabIndex = 1;
            // 
            // SongDBConverter_SrcDBFile_Label
            // 
            this.SongDBConverter_SrcDBFile_Label.AutoSize = true;
            this.SongDBConverter_SrcDBFile_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_SrcDBFile_Label.Location = new System.Drawing.Point(16, 44);
            this.SongDBConverter_SrcDBFile_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 14);
            this.SongDBConverter_SrcDBFile_Label.Name = "SongDBConverter_SrcDBFile_Label";
            this.SongDBConverter_SrcDBFile_Label.Size = new System.Drawing.Size(99, 22);
            this.SongDBConverter_SrcDBFile_Label.TabIndex = 0;
            this.SongDBConverter_SrcDBFile_Label.Text = "來源資料庫:";
            // 
            // SongDBConverter_JetktvLangCfg_GroupBox
            // 
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang9_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang9_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang8_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang8_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang7_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang7_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang6_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang6_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang5_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang5_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang4_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang4_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang3_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang3_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang2_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang2_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang1_ComboBox);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvLang1_Label);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Location = new System.Drawing.Point(635, 380);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Margin = new System.Windows.Forms.Padding(14, 3, 3, 3);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Name = "SongDBConverter_JetktvLangCfg_GroupBox";
            this.SongDBConverter_JetktvLangCfg_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongDBConverter_JetktvLangCfg_GroupBox.Size = new System.Drawing.Size(340, 276);
            this.SongDBConverter_JetktvLangCfg_GroupBox.TabIndex = 6;
            this.SongDBConverter_JetktvLangCfg_GroupBox.TabStop = false;
            this.SongDBConverter_JetktvLangCfg_GroupBox.Text = "JetKTV 語系對應";
            this.SongDBConverter_JetktvLangCfg_GroupBox.Visible = false;
            // 
            // SongDBConverter_JetktvLang9_ComboBox
            // 
            this.SongDBConverter_JetktvLang9_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang9_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang9_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang9_ComboBox.Location = new System.Drawing.Point(214, 184);
            this.SongDBConverter_JetktvLang9_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang9_ComboBox.Name = "SongDBConverter_JetktvLang9_ComboBox";
            this.SongDBConverter_JetktvLang9_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang9_ComboBox.TabIndex = 17;
            this.SongDBConverter_JetktvLang9_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang9_Label
            // 
            this.SongDBConverter_JetktvLang9_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang9_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang9_Label.Location = new System.Drawing.Point(178, 188);
            this.SongDBConverter_JetktvLang9_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang9_Label.Name = "SongDBConverter_JetktvLang9_Label";
            this.SongDBConverter_JetktvLang9_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang9_Label.TabIndex = 16;
            this.SongDBConverter_JetktvLang9_Label.Text = "9:";
            // 
            // SongDBConverter_JetktvLang8_ComboBox
            // 
            this.SongDBConverter_JetktvLang8_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang8_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang8_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang8_ComboBox.Location = new System.Drawing.Point(214, 136);
            this.SongDBConverter_JetktvLang8_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang8_ComboBox.Name = "SongDBConverter_JetktvLang8_ComboBox";
            this.SongDBConverter_JetktvLang8_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang8_ComboBox.TabIndex = 15;
            this.SongDBConverter_JetktvLang8_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang8_Label
            // 
            this.SongDBConverter_JetktvLang8_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang8_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang8_Label.Location = new System.Drawing.Point(178, 140);
            this.SongDBConverter_JetktvLang8_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang8_Label.Name = "SongDBConverter_JetktvLang8_Label";
            this.SongDBConverter_JetktvLang8_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang8_Label.TabIndex = 14;
            this.SongDBConverter_JetktvLang8_Label.Text = "8:";
            // 
            // SongDBConverter_JetktvLang7_ComboBox
            // 
            this.SongDBConverter_JetktvLang7_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang7_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang7_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang7_ComboBox.Location = new System.Drawing.Point(214, 88);
            this.SongDBConverter_JetktvLang7_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang7_ComboBox.Name = "SongDBConverter_JetktvLang7_ComboBox";
            this.SongDBConverter_JetktvLang7_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang7_ComboBox.TabIndex = 13;
            this.SongDBConverter_JetktvLang7_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang7_Label
            // 
            this.SongDBConverter_JetktvLang7_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang7_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang7_Label.Location = new System.Drawing.Point(178, 92);
            this.SongDBConverter_JetktvLang7_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang7_Label.Name = "SongDBConverter_JetktvLang7_Label";
            this.SongDBConverter_JetktvLang7_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang7_Label.TabIndex = 12;
            this.SongDBConverter_JetktvLang7_Label.Text = "7:";
            // 
            // SongDBConverter_JetktvLang6_ComboBox
            // 
            this.SongDBConverter_JetktvLang6_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang6_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang6_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang6_ComboBox.Location = new System.Drawing.Point(214, 40);
            this.SongDBConverter_JetktvLang6_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang6_ComboBox.Name = "SongDBConverter_JetktvLang6_ComboBox";
            this.SongDBConverter_JetktvLang6_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang6_ComboBox.TabIndex = 11;
            this.SongDBConverter_JetktvLang6_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang6_Label
            // 
            this.SongDBConverter_JetktvLang6_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang6_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang6_Label.Location = new System.Drawing.Point(178, 44);
            this.SongDBConverter_JetktvLang6_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang6_Label.Name = "SongDBConverter_JetktvLang6_Label";
            this.SongDBConverter_JetktvLang6_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang6_Label.TabIndex = 10;
            this.SongDBConverter_JetktvLang6_Label.Text = "6:";
            // 
            // SongDBConverter_JetktvLang5_ComboBox
            // 
            this.SongDBConverter_JetktvLang5_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang5_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang5_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang5_ComboBox.Location = new System.Drawing.Point(52, 232);
            this.SongDBConverter_JetktvLang5_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang5_ComboBox.Name = "SongDBConverter_JetktvLang5_ComboBox";
            this.SongDBConverter_JetktvLang5_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang5_ComboBox.TabIndex = 9;
            this.SongDBConverter_JetktvLang5_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang5_Label
            // 
            this.SongDBConverter_JetktvLang5_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang5_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang5_Label.Location = new System.Drawing.Point(16, 236);
            this.SongDBConverter_JetktvLang5_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang5_Label.Name = "SongDBConverter_JetktvLang5_Label";
            this.SongDBConverter_JetktvLang5_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang5_Label.TabIndex = 8;
            this.SongDBConverter_JetktvLang5_Label.Text = "5:";
            // 
            // SongDBConverter_JetktvLang4_ComboBox
            // 
            this.SongDBConverter_JetktvLang4_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang4_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang4_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang4_ComboBox.Location = new System.Drawing.Point(52, 184);
            this.SongDBConverter_JetktvLang4_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang4_ComboBox.Name = "SongDBConverter_JetktvLang4_ComboBox";
            this.SongDBConverter_JetktvLang4_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang4_ComboBox.TabIndex = 7;
            this.SongDBConverter_JetktvLang4_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang4_Label
            // 
            this.SongDBConverter_JetktvLang4_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang4_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang4_Label.Location = new System.Drawing.Point(16, 188);
            this.SongDBConverter_JetktvLang4_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang4_Label.Name = "SongDBConverter_JetktvLang4_Label";
            this.SongDBConverter_JetktvLang4_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang4_Label.TabIndex = 6;
            this.SongDBConverter_JetktvLang4_Label.Text = "4:";
            // 
            // SongDBConverter_JetktvLang3_ComboBox
            // 
            this.SongDBConverter_JetktvLang3_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang3_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang3_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang3_ComboBox.Location = new System.Drawing.Point(52, 136);
            this.SongDBConverter_JetktvLang3_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang3_ComboBox.Name = "SongDBConverter_JetktvLang3_ComboBox";
            this.SongDBConverter_JetktvLang3_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang3_ComboBox.TabIndex = 5;
            this.SongDBConverter_JetktvLang3_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang3_Label
            // 
            this.SongDBConverter_JetktvLang3_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang3_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang3_Label.Location = new System.Drawing.Point(16, 140);
            this.SongDBConverter_JetktvLang3_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang3_Label.Name = "SongDBConverter_JetktvLang3_Label";
            this.SongDBConverter_JetktvLang3_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang3_Label.TabIndex = 4;
            this.SongDBConverter_JetktvLang3_Label.Text = "3:";
            // 
            // SongDBConverter_JetktvLang2_ComboBox
            // 
            this.SongDBConverter_JetktvLang2_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang2_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang2_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang2_ComboBox.Location = new System.Drawing.Point(52, 88);
            this.SongDBConverter_JetktvLang2_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang2_ComboBox.Name = "SongDBConverter_JetktvLang2_ComboBox";
            this.SongDBConverter_JetktvLang2_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang2_ComboBox.TabIndex = 3;
            this.SongDBConverter_JetktvLang2_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang2_Label
            // 
            this.SongDBConverter_JetktvLang2_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang2_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang2_Label.Location = new System.Drawing.Point(16, 92);
            this.SongDBConverter_JetktvLang2_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang2_Label.Name = "SongDBConverter_JetktvLang2_Label";
            this.SongDBConverter_JetktvLang2_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang2_Label.TabIndex = 2;
            this.SongDBConverter_JetktvLang2_Label.Text = "2:";
            // 
            // SongDBConverter_JetktvLang1_ComboBox
            // 
            this.SongDBConverter_JetktvLang1_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SongDBConverter_JetktvLang1_ComboBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang1_ComboBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvLang1_ComboBox.Location = new System.Drawing.Point(52, 40);
            this.SongDBConverter_JetktvLang1_ComboBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 8);
            this.SongDBConverter_JetktvLang1_ComboBox.Name = "SongDBConverter_JetktvLang1_ComboBox";
            this.SongDBConverter_JetktvLang1_ComboBox.Size = new System.Drawing.Size(110, 30);
            this.SongDBConverter_JetktvLang1_ComboBox.TabIndex = 1;
            this.SongDBConverter_JetktvLang1_ComboBox.SelectedIndexChanged += new System.EventHandler(this.SongDBConverter_JetktvLang_ComboBox_SelectedIndexChanged);
            // 
            // SongDBConverter_JetktvLang1_Label
            // 
            this.SongDBConverter_JetktvLang1_Label.AutoSize = true;
            this.SongDBConverter_JetktvLang1_Label.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvLang1_Label.Location = new System.Drawing.Point(16, 44);
            this.SongDBConverter_JetktvLang1_Label.Margin = new System.Windows.Forms.Padding(6, 14, 6, 12);
            this.SongDBConverter_JetktvLang1_Label.Name = "SongDBConverter_JetktvLang1_Label";
            this.SongDBConverter_JetktvLang1_Label.Size = new System.Drawing.Size(24, 22);
            this.SongDBConverter_JetktvLang1_Label.TabIndex = 0;
            this.SongDBConverter_JetktvLang1_Label.Text = "1:";
            // 
            // SongDBConverter_JetktvPathCfg_GroupBox
            // 
            this.SongDBConverter_JetktvPathCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvPathCfg_Button);
            this.SongDBConverter_JetktvPathCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvPathCfg_TextBox);
            this.SongDBConverter_JetktvPathCfg_GroupBox.Controls.Add(this.SongDBConverter_JetktvPathCfg_ListBox);
            this.SongDBConverter_JetktvPathCfg_GroupBox.Location = new System.Drawing.Point(23, 380);
            this.SongDBConverter_JetktvPathCfg_GroupBox.Name = "SongDBConverter_JetktvPathCfg_GroupBox";
            this.SongDBConverter_JetktvPathCfg_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongDBConverter_JetktvPathCfg_GroupBox.Size = new System.Drawing.Size(595, 276);
            this.SongDBConverter_JetktvPathCfg_GroupBox.TabIndex = 5;
            this.SongDBConverter_JetktvPathCfg_GroupBox.TabStop = false;
            this.SongDBConverter_JetktvPathCfg_GroupBox.Text = "JetKTV 歌庫路徑";
            this.SongDBConverter_JetktvPathCfg_GroupBox.Visible = false;
            // 
            // SongDBConverter_JetktvPathCfg_Button
            // 
            this.SongDBConverter_JetktvPathCfg_Button.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvPathCfg_Button.Location = new System.Drawing.Point(509, 232);
            this.SongDBConverter_JetktvPathCfg_Button.Margin = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.SongDBConverter_JetktvPathCfg_Button.Name = "SongDBConverter_JetktvPathCfg_Button";
            this.SongDBConverter_JetktvPathCfg_Button.Size = new System.Drawing.Size(70, 32);
            this.SongDBConverter_JetktvPathCfg_Button.TabIndex = 1;
            this.SongDBConverter_JetktvPathCfg_Button.Text = "瀏覽";
            this.SongDBConverter_JetktvPathCfg_Button.UseVisualStyleBackColor = true;
            this.SongDBConverter_JetktvPathCfg_Button.Click += new System.EventHandler(this.SongDBConverter_JetktvPathCfg_Button_Click);
            // 
            // SongDBConverter_JetktvPathCfg_TextBox
            // 
            this.SongDBConverter_JetktvPathCfg_TextBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvPathCfg_TextBox.Location = new System.Drawing.Point(16, 233);
            this.SongDBConverter_JetktvPathCfg_TextBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongDBConverter_JetktvPathCfg_TextBox.Name = "SongDBConverter_JetktvPathCfg_TextBox";
            this.SongDBConverter_JetktvPathCfg_TextBox.ReadOnly = true;
            this.SongDBConverter_JetktvPathCfg_TextBox.Size = new System.Drawing.Size(481, 30);
            this.SongDBConverter_JetktvPathCfg_TextBox.TabIndex = 0;
            // 
            // SongDBConverter_JetktvPathCfg_ListBox
            // 
            this.SongDBConverter_JetktvPathCfg_ListBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongDBConverter_JetktvPathCfg_ListBox.FormattingEnabled = true;
            this.SongDBConverter_JetktvPathCfg_ListBox.ItemHeight = 22;
            this.SongDBConverter_JetktvPathCfg_ListBox.Location = new System.Drawing.Point(16, 40);
            this.SongDBConverter_JetktvPathCfg_ListBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongDBConverter_JetktvPathCfg_ListBox.Name = "SongDBConverter_JetktvPathCfg_ListBox";
            this.SongDBConverter_JetktvPathCfg_ListBox.Size = new System.Drawing.Size(563, 180);
            this.SongDBConverter_JetktvPathCfg_ListBox.TabIndex = 1;
            // 
            // SongAddResult_TabPage
            // 
            this.SongAddResult_TabPage.Controls.Add(this.SongAddResult_SplitContainer);
            this.SongAddResult_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongAddResult_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongAddResult_TabPage.Name = "SongAddResult_TabPage";
            this.SongAddResult_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.SongAddResult_TabPage.Size = new System.Drawing.Size(998, 684);
            this.SongAddResult_TabPage.TabIndex = 4;
            this.SongAddResult_TabPage.Text = "加歌結果";
            this.SongAddResult_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongAddResult_SplitContainer
            // 
            this.SongAddResult_SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SongAddResult_SplitContainer.Location = new System.Drawing.Point(20, 20);
            this.SongAddResult_SplitContainer.MinimumSize = new System.Drawing.Size(958, 639);
            this.SongAddResult_SplitContainer.Name = "SongAddResult_SplitContainer";
            this.SongAddResult_SplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SongAddResult_SplitContainer.Panel1
            // 
            this.SongAddResult_SplitContainer.Panel1.Controls.Add(this.SongAddResult_DuplicateSong_GroupBox);
            this.SongAddResult_SplitContainer.Panel1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // SongAddResult_SplitContainer.Panel2
            // 
            this.SongAddResult_SplitContainer.Panel2.Controls.Add(this.SongAddResult_FailureSong_GroupBox);
            this.SongAddResult_SplitContainer.Panel2.Padding = new System.Windows.Forms.Padding(3);
            this.SongAddResult_SplitContainer.Size = new System.Drawing.Size(958, 644);
            this.SongAddResult_SplitContainer.SplitterDistance = 320;
            this.SongAddResult_SplitContainer.SplitterWidth = 3;
            this.SongAddResult_SplitContainer.TabIndex = 0;
            // 
            // SongAddResult_DuplicateSong_GroupBox
            // 
            this.SongAddResult_DuplicateSong_GroupBox.Controls.Add(this.SongAddResult_DuplicateSong_ListBox);
            this.SongAddResult_DuplicateSong_GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SongAddResult_DuplicateSong_GroupBox.Location = new System.Drawing.Point(3, 3);
            this.SongAddResult_DuplicateSong_GroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.SongAddResult_DuplicateSong_GroupBox.Name = "SongAddResult_DuplicateSong_GroupBox";
            this.SongAddResult_DuplicateSong_GroupBox.Padding = new System.Windows.Forms.Padding(16, 14, 16, 14);
            this.SongAddResult_DuplicateSong_GroupBox.Size = new System.Drawing.Size(952, 314);
            this.SongAddResult_DuplicateSong_GroupBox.TabIndex = 0;
            this.SongAddResult_DuplicateSong_GroupBox.TabStop = false;
            this.SongAddResult_DuplicateSong_GroupBox.Text = "重複歌曲 (雙擊滑鼠左鍵可複製至剪貼簿)";
            // 
            // SongAddResult_DuplicateSong_ListBox
            // 
            this.SongAddResult_DuplicateSong_ListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SongAddResult_DuplicateSong_ListBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAddResult_DuplicateSong_ListBox.FormattingEnabled = true;
            this.SongAddResult_DuplicateSong_ListBox.ItemHeight = 22;
            this.SongAddResult_DuplicateSong_ListBox.Location = new System.Drawing.Point(16, 41);
            this.SongAddResult_DuplicateSong_ListBox.Margin = new System.Windows.Forms.Padding(0);
            this.SongAddResult_DuplicateSong_ListBox.Name = "SongAddResult_DuplicateSong_ListBox";
            this.SongAddResult_DuplicateSong_ListBox.Size = new System.Drawing.Size(920, 259);
            this.SongAddResult_DuplicateSong_ListBox.TabIndex = 0;
            this.SongAddResult_DuplicateSong_ListBox.DoubleClick += new System.EventHandler(this.Common_ListBox_DoubleClick);
            // 
            // SongAddResult_FailureSong_GroupBox
            // 
            this.SongAddResult_FailureSong_GroupBox.Controls.Add(this.SongAddResult_FailureSong_ListBox);
            this.SongAddResult_FailureSong_GroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SongAddResult_FailureSong_GroupBox.Location = new System.Drawing.Point(3, 3);
            this.SongAddResult_FailureSong_GroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.SongAddResult_FailureSong_GroupBox.Name = "SongAddResult_FailureSong_GroupBox";
            this.SongAddResult_FailureSong_GroupBox.Padding = new System.Windows.Forms.Padding(16, 14, 16, 14);
            this.SongAddResult_FailureSong_GroupBox.Size = new System.Drawing.Size(952, 315);
            this.SongAddResult_FailureSong_GroupBox.TabIndex = 0;
            this.SongAddResult_FailureSong_GroupBox.TabStop = false;
            this.SongAddResult_FailureSong_GroupBox.Text = "失敗歌曲 (雙擊滑鼠左鍵可複製至剪貼簿)";
            // 
            // SongAddResult_FailureSong_ListBox
            // 
            this.SongAddResult_FailureSong_ListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SongAddResult_FailureSong_ListBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongAddResult_FailureSong_ListBox.FormattingEnabled = true;
            this.SongAddResult_FailureSong_ListBox.ItemHeight = 22;
            this.SongAddResult_FailureSong_ListBox.Location = new System.Drawing.Point(16, 41);
            this.SongAddResult_FailureSong_ListBox.Margin = new System.Windows.Forms.Padding(0);
            this.SongAddResult_FailureSong_ListBox.Name = "SongAddResult_FailureSong_ListBox";
            this.SongAddResult_FailureSong_ListBox.Size = new System.Drawing.Size(920, 260);
            this.SongAddResult_FailureSong_ListBox.TabIndex = 0;
            this.SongAddResult_FailureSong_ListBox.DoubleClick += new System.EventHandler(this.Common_ListBox_DoubleClick);
            // 
            // SongLog_TabPage
            // 
            this.SongLog_TabPage.Controls.Add(this.SongLog_GroupBox);
            this.SongLog_TabPage.Location = new System.Drawing.Point(4, 34);
            this.SongLog_TabPage.Margin = new System.Windows.Forms.Padding(0);
            this.SongLog_TabPage.Name = "SongLog_TabPage";
            this.SongLog_TabPage.Padding = new System.Windows.Forms.Padding(20);
            this.SongLog_TabPage.Size = new System.Drawing.Size(998, 684);
            this.SongLog_TabPage.TabIndex = 6;
            this.SongLog_TabPage.Text = "操作記錄";
            this.SongLog_TabPage.UseVisualStyleBackColor = true;
            // 
            // SongLog_GroupBox
            // 
            this.SongLog_GroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SongLog_GroupBox.Controls.Add(this.SongLog_ListBox);
            this.SongLog_GroupBox.Location = new System.Drawing.Point(20, 20);
            this.SongLog_GroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.SongLog_GroupBox.Name = "SongLog_GroupBox";
            this.SongLog_GroupBox.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.SongLog_GroupBox.Size = new System.Drawing.Size(958, 639);
            this.SongLog_GroupBox.TabIndex = 0;
            this.SongLog_GroupBox.TabStop = false;
            this.SongLog_GroupBox.Text = "異常操作記錄 (雙擊滑鼠左鍵可複製至剪貼簿)";
            // 
            // SongLog_ListBox
            // 
            this.SongLog_ListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SongLog_ListBox.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.SongLog_ListBox.FormattingEnabled = true;
            this.SongLog_ListBox.ItemHeight = 22;
            this.SongLog_ListBox.Location = new System.Drawing.Point(16, 40);
            this.SongLog_ListBox.Margin = new System.Windows.Forms.Padding(6, 10, 6, 10);
            this.SongLog_ListBox.Name = "SongLog_ListBox";
            this.SongLog_ListBox.Size = new System.Drawing.Size(926, 576);
            this.SongLog_ListBox.TabIndex = 0;
            this.SongLog_ListBox.DoubleClick += new System.EventHandler(this.Common_ListBox_DoubleClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1006, 722);
            this.Controls.Add(this.MainTabControl);
            this.Font = new System.Drawing.Font("微軟正黑體", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "MainForm";
            this.Text = "CrazyKTV 加歌程式 v1.3.3";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MainTabControl.ResumeLayout(false);
            this.SongQuery_TabPage.ResumeLayout(false);
            this.SongQuery_OtherQuery_GroupBox.ResumeLayout(false);
            this.SongQuery_OtherQuery_GroupBox.PerformLayout();
            this.SongQuery_Query_GroupBox.ResumeLayout(false);
            this.SongQuery_Query_GroupBox.PerformLayout();
            this.SongQuery_Statistics_GroupBox.ResumeLayout(false);
            this.SongQuery_Statistics_GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SongQuery_DataGridView)).EndInit();
            this.SongQuery_QueryStatus_GroupBox.ResumeLayout(false);
            this.SongQuery_QueryStatus_GroupBox.PerformLayout();
            this.SongAdd_TabPage.ResumeLayout(false);
            this.SongAdd_TabPage.PerformLayout();
            this.SongAdd_SpecialStr_GroupBox.ResumeLayout(false);
            this.SongAdd_SpecialStr_GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SongAdd_DataGridView)).EndInit();
            this.SongAdd_Tooltip_GroupBox.ResumeLayout(false);
            this.SongAdd_DefaultSongInfo_GroupBox.ResumeLayout(false);
            this.SongAdd_DefaultSongInfo_GroupBox.PerformLayout();
            this.SongAdd_SongAddCfg_GroupBox.ResumeLayout(false);
            this.SongAdd_SongAddCfg_GroupBox.PerformLayout();
            this.SingerMgr_TabPage.ResumeLayout(false);
            this.SingerMgr_OtherQuery_GroupBox.ResumeLayout(false);
            this.SingerMgr_OtherQuery_GroupBox.PerformLayout();
            this.SingerMgr_SingerAdd_GroupBox.ResumeLayout(false);
            this.SingerMgr_SingerAdd_GroupBox.PerformLayout();
            this.SingerMgr_Manager_GroupBox.ResumeLayout(false);
            this.SingerMgr_Statistics_GroupBox.ResumeLayout(false);
            this.SingerMgr_Statistics_GroupBox.PerformLayout();
            this.SingerMgr_Tooltip_GroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SingerMgr_DataGridView)).EndInit();
            this.SingerMgr_Query_GroupBox.ResumeLayout(false);
            this.SingerMgr_Query_GroupBox.PerformLayout();
            this.SongMgrCfg_TabPage.ResumeLayout(false);
            this.SongMgrCfg_TabPage.PerformLayout();
            this.SongMgrCfg_SongType_GroupBox.ResumeLayout(false);
            this.SongMgrCfg_SongType_GroupBox.PerformLayout();
            this.SongMgrCfg_SongStructure_TabControl.ResumeLayout(false);
            this.SongMgrCfg_SongStructure_TabPage.ResumeLayout(false);
            this.SongMgrCfg_SongStructure_TabPage.PerformLayout();
            this.SongMgrCfg_CustomStructure_TabPage.ResumeLayout(false);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.ResumeLayout(false);
            this.SongMgrCfg_CustomSingerTypeStructure_GroupBox.PerformLayout();
            this.SongMgrCfg_Tooltip_GroupBox.ResumeLayout(false);
            this.SongMgrCfg_SongID_GroupBox.ResumeLayout(false);
            this.SongMgrCfg_SongID_GroupBox.PerformLayout();
            this.SongMgrCfg_General_GroupBox.ResumeLayout(false);
            this.SongMgrCfg_General_GroupBox.PerformLayout();
            this.SongMaintenance_TabPage.ResumeLayout(false);
            this.SongMaintenance_TabPage.PerformLayout();
            this.SongMaintenance_SongPathChange_GroupBox.ResumeLayout(false);
            this.SongMaintenance_SongPathChange_GroupBox.PerformLayout();
            this.SongMaintenance_PlayCount_GroupBox.ResumeLayout(false);
            this.SongMaintenance_TabControl.ResumeLayout(false);
            this.SongMaintenance_Misc_TabPage.ResumeLayout(false);
            this.SongMaintenance_RebuildSongStructure_GroupBox.ResumeLayout(false);
            this.SongMaintenance_RebuildSongStructure_GroupBox.PerformLayout();
            this.SongMaintenance_Phonetics_GroupBox.ResumeLayout(false);
            this.SongMaintenance_RemoteCfg_GroupBox.ResumeLayout(false);
            this.SongMaintenance_Misc_GroupBox.ResumeLayout(false);
            this.SongMaintenance_Favorite_TabPage.ResumeLayout(false);
            this.SongMaintenance_Favorite_TabPage.PerformLayout();
            this.SongMaintenance_CustomLang_TabPage.ResumeLayout(false);
            this.SongMaintenance_CustomLang_TabPage.PerformLayout();
            this.SongMaintenance_MultiSongPath_TabPage.ResumeLayout(false);
            this.SongMaintenance_MultiSongPath_TabPage.PerformLayout();
            this.SongMaintenance_DBVer_TabPage.ResumeLayout(false);
            this.SongMaintenance_DBVer_TabPage.PerformLayout();
            this.SongMaintenance_DBVerUpdate_GroupBox.ResumeLayout(false);
            this.SongMaintenance_DBVerUpdate_GroupBox.PerformLayout();
            this.SongMaintenance_DBVerTooltip_GroupBox.ResumeLayout(false);
            this.SongMaintenance_VolumeChange_GroupBox.ResumeLayout(false);
            this.SongMaintenance_VolumeChange_GroupBox.PerformLayout();
            this.SongMaintenance_TrackExchange_GroupBox.ResumeLayout(false);
            this.SongMaintenance_CodeConv_GroupBox.ResumeLayout(false);
            this.SongMaintenance_Tooltip_GroupBox.ResumeLayout(false);
            this.SongMaintenance_SpellCorrect_GroupBox.ResumeLayout(false);
            this.MainCfg_TabPage.ResumeLayout(false);
            this.MainCfg_TabPage.PerformLayout();
            this.MainCfg_Tooltip_GroupBox.ResumeLayout(false);
            this.MainCfg_General_ＧroupBox.ResumeLayout(false);
            this.MainCfg_General_ＧroupBox.PerformLayout();
            this.SongDBConverter_TabPage.ResumeLayout(false);
            this.SongDBConverter_TabPage.PerformLayout();
            this.SongDBConverter_ConvHelp_GroupBox.ResumeLayout(false);
            this.SongDBConverter_Tooltip_GroupBox.ResumeLayout(false);
            this.SongDBConverter_Converter_GroupBox.ResumeLayout(false);
            this.SongDBConverter_Converter_GroupBox.PerformLayout();
            this.SongDBConverter_JetktvLangCfg_GroupBox.ResumeLayout(false);
            this.SongDBConverter_JetktvLangCfg_GroupBox.PerformLayout();
            this.SongDBConverter_JetktvPathCfg_GroupBox.ResumeLayout(false);
            this.SongDBConverter_JetktvPathCfg_GroupBox.PerformLayout();
            this.SongAddResult_TabPage.ResumeLayout(false);
            this.SongAddResult_SplitContainer.Panel1.ResumeLayout(false);
            this.SongAddResult_SplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SongAddResult_SplitContainer)).EndInit();
            this.SongAddResult_SplitContainer.ResumeLayout(false);
            this.SongAddResult_DuplicateSong_GroupBox.ResumeLayout(false);
            this.SongAddResult_FailureSong_GroupBox.ResumeLayout(false);
            this.SongLog_TabPage.ResumeLayout(false);
            this.SongLog_GroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage SongQuery_TabPage;
        private System.Windows.Forms.DataGridView SongQuery_DataGridView;
        private System.Windows.Forms.GroupBox SongQuery_Query_GroupBox;
        private System.Windows.Forms.Label SongQuery_QueryValue_Label;
        private System.Windows.Forms.TextBox SongQuery_QueryValue_TextBox;
        private System.Windows.Forms.Button SongQuery_Query_Button;
        private System.Windows.Forms.Label SongQuery_QueryType_Label;
        private System.Windows.Forms.ComboBox SongQuery_QueryType_ComboBox;
        private System.Windows.Forms.GroupBox SongQuery_Statistics_GroupBox;
        private System.Windows.Forms.GroupBox SongQuery_QueryStatus_GroupBox;
        private System.Windows.Forms.Label SongQuery_QueryStatus_Label;
        private System.Windows.Forms.CheckBox SongQuery_EditMode_CheckBox;
        private System.Windows.Forms.TabPage SongMgrCfg_TabPage;
        private System.Windows.Forms.GroupBox SongMgrCfg_General_GroupBox;
        private System.Windows.Forms.Label SongMgrCfg_DBFile_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_DBFile_TextBox;
        private System.Windows.Forms.Button SongMgrCfg_DBFile_Button;
        private System.Windows.Forms.TextBox SongMgrCfg_SupportFormat_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_SupportFormat_Label;
        private System.Windows.Forms.Button SongMgrCfg_Save_Button;
        private System.Windows.Forms.TextBox SongMgrCfg_DestFolder_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_DestFolder_Label;
        private System.Windows.Forms.Button SongMgrCfg_DestFolder_Button;
        private System.Windows.Forms.Label SongMgrCfg_SongAddMode_Label;
        private System.Windows.Forms.ComboBox SongMgrCfg_SongAddMode_ComboBox;
        private System.Windows.Forms.CheckBox SongMgrCfg_CrtchorusMerge_CheckBox;
        private System.Windows.Forms.GroupBox SongMgrCfg_Tooltip_GroupBox;
        private System.Windows.Forms.GroupBox SongMgrCfg_SongID_GroupBox;
        private System.Windows.Forms.Label SongMgrCfg_MaxDigitCode_Label;
        private System.Windows.Forms.ComboBox SongMgrCfg_MaxDigitCode_ComboBox;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang1Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang1Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang5Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang5Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang4Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang4Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang3Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang3Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang2Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang2Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang6Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang6Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang10Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang10Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang9Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang9Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang8Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang8Code_Label;
        private System.Windows.Forms.TextBox SongMgrCfg_Lang7Code_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Lang7Code_Label;
        private System.Windows.Forms.GroupBox SongMgrCfg_SongType_GroupBox;
        private System.Windows.Forms.ListBox SongMgrCfg_SongType_ListBox;
        private System.Windows.Forms.Button SongMgrCfg_SongType_Button;
        private System.Windows.Forms.TextBox SongMgrCfg_SongType_TextBox;
        private System.Windows.Forms.Label SongMgrCfg_Tooltip_Label;
        private System.Windows.Forms.TabPage SongAdd_TabPage;
        private System.Windows.Forms.GroupBox SongAdd_SongAddCfg_GroupBox;
        private System.Windows.Forms.ComboBox SongMgrCfg_SongInfoSeparate_ComboBox;
        private System.Windows.Forms.Label SongMgrCfg_SongInfoSeparate_Label;
        private System.Windows.Forms.Label SongMgrCfg_CrtchorusSeparate_Label;
        private System.Windows.Forms.ComboBox SongMgrCfg_CrtchorusSeparate_ComboBox;
        private System.Windows.Forms.GroupBox SongAdd_DefaultSongInfo_GroupBox;
        private System.Windows.Forms.Label SongAdd_DefaultSongVolume_Label;
        private System.Windows.Forms.Label SongAdd_DefaultSongType_Label;
        private System.Windows.Forms.Label SongAdd_DefaultSongTrack_Label;
        private System.Windows.Forms.Label SongAdd_DefaultSingerType_Label;
        private System.Windows.Forms.Label SongAdd_DefaultSongLang_Label;
        private System.Windows.Forms.ComboBox SongAdd_DefaultSongLang_ComboBox;
        private System.Windows.Forms.Button SongAdd_Save_Button;
        private System.Windows.Forms.ComboBox SongAdd_DefaultSingerType_ComboBox;
        private System.Windows.Forms.ComboBox SongAdd_DefaultSongTrack_ComboBox;
        private System.Windows.Forms.ComboBox SongAdd_DefaultSongType_ComboBox;
        private System.Windows.Forms.TextBox SongAdd_DefaultSongVolume_TextBox;
        private System.Windows.Forms.GroupBox SongAdd_Tooltip_GroupBox;
        private System.Windows.Forms.Label SongAdd_Tooltip_Label;
        private System.Windows.Forms.Label SongAdd_SongIdentificationMode_Label;
        private System.Windows.Forms.ComboBox SongAdd_SongIdentificationMode_ComboBox;
        private System.Windows.Forms.DataGridView SongAdd_DataGridView;
        private System.Windows.Forms.GroupBox SongAdd_SpecialStr_GroupBox;
        private System.Windows.Forms.TextBox SongAdd_SpecialStr_TextBox;
        private System.Windows.Forms.Button SongAdd_SpecialStr_Button;
        private System.Windows.Forms.ListBox SongAdd_SpecialStr_ListBox;
        private System.Windows.Forms.Button SongAdd_Add_Button;
        private System.Windows.Forms.ComboBox SongMgrCfg_FileStructure_ComboBox;
        private System.Windows.Forms.Label SongMgrCfg_FileStructure_Label;
        private System.Windows.Forms.ComboBox SongMgrCfg_FolderStructure_ComboBox;
        private System.Windows.Forms.Label SongMgrCfg_FolderStructure_Label;
        private System.Windows.Forms.TabPage SongAddResult_TabPage;
        private System.Windows.Forms.GroupBox SongAddResult_DuplicateSong_GroupBox;
        private System.Windows.Forms.ListBox SongAddResult_DuplicateSong_ListBox;
        private System.Windows.Forms.GroupBox SongAddResult_FailureSong_GroupBox;
        private System.Windows.Forms.ListBox SongAddResult_FailureSong_ListBox;
        private System.Windows.Forms.SplitContainer SongAddResult_SplitContainer;
        private System.Windows.Forms.TabPage SongDBConverter_TabPage;
        private System.Windows.Forms.GroupBox SongDBConverter_Converter_GroupBox;
        private System.Windows.Forms.ComboBox SongDBConverter_SrcDBType_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_SrcDBType_Label;
        private System.Windows.Forms.Button SongDBConverter_DestDBFile_Button;
        private System.Windows.Forms.TextBox SongDBConverter_DestDBFile_TextBox;
        private System.Windows.Forms.Label SongDBConverter_DestDBFile_Label;
        private System.Windows.Forms.Button SongDBConverter_SrcDBFile_Button;
        private System.Windows.Forms.TextBox SongDBConverter_SrcDBFile_TextBox;
        private System.Windows.Forms.Label SongDBConverter_SrcDBFile_Label;
        private System.Windows.Forms.GroupBox SongDBConverter_Tooltip_GroupBox;
        private System.Windows.Forms.Label SongDBConverter_Tooltip_Label;
        private System.Windows.Forms.Button SongDBConverter_StartConv_Button;
        private System.Windows.Forms.CheckBox SongMgrCfg_SongTrackMode_CheckBox;
        private System.Windows.Forms.GroupBox SongDBConverter_ConvHelp_GroupBox;
        private System.Windows.Forms.RichTextBox SongDBConverter_ConvHelp_RichTextBox;
        private System.Windows.Forms.TabPage SongLog_TabPage;
        private System.Windows.Forms.GroupBox SongLog_GroupBox;
        private System.Windows.Forms.ListBox SongLog_ListBox;
        private System.Windows.Forms.Label SongAdd_DragDrop_Label;
        private System.Windows.Forms.ComboBox SongAdd_DupSongMode_ComboBox;
        private System.Windows.Forms.Label SongAdd_DupSongMode_Label;
        private System.Windows.Forms.GroupBox SongDBConverter_JetktvLangCfg_GroupBox;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang1_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang1_Label;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang5_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang5_Label;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang4_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang4_Label;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang3_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang3_Label;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang2_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang2_Label;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang9_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang9_Label;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang8_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang8_Label;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang7_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang7_Label;
        private System.Windows.Forms.ComboBox SongDBConverter_JetktvLang6_ComboBox;
        private System.Windows.Forms.Label SongDBConverter_JetktvLang6_Label;
        private System.Windows.Forms.GroupBox SongDBConverter_JetktvPathCfg_GroupBox;
        private System.Windows.Forms.Button SongDBConverter_JetktvPathCfg_Button;
        private System.Windows.Forms.TextBox SongDBConverter_JetktvPathCfg_TextBox;
        private System.Windows.Forms.ListBox SongDBConverter_JetktvPathCfg_ListBox;
        private System.Windows.Forms.TabPage SongMaintenance_TabPage;
        private System.Windows.Forms.Label SongQuery_Statistics1_Label;
        private System.Windows.Forms.Label SongQuery_Statistics6_Label;
        private System.Windows.Forms.Label SongQuery_Statistics5_Label;
        private System.Windows.Forms.Label SongQuery_Statistics4_Label;
        private System.Windows.Forms.Label SongQuery_Statistics3_Label;
        private System.Windows.Forms.Label SongQuery_Statistics2_Label;
        private System.Windows.Forms.Label SongQuery_Statistics11Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics10Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics9Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics8Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics7Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics11_Label;
        private System.Windows.Forms.Label SongQuery_Statistics10_Label;
        private System.Windows.Forms.Label SongQuery_Statistics9_Label;
        private System.Windows.Forms.Label SongQuery_Statistics8_Label;
        private System.Windows.Forms.Label SongQuery_Statistics7_Label;
        private System.Windows.Forms.Label SongQuery_Statistics6Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics5Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics4Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics3Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics2Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics1Value_Label;
        private System.Windows.Forms.GroupBox SongMaintenance_SpellCorrect_GroupBox;
        private System.Windows.Forms.Button SongMaintenance_SongSpellCorrect_Button;
        private System.Windows.Forms.Button SongMaintenance_SingerSpellCorrect_Button;
        private System.Windows.Forms.GroupBox SongMaintenance_Tooltip_GroupBox;
        private System.Windows.Forms.Label SongMaintenance_Tooltip_Label;
        private System.Windows.Forms.GroupBox SongMaintenance_CodeConv_GroupBox;
        private System.Windows.Forms.Button SongMaintenance_CodeConvTo6_Button;
        private System.Windows.Forms.Button SongMaintenance_CodeConvTo5_Button;
        private System.Windows.Forms.Button SongMaintenance_CodeCorrect_Button;
        private System.Windows.Forms.ComboBox SongQuery_ExceptionalQuery_ComboBox;
        private System.Windows.Forms.Label SongQuery_ExceptionalQuery_Label;
        private System.Windows.Forms.GroupBox SongQuery_OtherQuery_GroupBox;
        private System.Windows.Forms.CheckBox SongQuery_FuzzyQuery_CheckBox;
        private System.Windows.Forms.ComboBox SongQuery_QueryFilter_ComboBox;
        private System.Windows.Forms.Label SongQuery_QueryFilter_Label;
        private System.Windows.Forms.ComboBox SongQuery_FavoriteQuery_ComboBox;
        private System.Windows.Forms.Label SongQuery_FavoriteQuery_Label;
        private System.Windows.Forms.GroupBox SongMaintenance_TrackExchange_GroupBox;
        private System.Windows.Forms.Button SongMaintenance_LRTrackExchange_Button;
        private System.Windows.Forms.GroupBox SongMaintenance_VolumeChange_GroupBox;
        private System.Windows.Forms.TextBox SongMaintenance_VolumeChange_TextBox;
        private System.Windows.Forms.Label SongMaintenance_VolumeChange_Label;
        private System.Windows.Forms.Button SongMaintenance_VolumeChange_Button;
        private System.Windows.Forms.TabControl SongMaintenance_TabControl;
        private System.Windows.Forms.TabPage SongMaintenance_Favorite_TabPage;
        private System.Windows.Forms.TabPage SongMaintenance_CustomLang_TabPage;
        private System.Windows.Forms.TextBox SongMaintenance_Favorite_TextBox;
        private System.Windows.Forms.Button SongMaintenance_Favorite_Button;
        private System.Windows.Forms.ListBox SongMaintenance_Favorite_ListBox;
        private System.Windows.Forms.Button SongMaintenance_FavoriteExport_Button;
        private System.Windows.Forms.Button SongMaintenance_FavoriteImport_Button;
        private System.Windows.Forms.GroupBox SongMaintenance_PlayCount_GroupBox;
        private System.Windows.Forms.Button SongMaintenance_PlayCountReset_Button;
        private System.Windows.Forms.TabPage SingerMgr_TabPage;
        private System.Windows.Forms.GroupBox SingerMgr_Tooltip_GroupBox;
        private System.Windows.Forms.Label SingerMgr_Tooltip_Label;
        private System.Windows.Forms.DataGridView SingerMgr_DataGridView;
        private System.Windows.Forms.GroupBox SingerMgr_Query_GroupBox;
        private System.Windows.Forms.Label SingerMgr_QueryType_Label;
        private System.Windows.Forms.ComboBox SingerMgr_QueryType_ComboBox;
        private System.Windows.Forms.Button SingerMgr_Query_Button;
        private System.Windows.Forms.Label SingerMgr_QueryValue_Label;
        private System.Windows.Forms.TextBox SingerMgr_QueryValue_TextBox;
        private System.Windows.Forms.GroupBox SingerMgr_Statistics_GroupBox;
        private System.Windows.Forms.Label SingerMgr_Statistics9Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics8Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics7Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics9_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics8_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics7_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics6Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics5Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics4Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics3Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics2Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics1Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics6_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics5_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics4_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics3_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics2_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics1_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics10Value_Label;
        private System.Windows.Forms.Label SingerMgr_Statistics10_Label;
        private System.Windows.Forms.GroupBox SingerMgr_Manager_GroupBox;
        private System.Windows.Forms.GroupBox SingerMgr_SingerAdd_GroupBox;
        private System.Windows.Forms.Button SingerMgr_SingerAdd_Button;
        private System.Windows.Forms.ComboBox SingerMgr_SingerAddType_ComboBox;
        private System.Windows.Forms.Label SingerMgr_SingerAddType_Label;
        private System.Windows.Forms.TextBox SingerMgr_SingerAddName_TextBox;
        private System.Windows.Forms.Label SingerMgr_SingerAddName_Label;
        private System.Windows.Forms.Button SingerMgr_SingerImport_Button;
        private System.Windows.Forms.Button SingerMgr_SingerExport_Button;
        private System.Windows.Forms.Button SingerMgr_RebuildSingerData_Button;
        private System.Windows.Forms.Label SongQuery_Statistics12Value_Label;
        private System.Windows.Forms.Label SongQuery_Statistics12_Label;
        private System.Windows.Forms.GroupBox SongMaintenance_SongPathChange_GroupBox;
        private System.Windows.Forms.TextBox SongMaintenance_SrcSongPath_TextBox;
        private System.Windows.Forms.Label SongMaintenance_SrcSongPath_Label;
        private System.Windows.Forms.Button SongMaintenance_SongPathChange_Button;
        private System.Windows.Forms.TextBox SongMaintenance_DestSongPath_TextBox;
        private System.Windows.Forms.Label SongMaintenance_DestSongPath_Label;
        private System.Windows.Forms.Label SongMaintenance_Lang1_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang1_TextBox;
        private System.Windows.Forms.TextBox SongMaintenance_Lang1IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang1IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang10IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang10IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang10_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang10_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang9IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang9IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang9_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang9_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang8IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang8IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang8_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang8_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang7IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang7IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang7_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang7_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang6IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang6IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang6_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang6_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang5IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang5IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang5_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang5_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang4IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang4IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang4_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang4_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang3IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang3IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang3_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang3_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang2IDStr_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang2IDStr_Label;
        private System.Windows.Forms.TextBox SongMaintenance_Lang2_TextBox;
        private System.Windows.Forms.Label SongMaintenance_Lang2_Label;
        private System.Windows.Forms.Button SongMaintenance_Save_Button;
        private System.Windows.Forms.CheckBox SongMgrCfg_BackupRemoveSong_CheckBox;
        private System.Windows.Forms.ComboBox SongQuery_QueryValue_ComboBox;
        private System.Windows.Forms.Button SongQuery_Paste_Button;
        private System.Windows.Forms.Button SongQuery_Clear_Button;
        private System.Windows.Forms.CheckBox SongAdd_EngSongNameFormat_CheckBox;
        private System.Windows.Forms.TabPage SongMaintenance_Misc_TabPage;
        private System.Windows.Forms.Button SongMaintenance_SongWordCountCorrect_Button;
        private System.Windows.Forms.GroupBox SongMaintenance_RemoteCfg_GroupBox;
        private System.Windows.Forms.Button SongMaintenance_RemoteCfgExport_Button;
        private System.Windows.Forms.Button SongMaintenance_RemoteCfgImport_Button;
        private System.Windows.Forms.GroupBox SongMaintenance_Misc_GroupBox;
        private System.Windows.Forms.GroupBox SongMaintenance_Phonetics_GroupBox;
        private System.Windows.Forms.Button SongMaintenance_PhoneticsExport_Button;
        private System.Windows.Forms.Button SongMaintenance_PhoneticsImport_Button;
        private System.Windows.Forms.Button SongMaintenance_NonPhoneticsWordLog_Button;
        private System.Windows.Forms.GroupBox SongMaintenance_RebuildSongStructure_GroupBox;
        private System.Windows.Forms.Button SongMaintenance_RebuildSongStructure_Button;
        private System.Windows.Forms.Label SongMaintenance_RebuildSongStructure_Label;
        private System.Windows.Forms.TextBox SongMaintenance_RebuildSongStructure_TextBox;
        private System.Windows.Forms.TabPage SongMaintenance_DBVer_TabPage;
        private System.Windows.Forms.Label SongMaintenance_DBVer3Value_Label;
        private System.Windows.Forms.Label SongMaintenance_DBVer2Value_Label;
        private System.Windows.Forms.Label SongMaintenance_DBVer1Value_Label;
        private System.Windows.Forms.Label SongMaintenance_DBVer3_Label;
        private System.Windows.Forms.Label SongMaintenance_DBVer2_Label;
        private System.Windows.Forms.Label SongMaintenance_DBVer1_Label;
        private System.Windows.Forms.GroupBox SongMaintenance_DBVerTooltip_GroupBox;
        private System.Windows.Forms.Label SongMaintenance_DBVerTooltip_Label;
        private System.Windows.Forms.Button SongMaintenance_CompactAccessDB_Button;
        private System.Windows.Forms.TabPage MainCfg_TabPage;
        private System.Windows.Forms.Button MainCfg_Save_Button;
        private System.Windows.Forms.GroupBox MainCfg_Tooltip_GroupBox;
        private System.Windows.Forms.Label MainCfg_Tooltip_Label;
        private System.Windows.Forms.GroupBox MainCfg_General_ＧroupBox;
        private System.Windows.Forms.CheckBox MainCfg_AlwaysOnTop_CheckBox;
        private System.Windows.Forms.Label MainCfg_HideTab_Label;
        private System.Windows.Forms.CheckBox MainCfg_HideSongDBConvTab_CheckBox;
        private System.Windows.Forms.CheckBox MainCfg_HideSongLogTab_CheckBox;
        private System.Windows.Forms.CheckBox MainCfg_HideSongAddResultTab_CheckBox;
        private System.Windows.Forms.Button SingerMgr_NonSingerDataLog_Button;
        private System.Windows.Forms.ComboBox MainCfg_BackupRemoveSongDays_ComboBox;
        private System.Windows.Forms.Label MainCfg_BackupRemoveSongDays_Label;
        private System.Windows.Forms.Button SingerMgr_QueryClear_Button;
        private System.Windows.Forms.Button SingerMgr_QueryPaste_Button;
        private System.Windows.Forms.Button SingerMgr_SingerAddClear_Button;
        private System.Windows.Forms.Button SingerMgr_SingerAddPaste_Button;
        private System.Windows.Forms.Button SongMaintenance_RemoveEmptyDirs_Button;
        private System.Windows.Forms.Label SingerMgr_DefaultSingerDataTable_Label;
        private System.Windows.Forms.ComboBox SingerMgr_DefaultSingerDataTable_ComboBox;
        private System.Windows.Forms.GroupBox SongMaintenance_DBVerUpdate_GroupBox;
        private System.Windows.Forms.CheckBox SongMaintenance_EnableRebuildSingerData_CheckBox;
        private System.Windows.Forms.CheckBox SongMaintenance_EnableDBVerUpdate_CheckBox;
        private System.Windows.Forms.TabPage SongMaintenance_MultiSongPath_TabPage;
        private System.Windows.Forms.CheckBox SongMaintenance_EnableMultiSongPath_CheckBox;
        private System.Windows.Forms.TextBox SongMaintenance_MultiSongPath_TextBox;
        private System.Windows.Forms.Button SongMaintenance_MultiSongPath_Button;
        private System.Windows.Forms.ListBox SongMaintenance_MultiSongPath_ListBox;
        private System.Windows.Forms.CheckBox SongAdd_UseCustomSongID_CheckBox;
        private System.Windows.Forms.CheckBox SongQuery_SynonymousQuery_CheckBox;
        private System.Windows.Forms.TabControl SongMgrCfg_SongStructure_TabControl;
        private System.Windows.Forms.TabPage SongMgrCfg_SongStructure_TabPage;
        private System.Windows.Forms.TabPage SongMgrCfg_CustomStructure_TabPage;
        private System.Windows.Forms.Label SongMgrCfg_CustomSingerTypeStructure1_Label;
        private System.Windows.Forms.Label SongMgrCfg_CustomSingerTypeStructure8_Label;
        private System.Windows.Forms.Label SongMgrCfg_CustomSingerTypeStructure7_Label;
        private System.Windows.Forms.Label SongMgrCfg_CustomSingerTypeStructure6_Label;
        private System.Windows.Forms.Label SongMgrCfg_CustomSingerTypeStructure5_Label;
        private System.Windows.Forms.Label SongMgrCfg_CustomSingerTypeStructure4_Label;
        private System.Windows.Forms.Label SongMgrCfg_CustomSingerTypeStructure3_Label;
        private System.Windows.Forms.Label SongMgrCfg_CustomSingerTypeStructure2_Label;
        private System.Windows.Forms.ComboBox SongMgrCfg_CustomSingerTypeStructure1_ComboBox;
        private System.Windows.Forms.ComboBox SongMgrCfg_CustomSingerTypeStructure8_ComboBox;
        private System.Windows.Forms.ComboBox SongMgrCfg_CustomSingerTypeStructure6_ComboBox;
        private System.Windows.Forms.ComboBox SongMgrCfg_CustomSingerTypeStructure5_ComboBox;
        private System.Windows.Forms.ComboBox SongMgrCfg_CustomSingerTypeStructure4_ComboBox;
        private System.Windows.Forms.ComboBox SongMgrCfg_CustomSingerTypeStructure3_ComboBox;
        private System.Windows.Forms.ComboBox SongMgrCfg_CustomSingerTypeStructure7_ComboBox;
        private System.Windows.Forms.ComboBox SongMgrCfg_CustomSingerTypeStructure2_ComboBox;
        private System.Windows.Forms.GroupBox SongMgrCfg_CustomSingerTypeStructure_GroupBox;
        private System.Windows.Forms.GroupBox SingerMgr_OtherQuery_GroupBox;
        private System.Windows.Forms.CheckBox MainCfg_EnableAutoUpdate_CheckBox;
    }
}

